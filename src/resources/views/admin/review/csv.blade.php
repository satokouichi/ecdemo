<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>
  
  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">レビュー登録CSV</h1>

    <div class="card mb-4">
      <div class="card-header bg-dark text-white">
        レビュー登録CSV
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-2">CSVファイル選択</div>
          <div class="col">
            <form method="POST" action="{{ route('admin.review.import') }}" enctype="multipart/form-data">
              @csrf
              <input type="file" name="review_csv" accept="text/csv,text/tsv">
              <button type="submit" class="btn btn-primary btn-sm">一括登録を実行</button>
            </form>
            @if (count($errors) > 0) 
              @foreach ($errors as $error)
                <div class="mt-2 text-danger">{{ $error }}</div>
              @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="card mb-4">
      <div class="card-header bg-dark text-white">
        ファイルフォーマット
      </div>
      <div class="card-body">
        <div class="row align-items-center mb-2">
          <div class="col">下記のルールに沿ってCSVファイルを作成してください。</div>
          <div class="col text-end">
            <form method="POST" action="{{ route('admin.review.template') }}" novalidate>
              @csrf
              <button type="submit" class="btn btn-light border btn-sm">雛形CSVダウンロード</button>
            </form>
          </div>
        </div>
        <div class="accordion accordion-flush" id="accordionFlushExample2">
          <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseProductCsv" aria-expanded="false" aria-controls="flush-collapseProductCsv">
            項目一覧を見る
          </div>
          <div id="flush-collapseProductCsv" class="accordion-collapse collapse" aria-labelledby="flush-headingProductCsv" data-bs-parent="#accordionFlushExample2">
            <table class="table table-bproducted">
              <tbody>
                @foreach ($headers as $key => $header)
                  <tr>
                    <th class="align-middle p-2">
                      <span class="pe-2">{{ $key }}</span>
                      @if ($header['required'])
                        <span class="px-1 rounded bg-primary text-white">必須</span>
                      @endif
                    </th>
                    <td>{{ $header['description'] }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
  
  </div>

</x-admin.layout>