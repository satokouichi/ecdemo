<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">レビュー検索</h1>
    <form method="post" action="?">
      @csrf

      <div class="card mb-4 bproduct-0 shadow-sm">
        <div class="card-header">
          レビュー情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-4">
              <label for="review-ids">レビューID（複数対応）</label>
              <input id="review-ids" type="text" class="form-control" name="review_ids" value="{{ Request::get('review_ids') }}">
            </div>
            <div class="col-4">
              <label for="product-review-id-random">レビューURL</label>
              <input id="product-review-id-random" type="text" class="form-control" name="product_review_id_random" value="{{ Request::get('product_review_id_random') }}">
            </div>
            <div class="col-4">
              <label for="review-over">レビューID以上</label>
              <input id="review-over" type="text" class="form-control" name="review_over" value="{{ Request::get('review_over') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-4">
              <label for="product-ids">商品ID（複数対応）</label>
              <input id="product-ids" type="text" class="form-control" name="product_ids" value="{{ Request::get('product_ids') }}">
            </div>
            <div class="col-4">
              <label for="product-name">商品名</label>
              <input id="product-name" type="text" class="form-control" name="product_name" value="{{ Request::get('product_name') }}">
            </div>
            <div class="col-4">
              <label for="product-code">商品コード</label>
              <input id="product-code" type="text" class="form-control" name="product_code" value="{{ Request::get('product_code') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-4">
              <label for="user-ids">会員ID（複数対応）</label>
              <input id="user-ids" type="text" class="form-control" name="user_ids" value="{{ Request::get('user_ids') }}">
            </div>
            <div class="col-4">
              <label for="review-title">タイトル</label>
              <input id="review-title" type="text" class="form-control" name="review_title" value="{{ Request::get('review_title') }}">
            </div>
            <div class="col-4">
              <label for="review-title">おすすめレベル</label>
              <select id="review-level" class="form-select" name="review_level">
                <option value="0">選択してください</option>
                @foreach (config('star') as $key => $val)
                  <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <label for="review-create">投稿日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="review-create-start"
                    type="date"
                    class="form-control"
                    name="review_create_start"
                    value="{{ Request::get('review_create_start') }}"
                    data-target="#review-create-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="review-create-end"
                    type="date"
                    class="form-control"
                    name="review_create_end"
                    value="{{ Request::get('review_create_end') }}"
                    data-target="#review-create-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="review-update">更新日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="review-update-start"
                    type="date"
                    class="form-control"
                    name="review_update_start"
                    value="{{ Request::get('review_update_start') }}"
                    data-target="#review-update-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="review-update-end"
                    type="date"
                    class="form-control"
                    name="review_update_end"
                    value="{{ Request::get('review_update_end') }}"
                    data-target="#review-update-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-3 d-grid mx-auto text-center">
        <button type='submit' name="_method" value="post" formaction="{{ route('admin.review.index') }}" class="btn btn-block btn-primary btn-md">検索する</button>
      </div>

      @if(count($reviews) > 0)
        <div class="row align-items-center mb-2">
          <div class="col">
            <span class="fw-bold">{{ $reviews->total() }}件</span>の口コミが該当しました。
          </div>
          <div class="col text-end">
            <button type='submit' name="_method" value="post" formaction="{{ route('admin.review.export') }}" class="btn btn-primary btn-sm">CSVダウンロード</button>
          </div>
        </div>
        <div class="card rounded bproduct-0 mb-4">
          <div class="card-body p-0">
            <table class="table table-bproducted table-striped">
              <thead>
                <tr class="bg-dark">
                  <th class="align-middle p-2 text-white fw-normal">レビューID</th>
                  <th class="align-middle p-2 text-white fw-normal">投稿日</th>
                  <th class="align-middle p-2 text-white fw-normal">会員ID</th>
                  <th class="align-middle p-2 text-white fw-normal">商品名</th>
                  <th class="align-middle p-2 text-white fw-normal">タイトル</th>
                  <th class="align-middle p-2 text-white fw-normal">おすすめレベル</th>
                  <th class="align-middle p-2 text-white fw-normal">状態</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($reviews as $review)
                  <tr>
                    <td productclass="align-middle p-2">
                      {{ $review->id }}
                    </td>
                    <td productclass="align-middle p-2">
                      {{ \Carbon\Carbon::parse($review->payment_date)->format('Y/m/d H:i:s') }}
                    </td>
                    <td productclass="align-middle p-2">
                      {{ $review->user->id }}
                    </td>
                    <td productclass="align-middle p-2">
                      {{ $review->product->name }}
                    </td>
                    <td productclass="align-middle p-2">
                      <a href="{{ route('admin.review.edit', $review->id) }}" class="link-primary">{{ $review->title }}</a>
                    </td>
                    <td productclass="align-middle p-2">
                      {{ $review->recommend_level }}
                    </td>
                    <td productclass="align-middle p-2">
                      {{ $review->status }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="d-flex justify-content-center">
          {{ $reviews->links() }}
        </div>
      @else
        <p>レビューはありません</p>
      @endif

    </form>

  </div>

</x-admin.layout>