<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>
  
  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">レビュー登録</h1>

    <form method="POST" action="{{ route('admin.review.store') }}" enctype="multipart/form-data" novalidate>
      @csrf

      <input type="hidden" name="id" value="{{ $review ? $review->id : '' }}">

      <div class="row">

        <div class="col-8">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              レビュー情報
            </div>
            <div class="card-body">
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  レビューID
                </div>
                <div class="col-auto">
                  {{ $review ? $review->id : '' }}
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.product-id
                  product_id="product_id"
                  :value_product_id="old('product_id') ? old('product_id') : ($review ? $review->product_id : '')"/>
              </div>
              @if ($review)
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">商品名</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      {{ $review->product->name }}
                    </div>
                  </div>
                </div>
              </div>
              @endif
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">投稿日</span><span class="px-1 rounded bg-primary text-white">必須</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <x-admin.form.review.create-date
                        create_date="create_date"
                        :value_create_date="old('create_date') ? old('create_date') : ($review ? \Carbon\Carbon::parse($review->create_date)->format('Y-m-d') : '')"/>
                    </div>
                    <div class="col">
                      <x-admin.form.review.create-time
                        create_time="create_time"
                        :value_create_time="old('create_time') ? old('create_time') : ($review ? \Carbon\Carbon::parse($review->create_date)->format('H:i') : '')"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">規格ID</span>
                </div>
                <div class="col-9">
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">タグID</span>
                </div>
                <div class="col-9">
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.user-id
                  user_id="user_id"
                  :value_user_id="old('user_id') ? old('user_id') : ($review ? $review->user_id : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">レビュー画像</span>
                </div>
                <div class="col-9">
                  @if ( $review and $review->reviewer_url )
                    <div class="mb-2">
                      <img src="{{ asset('storage/images/review/main/'.$review->reviewer_url.'.jpg') }}">
                    </div>
                  @endif
                  <input type="file" name="image" accept="image/png,image/jpeg,image/jpg,image/gif" />
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.image-status
                  image_status="image_status"
                  :select="config('status')"
                  :value_image_status="old('image_status') ? old('image_status') : ($review ? $review->image_status : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.recommend-level
                  recommend_level="recommend_level"
                  :select="config('star')"
                  :value_recommend_level="old('recommend_level') ? old('recommend_level') : ($review ? $review->recommend_level : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.title
                  title="title"
                  :value_title="old('title') ? old('title') : ($review ? $review->title : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.comment
                  comment="comment"
                  :value_comment="old('comment') ? old('comment') : ($review ? $review->comment : '')"
                  :rows="10"/>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.review.reviewer-name
                  reviewer_name="reviewer_name"
                  :value_reviewer_name="old('reviewer_name') ? old('reviewer_name') : ($review ? $review->reviewer_name : '')"/>
              </div>
            </div>
          </div>
        </div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              登録日・更新日
            </div>
            <div class="card-body">
              <div class="row mb-2">
                <div class="col-4">登録日</div>
                <div class="col">{{ $review ? \Carbon\Carbon::parse($review->create)->format('Y/m/d H:i:s') : '' }}</div>
              </div>
              <div class="row mb-2">
                <div class="col-4">更新日</div>
                <div class="col">{{ $review ? \Carbon\Carbon::parse($review->update)->format('Y/m/d H:i:s') : '' }}</div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-9 text-start">
                <a href="{{ route('admin.review.index') }}" class="text-white"> レビュー一覧へ戻る</a>
              </div>
              <div class="col-3 text-end">
                <div class="row g-2 align-items-center">
                  <div class="col-6">
                    <x-admin.form.status
                      status="status"
                      :select="config('status')"
                      :value_status="old('status') ? old('status') : ($review ? $review->status : '')"/>
                  </div>
                  <div class="col-6">
                    <button type="submit" class="btn btn-primary btn-md w-100">
                      更新する
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>
  
  </div>

</x-admin.layout>