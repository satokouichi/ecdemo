<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4 pb-5">
  
    <h1 class="mb-3 fs-4">入金処理</h1>

    <div class="card mb-3">
      <div class="card-header bg-dark text-white">
        ご請求明細
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col">
            <table class="table table-bordered m-0">
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">注文番号</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->id }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">お名前</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->order_name01 }} {{ $order->order_name02 }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">対応状況</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->orderStatus ? $order->orderStatus->name : '' }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">請求金額</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  ￥{{ number_format($order->payment_total) }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">入金金額</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  ￥{{ $order->payment_sum ? number_format($order->payment_sum) : 0 }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">追跡番号</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->tracking_sum }}
                </td>
              </tr>
            </table>
          </div>
          <div class="col">
            <table class="table table-bordered m-0">
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">注文日</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->order_date ? \Carbon\Carbon::parse($order->order_date)->format('Y年m月d日') : '----' }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">入金日</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->payment_date ? \Carbon\Carbon::parse($order->payment_date)->format('Y年m月d日') : '----' }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">発送日</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->commit_date ? \Carbon\Carbon::parse($order->commit_date)->format('Y年m月d日') : '----' }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">更新日</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  {{ $order->update_date ? \Carbon\Carbon::parse($order->update_date)->format('Y年m月d日') : '----' }}
                </td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">備考欄</th>
                <td class="align-middle p-2 fw-normal bg-white">
                  <order-note
                  :id='@json($order->id)'
                  :text='@json($order->note)'
                  :token='@json(csrf_token())'
                  ></order-note>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header bg-dark text-white">
        入金処理
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col">
            <table class="table table-bordered m-0 mb-2">
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-4">今回の入金金額</th>
                <td class="align-middle p-2 fw-normal">
                  <payment-create
                  :id='@json($order->id)'
                  :token='@json(csrf_token())'
                  ></payment-create>
                </td>
              </tr>
            </table>
            <payment-delete
            :payments='@json($order->orderPayments->toArray())'
            :token='@json(csrf_token())'
            ></payment-delete>
            <table class="table table-bordered m-0">
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">入金金額の合計</th>
                <td class="align-middle p-2 fw-normal">￥{{ $order->payment_sum ? number_format($order->payment_sum) : 0 }}</td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">請求金額</th>
                <td class="align-middle p-2 fw-normal">￥{{ number_format($order->payment_total) }}</td>
              </tr>
              <tr>
                <th class="align-middle p-2 fw-normal bg-light col-3">差額</th>
                <td class="align-middle p-2 fw-normal">￥{{ $order->payment_sum ? number_format($order->payment_sum - $order->payment_total) : 0 }}</td>
              </tr>
            </table>
          </div>
          <div class="col">
            @if ($order->payment_sum > 0) 
              <table class="table table-bordered m-0">
                <tr>
                  <th class="align-middle p-2 fw-normal bg-light col-3">メールに使用するテンプレート</th>
                </tr>
                <tr>
                  <td class="align-middle p-2 fw-normal bg-white">
                    @if ($order->payment_total == $order->payment_sum) 
                      <a href="{{ route('admin.mail.index', ['orderId' => $order->id, 'templateId' => config('mail_template_id')['payment']]) }}" class="link-primary">丁度のメールテンプレート</a>
                    @elseif ($order->payment_total < $order->payment_sum)
                      <a href="{{ route('admin.mail.index', ['orderId' => $order->id, 'templateId' => config('mail_template_id')['many_pay']]) }}" class="link-primary">過剰金のメールテンプレート</a>
                    @elseif ($order->payment_total > $order->payment_sum)
                      <a href="{{ route('admin.mail.index', ['orderId' => $order->id, 'templateId' => config('mail_template_id')['not_pay']]) }}" class="link-primary">不足金のメールテンプレート</a>
                    @endif
                  </td>
                </tr>
              </table>
            @endif
          </div>
        </div>
      </div>
    </div>

    <div class="card mb-3">
      <div class="card-header bg-dark text-white">
        注文商品情報
      </div>
      <div class="card-body">
        <table class="table table-bordered m-0 mb-3">
          <tr>
            <th class="align-middle p-2 fw-normal bg-light col-3">商品コード</th>
            <th class="align-middle p-2 fw-normal bg-light col-3">商品名</th>
            <th class="align-middle p-2 fw-normal bg-light col-3">数量</th>
          </tr>
          @foreach ($order->orderDetails as $orderDetail)
            <tr>
              <td class="align-middle p-2 fw-normal">{{ $orderDetail->product_code }}</td>
              <td class="align-middle p-2 fw-normal">{{ $orderDetail->product_name }}</td>
              <td class="align-middle p-2 fw-normal">{{ $orderDetail->quantity }}</td>
            </tr>
          @endforeach
        </table>
      </div>
    </div>

    <div class="footer fixed-bottom text-center">
      <div class="d-flex">
        <div class="col-2 py-2 bg-white">
        </div>
        <div class="col-10 py-2 px-3 bg-secondary">
          <div class="col text-start">
            <a href="/admin/order" class="text-white"> 受注一覧へ戻る</a>
          </div>
          <div class="col text-end"></div>
        </div>
      </div>
    </div>

  </div>

</x-admin.layout>