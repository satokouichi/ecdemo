<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>
  
  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">{{ $product->name }}の商品規格</h1>

    <form method="post" action="{{ route('admin.productclass.store', $product->id) }}">
      @csrf

      <input type="hidden" name="product_id" value="{{ $product->id }}" />

      <div class="card mb-4 bproduct-0 shadow-sm">
        <div class="card-body">
          @if(count($productClasses) > 0)
            <div class="row w-50 g-2 align-items-center mb-2">
              <div class="col-2">割引パターン </div>
              <div class="col-auto">
                <x-admin.form.productclass.discount-pattern
                  discount_pattern="discount_pattern"
                  :select="$discountPatterns"
                  :value_discount_pattern="old('discount_pattern') ? old('discount_pattern') : ($product ? $product->discount_pattern : '')"/>
              </div>
            </div>
            <div class="mb-1">
              ※通常価格の単品のみ編集可能です。セットの価格と販売価格は「割引パターン」により自動計算されます。
            </div>
            <div class="row align-items-center mb-2">
              <div class="col">
                <span class="fw-bold">{{ count($productClasses) }}件</span>の商品規格が該当しました。
              </div>
              <div class="col text-end">
              </div>
            </div>
            <div class="card rounded bproduct-0 mb-4">
              <div class="card-body p-0">
                <table class="table table-bproducted table-hover">
                  <thead>
                    <tr class="bg-dark">
                      <th class="align-middle p-2 text-white fw-normal" width="5%">規格ID</th>
                      <th class="align-middle p-2 text-white fw-normal" width="7%">規格1</th>
                      <th class="align-middle p-2 text-white fw-normal" width="7%">規格2</th>
                      <th class="align-middle p-2 text-white fw-normal" >商品コード/在庫ID</th>
                      <th class="align-middle p-2 text-white fw-normal" width="9%">在庫数</th>
                      <th class="align-middle p-2 text-white fw-normal" width="9%">状態</th>
                      <th class="align-middle p-2 text-white fw-normal" width="10%">通常価格</th>
                      <th class="align-middle p-2 text-white fw-normal" width="10%">販売価格</th>
                      <th class="align-middle p-2 text-white fw-normal" width="6%">内容量</th>
                      <th class="align-middle p-2 text-white fw-normal" width="6%">取扱い</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($productClasses as $productClass)
                      <tr>
                        <td class="align-middle p-2">
                          <input
                            type="hidden"
                            name="class[{{ $productClass->id }}][id]"
                            value="{{ $productClass->id }}"
                          />
                          {{ $productClass->id }}
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="hidden"
                            name="class[{{ $productClass->id }}][class_category_id1]"
                            value="{{ $productClass->classCategories1->id }}"
                          />
                          {{ $productClass->classCategories1->name }}
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="hidden"
                            name="class[{{ $productClass->id }}][class_category_id2]"
                            value="{{ $productClass->classCategories2->id }}"
                          >
                          {{ $productClass->classCategories2->name }}
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="text"
                            name="class[{{ $productClass->id }}][product_code]"
                            value="{{ $productClass->product_code }}"
                            class="form-control mb-1"
                          />
                          <input
                            type="text"
                            name="class[{{ $productClass->id }}][zaiko_id]"
                            value="{{ $productClass->zaiko_id }}"
                            class="form-control"
                          />
                        </td>
                        <td class="align-middle p-2">
                          <product-stock
                          :id="{{ $productClass->id }}"
                          :value-stock="{{ $productClass->stock ? $productClass->stock : 0 }}"
                          :value-stock-unlimited="{{ $productClass->stock_unlimited }}"/>
                        </td>
                        <td class="align-middle p-2">
                          @foreach (config('status') as $key => $val)
                            <div class="form-check">
                              <input
                                id="status{{ $key }}"
                                class="form-check-input"
                                type="radio"
                                name="class[{{ $productClass->id }}][status]"
                                value="{{ $key }}"
                                {{ $productClass->status == $key ? 'checked' : '' }}
                              >
                              <label class="form-check-label" for="status{{ $key }}">
                                {{ $val }}
                              </label>
                            </div>
                          @endforeach
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="text"
                            name="class[{{ $productClass->id }}][price01]"
                            value="{{ $productClass->price01 }}"
                            class="form-control mb-1"
                            {{ $productClass->classCategories2->name != 1 ? 'disabled' : '' }}
                          />
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="text"
                            name="class[{{ $productClass->id }}][price02]"
                            value="{{ $productClass->price02 }}"
                            class="form-control mb-1"
                            disabled
                          />
                        </td>
                        <td class="align-middle p-2">
                          <input
                            type="text"
                            name="class[{{ $productClass->id }}][dose]"
                            value="{{ $productClass->dose }}"
                            class="form-control mb-1"
                            {{ $productClass->classCategories2->name != 1 ? 'disabled' : '' }}
                          />
                        </td>
                        <td class="align-middle p-2">
                          @foreach (config('handling') as $key => $val)
                            <div class="form-check">
                              <input
                                id="handling{{ $key }}"
                                class="form-check-input"
                                type="radio"
                                name="class[{{ $productClass->id }}][handling_flg]"
                                value="{{ $key }}"
                                {{ $productClass->handling_flg == $key ? 'checked' : '' }}
                              />
                              <label class="form-check-label" for="status{{ $key }}">
                                {{ $val }}
                              </label>
                            </div>
                          @endforeach
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          @else
            <p>商品はありません</p>
          @endif
        </div>
      </div>
      
      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-9 text-start">
                <a href="{{ route('admin.product.edit', $product->id) }}" class="text-white"> 商品登録へ戻る</a>
              </div>
              <div class="col-3 text-end">
                <div class="row g-2 align-items-center">
                  <div class="col-6">
                  </div>
                  <div class="col-6">
                    <button type="submit" class="btn btn-primary btn-md w-100">
                      更新する
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>

</x-admin.layout>