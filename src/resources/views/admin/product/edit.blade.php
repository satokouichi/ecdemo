<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>
  
  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">商品登録</h1>

    <form method="POST" action="{{ route('admin.product.store') }}" class="pb-5" novalidate>
      @csrf

      <input type="hidden" name="id" value="{{ $product ? $product->id : '' }}">

      <div class="row">

        <div class="col-8">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              商品情報
            </div>
            <div class="card-body">
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  商品ID
                </div>
                <div class="col-auto">
                  {{ $product ? $product->id : '' }}
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.product.name
                  name="name"
                  :value_name="old('name') ? old('name') : ($product ? $product->name : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.product.category
                  category="category"
                  :value_category="old('category') ? old('category') : ($product ? $product->category_ids : '')"/>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">提案カテゴリ</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">レコメンド</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">疾患ID</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">成分ID</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">製薬会社ID</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">種別</span>
                </div>
                <div class="col-9">
                  <div class="row">
                    <div class="col">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <x-admin.form.product.trader
                  trader="trader_id"
                  :select="config('trader')"
                  :value_trader="old('trader_id') ? old('trader_id') : ($product ? $product->trader_id : '')"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.image
                  image="image"
                  alt="alt"
                  :images="count($product->productImages) ? $product->productImages : ''"
                  :value_image="old('image') ? old('image') : ''"
                  :value_alt="old('alt') ? old('alt') : ''"
                  :rows="5"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.description-list
                  description_list="description_list"
                  :value_description_list="old('description_list') ? old('description_list') : ($product ? $product->description_list : '')"
                  :rows="5"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.description-detail
                  description_detail="description_detail"
                  :value_description_detail="old('description_detail') ? old('description_detail') : ($product ? $product->description_detail : '')"
                  :rows="10"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.search-word
                  search_word="search_word"
                  :value_search_word="old('search_word') ? old('search_word') : ($product ? $product->search_word : '')"
                  :rows="10"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.recommend-text
                  recommend_text="recommend_text"
                  :value_recommend_text="old('recommend_text') ? old('recommend_text') : ($product ? $product->recommend_text : '')"
                  :rows="5"/>
              </div>
              <div class="row mb-3">
                <x-admin.form.product.title
                  title="title"
                  :value_title="old('title') ? old('title') : ($product ? $product->title : '')"
                  :rows="5"/>
              </div>
              <div class="row">
                <x-admin.form.product.description
                  description="description"
                  :value_description="old('description') ? old('description') : ($product ? $product->description : '')"
                  :rows="5"/>
              </div>
            </div>
          </div>
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              タグ登録
            </div>
            <div class="card-body">
              使用するタグ
            </div>
          </div>
        </div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              登録日・更新日
            </div>
            <div class="card-body">
              <div class="row mb-2">
                <div class="col-4">登録日</div>
                <div class="col">{{ \Carbon\Carbon::parse($product->create)->format('Y/m/d H:i:s') }}</div>
              </div>
              <div class="row mb-2">
                <div class="col-4">更新日</div>
                <div class="col">{{ \Carbon\Carbon::parse($product->update)->format('Y/m/d H:i:s') }}</div>
              </div>
            </div>
            <div class="card-footer bg-white">
              <a href="{{ route('admin.productclass.index', $product->id) }}" class="btn btn-light border-dark btn-md w-100">規格設定</a>
            </div>
          </div>
        </div>

      </div>

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-9 text-start">
                <a href="{{ route('admin.product.index') }}" class="text-white"> 商品一覧へ戻る</a>
              </div>
              <div class="col-3 text-end">
                <div class="row g-2 align-items-center">
                  <div class="col-6">
                    <x-admin.form.status
                      status="status"
                      :select="config('status')"
                      :value_status="old('status') ? old('status') : ($product ? $product->status : '')"/>
                  </div>
                  <div class="col-6">
                    <button type="submit" class="btn btn-primary btn-md w-100">
                      更新する
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>
  
  </div>

</x-admin.layout>