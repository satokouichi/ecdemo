<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">商品検索</h1>
    <form method="post" action="?">
      @csrf

      <div class="card mb-4 bproduct-0 shadow-sm">
        <div class="card-header">
          商品情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-4">
              <label for="product-id">商品ID（複数対応）</label>
              <input id="product-id" type="text" class="form-control" name="product_ids" value="{{ Request::get('product_ids') }}">
            </div>
            <div class="col-4">
              <label for="product-name">商品名</label>
              <input id="product-name" type="text" class="form-control" name="product_name" value="{{ Request::get('product_name') }}">
            </div>
            <div class="col-4">
              <label for="product-category">カテゴリ</label>
              <select id="product-category" class="form-select" name="product_category">
              @foreach (config('month') as $key => $val)
                @if ($loop->first)
                  <option value="">--</option>
                @endif
                <option value="{{ $val }}" @if(old('product_category') == $val) selected @endif>{{ $val }}</option>
              @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-4">
              <label for="product-code">商品コード</label>
              <input id="product-code" type="text" class="form-control" name="product_code" value="{{ Request::get('product_code') }}">
            </div>
            <div class="col-4">
              <label for="product-zaiko">在庫ID</label>
              <input id="product-zaiko" type="text" class="form-control" name="product_zaiko" value="{{ Request::get('product_zaiko') }}">
            </div>
            <div class="col-4">
              <label for="product-stock">在庫</label>
              <div class="col">
                <div class="form-check form-check-inline">
                  <input
                    id="product-stock1"
                    class="form-check-input"
                    type="checkbox"
                    name="product_stock[]"
                    value="1"
                    {{ is_array(Request::get('product_stock')) && in_array(1, Request::get('product_stock'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="product-stock1">
                    あり
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input
                    id="product-stock2"
                    class="form-check-input"
                    type="checkbox"
                    name="product_stock[]"
                    value="2"
                    {{ is_array(Request::get('product_stock')) && in_array(2, Request::get('product_stock'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="product-stock2">
                    なし
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <label for="product-create">登録日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="product-create-start"
                    type="date"
                    class="form-control"
                    name="product_create_start"
                    value="{{ Request::get('product_create_start') }}"
                    data-target="#product-create-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="product-create-end"
                    type="date"
                    class="form-control"
                    name="product_create_end"
                    value="{{ Request::get('product_create_end') }}"
                    data-target="#product-create-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="product-update">更新日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="product-update-start"
                    type="date"
                    class="form-control"
                    name="product_update_start"
                    value="{{ Request::get('product_update_start') }}"
                    data-target="#product-update-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="product-update-end"
                    type="date"
                    class="form-control"
                    name="product_update_end"
                    value="{{ Request::get('product_update_end') }}"
                    data-target="#product-update-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-3 d-grid mx-auto text-center">
        <button type='submit' name="_method" value="post" formaction="{{ route('admin.product.index') }}" class="btn btn-block btn-primary btn-md">検索する</button>
      </div>

      @if(count($products) > 0)
        <div class="row align-items-center mb-2">
          <div class="col">
            <span class="fw-bold">{{ $products->total() }}件</span>の商品が該当しました。
          </div>
          <div class="col text-end">
            <button type='submit' name="_method" value="post" formaction="{{ route('admin.product.export') }}" class="btn btn-primary btn-sm">CSVダウンロード</button>
          </div>
        </div>
        <div class="card rounded bproduct-0 mb-4">
          <div class="card-body p-0">
            <table class="table table-bproducted table-striped">
              <thead>
                <tr class="bg-dark">
                  <th class="align-middle p-2 text-white fw-normal">商品ID</th>
                  <th class="align-middle p-2 text-white fw-normal">商品画像</th>
                  <th class="align-middle p-2 text-white fw-normal">商品名</th>
                  <th class="align-middle p-2 text-white fw-normal">商品コード</th>
                  <th class="align-middle p-2 text-white fw-normal">在庫</th>
                  <th class="align-middle p-2 text-white fw-normal">種別</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($products as $product)
                  <tr>
                    <td productclass="align-middle p-2">{{ $product->id }}</td>
                    <td productclass="align-middle p-2"><img src="http://via.placeholder.com/50x50" alt=""></td>
                    <td productclass="align-middle p-2">
                      <a href="{{ route('admin.product.edit', $product->id) }}" class="link-primary">{{ $product->name }}</a>
                    </td>
                    <td productclass="align-middle p-2">-</td>
                    <td productclass="align-middle p-2">-</td>
                    <td productclass="align-middle p-2">-</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="d-flex justify-content-center">
          {{ $products->links() }}
        </div>
      @else
        <p>商品はありません</p>
      @endif

    </form>

  </div>

</x-admin.layout>