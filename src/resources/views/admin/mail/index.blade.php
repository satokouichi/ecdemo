<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">
      メール通知
    </h1>

    <form method="POST" action="{{ route('admin.mail.confirm', ['orderId' => $orderId, 'templateId' => $templateId]) }}" novalidate>
      @csrf
      <input type="hidden" name="order_id" value="{{ $orderId }}">
      <div class="card mb-4">
        <div class="card-header bg-dark text-white">
          メール配信履歴
        </div>
        <div class="card-body">
          @if (count($mailHistories) > 0) 
            <table class="table">
              <thead>
                <tr>
                  <th rowspan="2" class="align-middle p-2" width="20%">処理日</th>
                  <th rowspan="2" class="align-middle p-2" width="40%">通知メール</th>
                  <th rowspan="2" class="align-middle p-2" width="40%">件名</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($mailHistories as $mailHistory)
                  <tr>
                    <td>{{ $mailHistory->send_date ? \Carbon\Carbon::parse($mailHistory->send_date)->format('Y年m月d日') : '----' }}</td>
                    <td>{{ $mailHistory->subject }}</td>
                    <td>{{ $mailHistory->subject }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          @else
            <p>メール通知履歴はありません</p>
          @endif
        </div>
      </div>

      <div class="card mb-4">
        <div class="card-header bg-dark text-white">
          メール配信
        </div>
        <div class="card-body">
          <div class="col-9">
            @if ($templateId) 
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">テンプレート</span>
                </div>
                <div class="col-9">
                  <input type="hidden" name="template_id" value="{{ $mailTemplate['id'] }}">
                  <div>{{ $mailTemplate['name'] }}</div>
                </div>
              </div>   
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">件名</span>
                </div>
                <div class="col-9">
                  <input
                    type="text"
                    name="subject"
                    value="{{ old('subject') ? old('subject') : $mailTemplate['subject'] }}"
                    class="form-control"
                  >
                  <x-admin.form.errors name="subject"/>
                </div>
              </div>
              <div class="row mb-3">
                <div class="col-3">
                  <span class="pe-2">ヘッダー</span>
                </div>
                <div class="col-9">
                  <textarea
                    type="text"
                    name="header"
                    value="{{ old('header') ? old('header') : $mailTemplate['header'] }}"
                    class="form-control"
                    rows="10"
                  ></textarea>
                  <x-admin.form.errors name="subject"/>
                </div>
              </div>
              <div class="row align-items-center mb-3">
                <div class="col-3">
                  <span class="pe-2">フッター</span>
                </div>
                <div class="col-9">
                  <textarea
                    type="text"
                    name="footer"
                    value="{{ old('footer') ? old('footer') : $mailTemplate['footer'] }}"
                    class="form-control"
                    rows="10"
                  ></textarea>
                  <x-admin.form.errors name="footer"/>
                </div>
              </div>           
            @else
              <select class="form-select" name="$templateId">
                  <option value="0">選択してください</option>
                @foreach($select as $key => $val)
                  <option value="{{ $key }}" {{ $valueTemplateId == $key ? "selected" : "" }}>{{ $val }}</option>
                @endforeach 
              </select>
            @endif
          </div>
        </div>
      </div>

      <div class="card mb-4">
        <div class="card-header bg-dark text-white">
          メール変数
        </div>
        <div class="card-body">
          <div class="mb-3">
            <div class="mb-2">注文番号　{order_id}</div>
            <div>請求金額　{payment_total}</div>
          </div>
          <div class="mb-3 p-3 bg-light border rounded">
            <div class="mb-2 fw-bold">注文時の顧客情報</div>
            <div class="mb-2">名前　{order_name}</div>
            <div class="mb-2">支払期限　{order_kigen}</div>
            <div>都道府県　{order_pref_addr}</div>
          </div>
          <div class="mb-3 p-3 bg-light border rounded">
            <div class="mb-2 fw-bold">現時点での顧客情報</div>
            <div class="mb-2">名前　{name}</div>
            <div class="mb-2">メールアドレス　{email}</div>
            <div class="mb-2">住所　{pref_addr}</div>
            <div class="mb-2">性別　{sex}</div>
            <div class="mb-2">ポイント　{point}</div>
            <div class="mb-2">誕生日年　{birth_y}</div>
            <div>誕生日月日　{birth_md}</div>
          </div>
          <div class="p-3 bg-light border rounded">
            <div class="mb-2 fw-bold">サイト情報</div>
            <div>サイト名　{site_name}</div>
          </div>
        </div>
      </div>

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-10 text-start">
                <a href="{{ route('admin.order.index') }}" class="text-white"> 注文一覧へ戻る</a>
              </div>
              <div class="col-2 text-end">
                <button type="submit" class="btn btn-primary btn-md">
                  確認する
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>

</x-admin.layout>