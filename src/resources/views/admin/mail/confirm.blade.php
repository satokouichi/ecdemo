<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">
      メール確認
    </h1>

    <form method="POST" action="?" novalidate>
      @csrf
      <input type="hidden" name="order_id" value="{{ $orderId }}">
      <div class="card mb-4">
        <div class="card-header bg-dark text-white">
          メール確認
        </div>
        <div class="card-body">
          <div class="col-9">
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">テンプレート</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="template_id" value="{{ $inputs['template_id'] }}">
                <div>{{ $inputs['template_id'] }}</div>
              </div>
            </div>   
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">件名</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="subject" value="{{ $inputs['subject'] }}">
                <div>{{ $inputs['subject'] }}</div>
              </div>
            </div>
            <div class="row mb-3">
              <div class="col-3">
                <span class="pe-2">ヘッダー</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="header" value="{{ $inputs['header'] }}">
                <div>{{ $inputs['header'] }}</div>
              </div>
            </div>
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">フッター</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="footer" value="{{ $inputs['footer'] }}">
                <div>{{ $inputs['footer'] }}</div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-10 text-start">
                <button type='submit' name="_method" value="post" formaction="{{ route('admin.mail.back', ['orderId' => $orderId, 'templateId' => $templateId]) }}" class="btn btn-light btn-md">
                  修正する
                </button>
              </div>
              <div class="col-2 text-end">
                <button type='submit' name="_method" value="post" formaction="{{ route('admin.mail.send', ['orderId' => $orderId, 'templateId' => $templateId]) }}" class="btn btn-primary btn-md">
                  送信する
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>

</x-admin.layout>