<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">カテゴリ</h1>
    <form method="post" action="?">
      @csrf

      <div class="row">

        <div class="col-3">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              カテゴリ検索
            </div>
            <div class="card-body">
            </div>
            <div class="card-footer bg-white">
            </div>
          </div>
        </div>

        <div class="col-9">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              カテゴリ情報
            </div>
            <div class="card-body">
              <div class="row align-items-center mb-2">
                <div class="col">
                  カテゴリを設定してください。
                </div>
                <div class="col text-end">
                  <button type='submit' name="_method" value="post" formaction="{{ route('admin.category.export') }}" class="btn btn-primary btn-sm">CSVダウンロード</button>
                </div>
              </div>
              @if (count($categories))
                @foreach ($categories as $category)
                  <div>{{ $category->category_name }}</div>
                @endforeach
              @endif
            </div>
          </div>
        </div>

      </div>
    
    </form>

  </div>

</x-admin.layout>