<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">受注登録</h1>

    <form method="POST" action="{{ route('admin.order.store') }}" novalidate>
      @csrf

      <input type="hidden" name="id" value="{{ $order ? $order->id : '' }}">

      <div class="card mb-3">
        <div class="card-header bg-dark text-white">
          基本情報
        </div>
        <div class="card-body">

          <div class="row align-items-center mb-3">
            <div class="col-3">
              注文番号
            </div>
            <div class="col-auto">
              @if ($order)
                {{ $order->id }}（{{ $order->order_no }}）
              @endif
            </div>
          </div>
          <div class="row align-items-center mb-3">
            <div class="col-3">
              受注日
            </div>
            <div class="col-auto">
              @if ($order)
                {{ $order->order_date ? \Carbon\Carbon::parse($order->order_date)->format('Y年m月d日') : '----' }}
              @endif
            </div>
          </div>
          <div class="row align-items-center">
            <div class="col-3">
              対応状況
            </div>
            <div class="col-3">
              @if ($order)
                <x-admin.form.order.status
                  status="status"
                  :select="$orderStatuses"
                  :value_status="old('status') ? old('status') : ($order ? $order->status : '')"/>
              @else
                新規受付
              @endif
            </div>
          </div>
          <div class="row align-items-center mt-3 mb-3">
            <div class="col-3">
              入金日
            </div>
            <div class="col-auto">
              @if ($order)
                {{ $order->payment_date ? \Carbon\Carbon::parse($order->payment_date)->format('Y年m月d日') : '未入金' }}
              @else
                未入金
              @endif
            </div>
          </div>
          <div class="row align-items-center">
            <div class="col-3">
              発送日
            </div>
            <div class="col-auto">
              @if ($order)
                {{ $order->commit_date ? \Carbon\Carbon::parse($order->commit_date)->format('Y年m月d日') : '未発送' }}
              @else
                未発送
              @endif
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-3">
        <div class="card-header bg-dark text-white">
          注文者情報
        </div>
        <div class="card-body">
          <div class="row align-items-center mb-3">
            <div class="col-3">
              会員ID
            </div>
            <div class="col-auto">
              @if ($order)
                  {{ $order->user->id }}
              @else
                <div>非会員</div>
                <button type="button" class="btn d-inline btn-light border" data-bs-toggle="modal" data-bs-target="#userAddModal">会員から検索・入力</button>
              @endif
            </div>
          </div>        
          @if ($order)
            <div class="row mb-3">
              <div class="col-3">
                現在のポイント
              </div>
              <div class="col-auto">
                {{ $order->user->point }}
              </div>
            </div>
          @endif
          <div class="row align-items-center mb-3">
            <x-admin.form.name
              name01="order_name01"
              name02="order_name02"
              :value_name01="old('order_name01') ? old('order_name01') : ($order ? $order->order_name01 : '')"
              :value_name02="old('order_name02') ? old('order_name02') : ($order ? $order->order_name02 : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.kana
              kana01="order_kana01"
              kana02="order_kana02"
              :value_kana01="old('order_kana01') ? old('order_kana01') : ($order ? $order->order_kana01 : '')"
              :value_kana02="old('order_kana02') ? old('order_kana02') : ($order ? $order->order_kana02 : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.email
              email="order_email"
              :value_email="old('order_email') ? old('order_email') : ($order ? $order->order_email : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <div class="col-3">
              TEL(電話注文)
            </div>
            <div class="col-6">
              <div class="row">
                <div class="col">
                  <input
                  type="text"
                  class="form-control"
                  name="order_tel"
                  value="{{ old('order_tel') ? old('order_tel') : ($order ? $order->order_tel : '') }}"
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-3">
              住所
            </div>
            <div class="col-6">
              <div class="row w-50 g-2 align-items-center mb-3">
                <div class="col-1 text-center">〒</div>
                <div class="col-4">
                  <input
                  type="text"
                  class="form-control"
                  name="order_zip01"
                  value="{{ old('order_zip01') ? old('order_zip01') : ($order ? $order->order_zip01 : '') }}"
                  >
                </div>
                <div class="col-1 text-center">-</div>
                <div class="col-4">
                  <input
                  type="text"
                  class="form-control"
                  name="order_zip02"
                  value="{{ old('order_zip02') ? old('order_zip02') : ($order ? $order->order_zip02 : '') }}"
                  >
                </div>
                @error('order_zip01')
                  <div class="mt-1 text-danger">{{ $message }}</div>
                @enderror
                @error('order_zip02')
                  <div class="mt-1 text-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="w-50 mb-3">
                <select class="form-select" name="order_pref">
                  @foreach(config('pref') as $key => $pref)
                    @if(old('order_pref'))
                      <option value="{{ $key }}" {{ old('order_pref') == $pref ? "selected" : "" }}>{{ $pref }}</option>
                    @elseif($order && $order->order_pref)
                      <option value="{{ $key }}" {{ $order->order_pref == $pref ? "selected" : "" }}>{{ $pref }}</option>
                    @else
                      <option value="{{ $key }}">{{ $pref }}</option>
                    @endif
                  @endforeach 
                </select>
              </div>
              <div class="mb-3">
                <input
                type="text"
                class="form-control"
                name="order_addr01"
                value="{{ old('order_addr01') ? old('order_addr01') : ($order ? $order->order_addr01 : '') }}"
                >
                @error('order_addr01')
                  <div class="mt-1 text-danger">{{ $message }}</div>
                @enderror
              </div>
              <div class="mb-0">
                <input
                type="text"
                class="form-control"
                name="order_addr02"
                value="{{ old('order_addr02') ? old('order_addr02') : ($order ? $order->order_addr02 : '') }}"
                >
                @error('order_addr02')
                  <div class="mt-1 text-danger">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>        
        </div>
      </div>

      <div class="card mb-3">
        <div class="card-header bg-dark text-white">
          受注商品情報
        </div>
        <div class="card-body">
          <button type="button" class="btn d-inline btn-light border me-2">計算結果を更新</button>
          <button type="button" class="btn d-inline btn-light border" data-bs-toggle="modal" data-bs-target="#productAddModal">商品を追加</button>
          <table class="table table-hover m-0 mb-3">
            <thead>
              <tr>
                <th>商品名・商品コード</th>
                <th>単価</th>
                <th>数量</th>
                <th>小計</th>
                <th class="text-end">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
                @if($order && $order->orderDetails)
                  @foreach ($order->orderDetails as $orderDetail)
                    <tr>
                      <td class="align-middle">
                        {{ $orderDetail->product_name }}<br>
                        {{ $orderDetail->product_code }}
                      </td>
                      <td class="align-middle" width="10%">
                        ￥{{ number_format($orderDetail->price) }}
                      </td>
                      <td class="align-middle" width="10%">
                        <input type="hidden" name="order_details[{{ $orderDetail->id }}][product_id]" value="{{ $orderDetail->product_id }}">
                        <input type="hidden" name="order_details[{{ $orderDetail->id }}][product_class_id]" value="{{ $orderDetail->product_class_id }}">
                        <input
                          type="text"
                          name="order_details[{{ $orderDetail->id }}][quantity]"
                          value="{{ old('quantity') ? old('quantity') : ($order ? $orderDetail->quantity : '') }}"
                          class="form-control w-75"
                        >
                      </td>
                      <td class="align-middle" width="10%">
                        ￥{{ number_format($orderDetail->price * $orderDetail->quantity) }}
                      </td>
                      <td class="align-middle text-end" width="10%">
                        <button class="btn btn-sm btn-light border rounded">削除</button>
                      </td>
                    </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
            <div class="row">
              <div class="col-8">
              </div>            
              <div class="col-4 text-end">
                <div class="row align-items-center">
                  <div class="col">小計</div>
                  <div class="col">￥{{ $order ? number_format($order->subtotal) : 0 }}</div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">値引き</div>
                  <div class="col p-1 d-flex justify-content-end align-items-center">
                    <span class="pe-1">￥</span>
                    <input
                    type="text"
                    class="w-75 form-control"
                    name="discount"
                    value="{{ old('discount') ? old('discount') : ($order ? $order->discount : 0) }}"
                    >
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">送料</div>
                  <div class="col p-1 d-flex justify-content-end align-items-center">
                    <span class="pe-1">￥</span>
                    <input
                    type="text"
                    class="w-75 form-control"
                    name="delivery_fee_total"
                    value="{{ old('delivery_fee_total') ? old('delivery_fee_total') : ($order ? $order->delivery_fee_total : 0) }}"
                    >
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">合計</div>
                  <div class="col p-1">￥{{ $order ? $order->total : 0 }}</div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">お支払い合計</div>
                  <div class="col p-1 fs-4">￥{{ $order ? $order->payment_total : 0 }}</div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">使用ポイント</div>
                  <div class="col p-1 d-flex justify-content-end align-items-center">
                    <span class="pe-1">￥</span>
                    <input
                    type="text"
                    class="w-75 form-control"
                    name="use_point"
                    value="{{ old('use_point') ? old('use_point') : ($order ? $order->use_point : '') }}"
                    >
                  </div>
                </div>
                <div class="row align-items-center">
                  <div class="col p-1">加算ポイント</div>
                  <div class="col p-1">￥{{ ($order ? $order->add_point : 0) }}</div>
                </div>
              </div>
            </div>
        </div>
      </div>

      <div class="card mb-5">
        <div class="card-header bg-dark text-white">
          その他
        </div>
        <div class="card-body">
          <div class="row align-items-center mb-3">
            <div class="col-3">
              お支払い方法
            </div>
            <div class="col-9">
              <div class="w-50">
                <select id="payment-id" class="form-select" name="payment_id">
                  @foreach($payments as $payment)
                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <x-admin.form.note
              note="note"
              :value_note="old('note') ? old('note') : ($order ? $order->note : '')"
              :rows="10"/>
          </div>
        </div>
      </div>

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col text-start">
                <a href="{{ route('admin.order.index') }}" class="text-white"> 受注一覧へ戻る</a>
              </div>
              <div class="col text-end">
                <button type="submit" class="btn btn-primary btn-md w-25">
                  登録する
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

    <form method="POST" action="{{-- route('admin.order.store') --}}" novalidate>
      <div class="modal" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="userAddModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="userAddModalLabel">会員を検索</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <search-user :token='@json(csrf_token())'></search-user>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form method="POST" action="{{-- route('admin.order.store') --}}" novalidate>
      <div class="modal" id="productAddModal" tabindex="-1" role="dialog" aria-labelledby="productAddModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="productAddModalLabel">商品を追加</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <search-product :categories='@json($categorySelect)' :token='@json(csrf_token())'></search-product>
            </div>
          </div>
        </div>
      </div>
    </form>

  </div>

</x-admin.layout>