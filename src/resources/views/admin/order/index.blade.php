<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">受注検索</h1>
    <form method="post" action="?">
      @csrf

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          注文情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-4">
              <label for="order-id">注文番号（複数対応）</label>
              <input id="order-id" type="text" class="form-control" name="order_ids" value="{{ Request::get('order_ids') }}">
            </div>
            <div class="col-4">
              <label for="order-name">名前</label>
              <input id="order-name" type="text" class="form-control" name="order_name" value="{{ Request::get('order_name') }}">
            </div>
            <div class="col-4">
              <label for="order-email">メールアドレス</label>
              <input id="order-email" type="text" class="form-control" name="order_email" value="{{ Request::get('order_email') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-4">
              <label for="order-no">注文番号（乱数）</label>
              <input id="order-no" type="text" class="form-control" name="order_no" value="{{ Request::get('order_no') }}">
            </div>
            <div class="col-4">
              <label for="order-kana">フリガナ</label>
              <input id="order-kana" type="text" class="form-control" name="order_kana" value="{{ Request::get('order_kana') }}">
            </div>
            <div class="col-4">
              <label for="order-tel">電話番号</label>
              <input id="order-tel" type="text" class="form-control" name="order_tel" value="{{ Request::get('order_tel') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-4">
              <label for="order-id">注文方法</label>
              <div class="col">
                <div class="form-check form-check-inline">
                  <input
                    id="order-how1"
                    class="form-check-input"
                    type="checkbox"
                    name="order_how[]"
                    value="1"
                    {{ is_array(Request::get('order_how')) && in_array(1, Request::get('order_how'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="order-how1">
                    web注文
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input
                    id="order-how2"
                    class="form-check-input"
                    type="checkbox"
                    name="order_how[]"
                    value="2"
                    {{ is_array(Request::get('order_how')) && in_array(2, Request::get('order_how'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="order-how2">
                    電話注文
                  </label>
                </div>
              </div>
            </div>
            <div class="col-4">
              <label for="order-name">注文回数</label>
              <div class="col">
                <div class="form-check form-check-inline">
                  <input
                    id="order-count1"
                    class="form-check-input"
                    type="checkbox"
                    name="order_count[]"
                    value="1"
                    {{ is_array(Request::get('order_count')) && in_array(1, Request::get('order_count'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="order-count1">
                    新規
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <input
                    id="order-count2"
                    class="form-check-input"
                    type="checkbox"
                    name="order_count[]"
                    value="2"
                    {{ is_array(Request::get('order_count')) && in_array(2, Request::get('order_count'))? 'checked' : '' }}
                  >
                  <label class="form-check-label" for="order-count2">
                    常連
                  </label>
                </div>
              </div>
            </div>
            <div class="col-4">
              <div class="form-row">
                <div class="form-group">
                  <label for="sex">性別</label>
                  <div class="col">
                    @foreach (config('sex') as $key => $val)
                      <div class="form-check form-check-inline">
                        <input
                          id="sex{{ $key }}"
                          class="form-check-input"
                          type="checkbox"
                          name="sex[]"
                          value="{{ $key }}"
                          {{ is_array(Request::get('sex')) && in_array($key, Request::get('sex'))? 'checked' : '' }}
                        >
                        <label class="form-check-label" for="sex{{ $key }}">
                          {{ $val }}
                        </label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="order-notein">備考欄に含む</label>
              <input id="order-notein" type="text" class="form-control" name="order_notein" value="{{ Request::get('order_notein') }}">
            </div>
            <div class="col-6">
              <label for="order-noteout">備考欄に含まない</label>
              <input id="order-noteout" type="text" class="form-control" name="order_noteout" value="{{ Request::get('order_noteout') }}">
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <label for="order-date">注文日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="order-date-start"
                    type="date"
                    class="form-control"
                    name="order_date_start"
                    value="{{ Request::get('order_date_start') }}"
                    data-target="#order-date-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="order-date-end"
                    type="date"
                    class="form-control"
                    name="order_date_end"
                    value="{{ Request::get('order_date_end') }}"
                    data-target="#order-date-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="order-product">注文商品</label>
              <input id="order-product" type="text" class="form-control" name="order_product" value="{{ Request::get('order_product') }}">
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          入金情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="pay-date">入金日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="pay-date-start"
                    type="date"
                    class="form-control"
                    name="pay_date_start"
                    value="{{ Request::get('pay_date_start') }}"
                    data-target="#pay-date-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="pay-date-end"
                    type="date"
                    class="form-control"
                    name="pay_date_end"
                    value="{{ Request::get('pay_date_end') }}"
                    data-target="#pay-date-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="pay-total">請求額</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="pay-total-start" type="text" class="form-control" name="pay_total_start" value="{{ Request::get('pay_total_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="pay-total-end" type="text" class="form-control" name="pay_total_end" value="{{ Request::get('pay_total_end') }}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          発送情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="send-date">発送日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="send-date-start"
                    type="date"
                    class="form-control"
                    name="send_date_start"
                    value="{{ Request::get('send_date_start') }}"
                    data-target="#send-date-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="send-date-end"
                    type="date"
                    class="form-control"
                    name="send_date_end"
                    value="{{ Request::get('send_date_end') }}"
                    data-target="#send-date-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="order-tracks">追跡番号（複数対応）</label>
              <input id="order-tracks" type="text" class="form-control" name="order_tracks" value="{{ Request::get('order_tracks') }}">
            </div>
          </div>
        </div>
      </div>
      
      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          会員情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-id">会員ID</label>
              <input id="user-id" type="text" class="form-control" name="user_id" value="{{ Request::get('user_id') }}">
            </div>
            <div class="col-6">
              <label for="order-first">初回注文日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="order-first-start"
                    type="date"
                    class="form-control"
                    name="order_first_start"
                    value="{{ Request::get('order_first_start') }}"
                    data-target="#order-first-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="order-first-end"
                    type="date"
                    class="form-control"
                    name="order_first_end"
                    value="{{ Request::get('order_first_end') }}"
                    data-target="#order-first-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
              <label for="user-notein">備考欄に含む</label>
              <input id="user-notein" type="text" class="form-control" name="user_notein" value="{{ Request::get('user_notein') }}">
            </div>
            <div class="col-6">
              <label for="user-noteout">備考欄に含まない</label>
              <input id="user-noteout" type="text" class="form-control" name="user_noteout" value="{{ Request::get('user_noteout') }}">
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-3 d-grid mx-auto text-center">
        <button type='submit' name="_method" value="post" formaction="{{ route('admin.order.index') }}" class="btn btn-block btn-primary btn-md">検索する</button>
      </div>

      @if(count($orders) > 0)
        <div class="row align-items-center mb-2">
          <div class="col">
            <span class="fw-bold">{{ $orders->total() }}件</span>の注文が該当しました。
          </div>
          <div class="col text-end">
            <button type='submit' name="_method" value="post" formaction="{{ route('admin.order.export') }}" class="btn btn-primary btn-sm">CSVダウンロード</button>
          </div>
        </div>
        <div class="card rounded border-0 mb-4">
          <div class="card-body p-0">
            <table class="table table-bordered table-striped">
              <thead>
                <tr class="bg-dark">
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">対応状況</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">注文日時</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">注文番号</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">回数</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">性別</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">名前</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">支払</th>
                  <th colspan="5" class="align-middle p-2 text-white fw-normal text-center">注文内容</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">使用PT</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">請求額</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">入金額</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">入金日</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">発送日</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">&nbsp;</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">&nbsp;</th>
                </tr>
                <tr class="bg-dark">
                  <th class="align-middle p-2 text-white fw-normal">商品ID</th>
                  <th class="align-middle p-2 text-white fw-normal">規格ID</th>
                  <th class="align-middle p-2 text-white fw-normal">価格</th>
                  <th class="align-middle p-2 text-white fw-normal">数</th>
                  <th class="align-middle p-2 text-white fw-normal">セット</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($orders as $order)
                  <tr>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      {{ $order->orderStatus ? $order->orderStatus->name : '' }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      {{ $order->order_date ? \Carbon\Carbon::parse($order->order_date)->format('Y年m月d日') : '----' }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      <a href="{{ route('admin.order.edit', $order->id) }}" class="link-primary">{{ $order->id }}</a>
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">回数</td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      {{ isset(config('sex')[$order->order_sex]) ? config('sex')[$order->order_sex] : '--' }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      {{ $order->order_name01 }} {{ $order->order_name02 }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">{{ $order->payment ? $order->payment->name : '' }}</td>
                      @if($order->orderDetails)
                        @foreach ($order->orderDetails as $orderDetail)
                          @if($loop->first)
                            <td class="align-middle p-2">{{ $orderDetail->product->id }}</td>
                            <td class="align-middle p-2"></td>
                            <td class="align-middle p-2">￥{{ number_format($orderDetail->price) }}</td>
                            <td class="align-middle p-2">{{ $orderDetail->quantity }}</td>
                            <td class="align-middle p-2"></td>
                          @endif
                        @endforeach
                      @endif
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      {{ $order->use_point }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      ￥{{ number_format($order->payment_total) }}
                    </td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                      ￥{{ $order->payment_sum }}
                    </td>
                    @if($order->payment_date)
                      <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                        {{ \Carbon\Carbon::parse($order->payment_date)->format('Y年m月d日') }}
                      </td>
                    @else
                      <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                        <a href="/admin/payment/{{ $order->id }}" class="btn btn-block bg-danger btn-md text-white">入金処理</a>
                      </td>
                    @endif
                    @if($order->commit_date)
                      <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                        {{ \Carbon\Carbon::parse($order->commit_date)->format('Y年m月d日') }}
                      </td>
                    @else
                      <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2">
                        <a href="/admin/tracking/{{ $order->id }}" class="btn btn-block bg-success btn-md text-white">発送処理</a>
                      </td>
                    @endif
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2"></td>
                    <td rowspan="{{ count($order->orderDetails) }}" class="align-middle p-2"></td>
                  </tr>
                  @if($order->orderDetails)
                    @foreach ($order->orderDetails as $orderDetail)
                      @if($loop->first == false)
                        <tr>
                          <td class="align-middle p-2">{{ $orderDetail->id }}</td>
                          <td class="align-middle p-2">{{ $orderDetail->id }}</td>
                          <td class="align-middle p-2">￥{{ number_format($orderDetail->price) }}</td>
                          <td class="align-middle p-2">{{ $orderDetail->quantity }}</td>
                          <td class="align-middle p-2">{{ $orderDetail->id }}</td>
                        </tr>
                      @endif
                    @endforeach
                  @endif
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="d-flex justify-content-center">
          {{ $orders->links() }}
        </div>
      @else
        <p>注文はありません</p>
      @endif

    </form>

  </div>

</x-admin.layout>