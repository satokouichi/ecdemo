<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <form method="POST" action="{{ route('two-factor.login') }}">
    @csrf
    <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('Code') }}</label>
    <input id="code" type="code" class="form-control @error('code') is-invalid @enderror" name="code" required autocomplete="current-code">
    @error('code')
      <strong>{{ $message }}</strong>
    @enderror
    <button type="submit" class="btn btn-primary">
      {{ __('Submit') }}
    </button>
  </form>

  <form method="POST" action="{{ route('two-factor.login') }}">
    @csrf
    <label for="recovery_code" class="col-md-4 col-form-label text-md-right">{{ __('Reocvery Code') }}</label>
    <input id="recovery_code" type="recovery_code" class="form-control @error('recovery_code') is-invalid @enderror" name="recovery_code" required autocomplete="current-recovery_code">
    @error('code')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
    <button type="submit" class="btn btn-primary">
      {{ __('Submit') }}
    </button>
  </form>

</x-front.layout>