<x-front.layout>
  
  <x-slot name="header">
  </x-slot>
  <x-slot name="footer">
  </x-slot>

<div class="d-flex align-items-center justify-content-center vh-100 bg-dark">
  <div class="mx-auto col-11 col-sm-7 col-md-6 col-lg-4 col-xl-3 col-xxl-3">

    <div class="fs-2 text-center text-white">会員登録</div>
    <form method="POST" action="{{ route('admin.register') }}">
      @csrf
      <div class="mb-2">
        <label for="name" class="col-form-label text-white">お名前</label>
        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
      </div>
      @error('name')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
      <div class="mb-2">
        <label for="email" class="col-form-label text-white">Eメール</label>
        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
      </div>
      @error('email')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
      <div class="mb-2">
        <label for="password" class="col-form-label text-white">パスワード</label>
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
      </div>
      @error('password')
        <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
      @enderror
      <div class="mb-4">
        <label for="password-confirm" class="col-form-label text-white">パスワード（確認）</label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
      </div>
      <button type="submit" class="btn btn-primary w-100 btn-md">
        登録する
      </button>
    </form>

  </div>
</div>

</x-front.layout>