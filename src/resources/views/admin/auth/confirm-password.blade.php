<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-8 mx-auto py-4">
    <div class="mb-3 fs-2">2要素認証</div>
    <div class="card">
      <div class="card-body">
        <form method="POST" action="{{ route('password.confirm') }}">
          @csrf
          <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
            <strong>{{ $message }}</strong>
          @enderror
          <button type="submit" class="btn btn-primary">
            {{ __('Confirm Password') }}
          </button>
          @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
              {{ __('Forgot Your Password?') }}
            </a>
          @endif
        </form>
      </div>
    </div>
  </div>

</x-front.layout>