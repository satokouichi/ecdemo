<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">規格分類</h1>
    <form method="post" action="?">
      @csrf

      <div class="row">

        <div class="col-3">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              規格分類検索
            </div>
            <div class="card-body">
            </div>
            <div class="card-footer bg-white">
            </div>
          </div>
        </div>

        <div class="col-9">
          <div class="card mb-4">
            <div class="card-header bg-dark text-white">
              規格分類情報
            </div>
            <div class="card-body">
              <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                <button type='submit' name="_method" value="post" formaction="{{ route('admin.classcategory.export') }}" class="btn btn-primary btn-sm d-inline-block">
                  CSVダウンロード
                </button>
              </div>
            </div>
          </div>
        </div>

      </div>
    
    </form>

  </div>

</x-admin.layout>