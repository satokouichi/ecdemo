<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 p-4">
  
    <h1 class="mb-3 fs-4">会員検索</h1>
    <form method="post" action="?">
      @csrf

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          会員情報
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-4">
              <label for="user-id">会員番号（複数対応）</label>
              <input id="user-id" type="text" class="form-control" name="user_ids" value="{{ Request::get('user_ids') }}">
            </div>
            <div class="col-4">
              <label for="user-name">名前</label>
              <input id="user-name" type="text" class="form-control" name="user_name" value="{{ Request::get('user_name') }}">
            </div>
            <div class="col-4">
              <label for="user-kana">フリガナ</label>
              <input id="user-kana" type="text" class="form-control" name="user_kana" value="{{ Request::get('user_kana') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-email">メールアドレス</label>
              <input id="user-email" type="text" class="form-control" name="user_email" value="{{ Request::get('user_email') }}">
            </div>
            <div class="col-6">
              <label for="user-point">所持ポイント</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="user-point-start" type="text" class="form-control" name="user_point_start" value="{{ Request::get('user_point_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="user-point-end" type="text" class="form-control" name="user_point_end" value="{{ Request::get('user_point_end') }}">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-notein">備考欄に含む</label>
              <input id="user-notein" type="text" class="form-control" name="user_notein" value="{{ Request::get('user_notein') }}">
            </div>
            <div class="col-6">
              <label for="user-noteout">備考欄に含まない</label>
              <input id="user-noteout" type="text" class="form-control" name="user_noteout" value="{{ Request::get('user_noteout') }}">
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-create">登録日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-create-start"
                    type="date"
                    class="form-control"
                    name="user_create_start"
                    value="{{ Request::get('user_create_start') }}"
                    data-target="#user-create-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-create-end"
                    type="date"
                    class="form-control"
                    name="user_create_end"
                    value="{{ Request::get('user_create_end') }}"
                    data-target="#user-create-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="user-date">更新日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-update-start"
                    type="date"
                    class="form-control"
                    name="user_update_start"
                    value="{{ Request::get('user_update_start') }}"
                    data-target="#user-update-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-update-end"
                    type="date"
                    class="form-control"
                    name="user_update_end"
                    value="{{ Request::get('user_update_end') }}"
                    data-target="#user-update-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <div class="form-row">
                <div class="form-group">
                  <label for="sex">メルマガ受信設定</label>
                  <div class="col">
                    @foreach (config('mailmaga_flg') as $key => $val)
                      <div class="form-check form-check-inline">
                        <input
                          id="user-mailmaga{{ $key }}"
                          class="form-check-input"
                          type="radio"
                          name="mailmaga_flg"
                          value="{{ $key }}"
                          {{ Request::get('mailmaga_flg') == $key ? 'checked' : '' }}
                        >
                        <label class="form-check-label" for="user-mailmaga{{ $key }}">
                          {{ $val }}
                        </label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="form-row">
                <div class="form-group">
                  <label for="sex">性別</label>
                  <div class="col">
                    @foreach (config('sex') as $key => $val)
                      <div class="form-check form-check-inline">
                        <input
                          id="sex{{ $key }}"
                          class="form-check-input"
                          type="checkbox"
                          name="sex[]"
                          value="{{ $key }}"
                          {{ is_array(Request::get('sex')) && in_array($key, Request::get('sex'))? 'checked' : '' }}
                        >
                        <label class="form-check-label" for="sex{{ $key }}">
                          {{ $val }}
                        </label>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          注文履歴
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-firstorder">初回注文日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-firstorder-start"
                    type="date"
                    class="form-control"
                    name="user_firstorder_start"
                    value="{{ Request::get('user_firstorder_start') }}"
                    data-target="#user-firstorder-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-firstorder-end"
                    type="date"
                    class="form-control"
                    name="user_firstorder_end"
                    value="{{ Request::get('user_firstorder_end') }}"
                    data-target="#user-firstorder-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="user-ordercount">累積注文回数</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="user-ordercount-start" type="text" class="form-control" name="user_ordercount_start" value="{{ Request::get('user_ordercount_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="user-ordercount-end" type="text" class="form-control" name="user_ordercount_end" value="{{ Request::get('user_ordercount_end') }}">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-lastorder">最終注文日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-lastorder-start"
                    type="date"
                    class="form-control"
                    name="user_lastorder_start"
                    value="{{ Request::get('user_lastorder_start') }}"
                    data-target="#user-lastorder-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-lastorder-end"
                    type="date"
                    class="form-control"
                    name="user_lastorder_end"
                    value="{{ Request::get('user_lastorder_end') }}"
                    data-target="#user-lastorder-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="user-orderpay">累積注文金額</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="user-orderpay-start" type="text" class="form-control" name="user_orderpay_start" value="{{ Request::get('user_orderpay_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="user-orderpay-end" type="text" class="form-control" name="user_orderpay_end" value="{{ Request::get('user_orderpay_end') }}">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-orderall">注文日ALL</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-orderall-start"
                    type="date"
                    class="form-control"
                    name="user_orderall_start"
                    value="{{ Request::get('user_orderall_start') }}"
                    data-target="#user-orderall-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-orderall-end"
                    type="date"
                    class="form-control"
                    name="user_orderall_end"
                    value="{{ Request::get('user_orderall_end') }}"
                    data-target="#user-orderall-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
            </div>
          </div>          
        </div>
      </div>
     
      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          入金履歴
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-firstpay">初回入金日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-firstpay-start"
                    type="date"
                    class="form-control"
                    name="user_firstpay_start"
                    value="{{ Request::get('user_firstpay_start') }}"
                    data-target="#user-firstpay-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-firstpay-end"
                    type="date"
                    class="form-control"
                    name="user_firstpay_end"
                    value="{{ Request::get('user_firstpay_end') }}"
                    data-target="#user-firstpay-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="user-paycount">累積入金回数</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="user-paycount-start" type="text" class="form-control" name="user_paycount_start" value="{{ Request::get('user_paycount_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="user-paycount-end" type="text" class="form-control" name="user_paycount_end" value="{{ Request::get('user_paycount_end') }}">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-lastpay">最終入金日</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-lastpay-start"
                    type="date"
                    class="form-control"
                    name="user_lastpay_start"
                    value="{{ Request::get('user_lastpay_start') }}"
                    data-target="#user-lastpay-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-lastpay-end"
                    type="date"
                    class="form-control"
                    name="user_lastpay_end"
                    value="{{ Request::get('user_lastpay_end') }}"
                    data-target="#user-lastpay-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
              <label for="user-paypay">累積入金金額</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input id="user-paypay-start" type="text" class="form-control" name="user_paypay_start" value="{{ Request::get('user_paypay_start') }}">
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input id="user-paypay-end" type="text" class="form-control" name="user_paypay_end" value="{{ Request::get('user_paypay_end') }}">
                </div>
              </div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-payall">入金日ALL</label>
              <div class="row g-2 align-items-center">
                <div class="col">
                  <input
                    id="user-payall-start"
                    type="date"
                    class="form-control"
                    name="user_payall_start"
                    value="{{ Request::get('user_payall_start') }}"
                    data-target="#user-payall-start"
                    data-toggle="datetimepicker"
                  >
                </div>
                <div class="col-auto text-center">〜</div>
                <div class="col">
                  <input
                    id="user-payall-end"
                    type="date"
                    class="form-control"
                    name="user_payall_end"
                    value="{{ Request::get('user_payall_end') }}"
                    data-target="#user-payall-end"
                    data-toggle="datetimepicker"
                  >
                </div>
              </div>
            </div>
            <div class="col-6">
            </div>
          </div>          
        </div>
      </div>

      <div class="card mb-4 border-0 shadow-sm">
        <div class="card-header bg-dark text-white">
          注文商品
        </div>
        <div class="card-body">
          <div class="row mb-2">
            <div class="col-6">
              <label for="user-product">注文商品名</label>
              <input id="user-product" type="text" class="form-control" name="user_product" value="{{ Request::get('user_product') }}">
            </div>
            <div class="col-6">
              <label for="user_category">注文商品のカテゴリ</label>
              <select class="form-select" name="user_category">
                @foreach ($categorySelect as $key => $val)
                  @if ($loop->index === 0)
                    <option value="">選択してください</option>
                  @endif
                  <option
                  value="{{ $key }}"
                  {{ Request::get('user_category') == $key ? 'selected' : '' }}
                  >{{ $val }}
                  </option>
                @endforeach 
              </select>
            </div>
          </div>
        </div>
      </div>

      <div class="col-3 d-grid mx-auto text-center">
        <button type='submit' name="_method" value="post" formaction="{{ route('admin.user.index') }}" class="btn btn-block btn-primary btn-md">検索する</button>
      </div>

      @if(count($users) > 0)
        <div class="row align-items-center mb-2">
          <div class="col">
            <span class="fw-bold">{{ $users->total() }}件</span>の会員が該当しました。
          </div>
          <div class="col text-end">
            <button type='submit' name="_method" value="post" formaction="{{ route('admin.user.export') }}" class="btn btn-primary btn-sm">CSVダウンロード</button>
          </div>
        </div>
        <div class="card rounded border-0 mb-4">
          <div class="card-body p-0">
            <table class="table table-bordered table-striped">
              <thead>
                <tr class="bg-dark">
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">会員ID</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">登録日時</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">都道府県</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">名前</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">生年月日</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">性別</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">所持PT</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">最終注文日時</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">メルマガ</th>
                  <th rowspan="2" class="align-middle p-2 text-white fw-normal">備考欄</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($users as $user)
                  <tr>
                    <td class="align-middle p-2">
                      <a href="{{ route('admin.user.edit', $user->id) }}" class="link-primary">{{ $user->id }}</a>
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->create_date ? \Carbon\Carbon::parse($user->create_date)->format('Y年m月d日') : '----' }}
                    </td>
                    <td class="align-middle p-2">
                      {{ isset(config('pref')[$user->pref]) ? config('pref')[$user->pref] : '--' }}
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->name01 }} {{ $user->name02 }}
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->birth ? \Carbon\Carbon::parse($user->birth)->format('Y年m月d日') : '----' }}
                    </td>
                    <td class="align-middle p-2">
                      {{ isset(config('sex')[$user->sex]) ? config('sex')[$user->sex] : '--' }}
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->point }}
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->last_buy_date ? \Carbon\Carbon::parse($user->last_buy_date)->format('Y年m月d日') : '----' }}
                    </td>
                    <td class="align-middle p-2">
                      個人宛
                    </td>
                    <td class="align-middle p-2">
                      {{ $user->note }}
                    </td>
                  </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="d-flex justify-content-center">
          {{ $users->links() }}
        </div>
      @else
        <p>会員はいません</p>
      @endif

    </form>

  </div>

</x-admin.layout>