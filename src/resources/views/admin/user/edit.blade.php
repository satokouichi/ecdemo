<x-admin.layout>
  
  <x-slot name="header">
    <x-admin.layouts.header></x-admin.layouts.header>
  </x-slot>
  
  <x-admin.layouts.side></x-admin.layouts.side>

  <div class="col-10 mb-5 p-4">

    <x-admin.layouts.flush></x-admin.layouts.flush>
  
    <h1 class="mb-3 fs-4">
      @if ($user) 
        会員編集
      @else
        会員登録
      @endif
    </h1>

    <form method="POST" action="{{ route('admin.user.store') }}" class="pb-5" novalidate>
      @csrf

      <input type="hidden" name="id" value="{{ $user ? $user->id : '' }}">
  
      <div class="card mb-4">
        <div class="card-header bg-dark text-white">
          会員情報
        </div>
        <div class="card-body">
          @if ($user)
            <div class="row align-items-center mb-3">
              <div class="col-3">
                会員ID
              </div>
              <div class="col-auto">
                {{ $user->id }}
              </div>
            </div>
          @endif
          <div class="row align-items-center mb-3">
            <x-admin.form.name
              name01="name01"
              name02="name02"
              :value_name01="old('name01') ? old('name01') : ($user ? $user->name01 : '')"
              :value_name02="old('name02') ? old('name02') : ($user ? $user->name02 : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.kana
              kana01="kana01"
              kana02="kana02"
              :value_kana01="old('kana01') ? old('kana01') : ($user ? $user->kana01 : '')"
              :value_kana02="old('kana02') ? old('kana02') : ($user ? $user->kana02 : '')"/>
          </div>
          <div class="row mb-3">
            <div class="col-3">
              住所
            </div>
            <div class="col-6">
              <div class="row w-50 g-2 align-items-center mb-3">
                <div class="col-1 text-center">〒</div>
                <div class="col-4">
                  <x-admin.form.zip01
                    zip01="zip01"
                    :value_zip01="old('zip01') ? old('zip01') : ($user ? $user->zip01 : '')"/>
                </div>
                <div class="col-1 text-center">-</div>
                <div class="col-4">
                  <x-admin.form.zip02
                    zip02="zip02"
                    :value_zip02="old('zip02') ? old('zip02') : ($user ? $user->zip02 : '')"/>
                </div>
                <x-admin.form.errors
                  name="zip01"/>
                <x-admin.form.errors
                  name="zip02"/>
              </div>
              <div class="w-50 mb-3">
                <x-admin.form.pref
                  pref="pref"
                  :select="config('pref')"
                  :value_pref="old('pref') ? old('pref') : ($user ? $user->pref : '')"/>
              </div>
              <div class="mb-3">
                <x-admin.form.addr01
                  addr01="addr01"
                  :value_addr01="old('addr01') ? old('addr01') : ($user ? $user->addr01 : '')"/>
                <x-admin.form.errors
                  name="addr01"/>
              </div>
              <div class="">
                <x-admin.form.addr02
                  addr02="addr02"
                  :value_addr02="old('addr02') ? old('addr02') : ($user ? $user->addr02 : '')"/>
                <x-admin.form.errors
                  name="addr02"/>
              </div>
            </div>
          </div>        
          <div class="row align-items-center mb-3">
            <x-admin.form.email
              email="email"
              :value_email="old('email') ? old('email') : ($user ? $user->email : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.sex
              sex="sex"
              :select="config('sex')"
              :value_sex="old('sex') ? old('sex') : ($user ? $user->sex : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.birth
              birth="birth"
              :value_birth="old('birth') ? old('birth') : ($user ? \Carbon\Carbon::parse($user->birth)->format('Y-m-d') : '')"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.password
              password="password"
              :value_password="$user ? config('params')['password_default'] : ''"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.password-confirm
              password="password_confirmation"
              :value_password_confirm="$user ? config('params')['password_default'] : ''"/>
          </div>
          <div class="row align-items-center mb-3">
            <x-admin.form.mailmaga-flg
              mailmaga_flg="mailmaga_flg"
              :select="config('mailmaga_flg')"
              :value_mailmaga_flg="old('mailmaga_flg') ? old('mailmaga_flg') : ($user ? $user->mailmaga_flg : '')"/>
          </div>
          <div class="row mb-3">
            <x-admin.form.note
              note="note"
              :value_note="old('note') ? old('note') : ($user ? $user->note : '')"
              :rows="10"/>
          </div>
          <div class="row align-items-center mb-0">
            <x-admin.form.point
              point="point"
              :value_point="old('point') ? old('point') : ($user ? $user->point : '')"/>
          </div>        
        </div>
      </div>

      @if ($user) 
        <div class="card">
          <div class="card-header bg-dark text-white">
            注文履歴
          </div>
          <div class="card-body">
            @if(count($orders) > 0)
              <table class="table">
                <thead>
                  <tr>
                    <th class="align-middle p-2 fw-normal">日付</th>
                    <th class="align-middle p-2 fw-normal">注文番号</th>
                    <th class="align-middle p-2 fw-normal">ステータス</th>
                    <th class="align-middle p-2 fw-normal">請求金額</th>
                    <th class="align-middle p-2 fw-normal">入金金額</th>
                    <th class="align-middle p-2 fw-normal">入金日</th>
                    <th class="align-middle p-2 fw-normal">発送日</th>
                    <th class="align-middle p-2 fw-normal">支払方法</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($orders as $order)
                  <tr>
                    <td>{{ $order->order_date ? \Carbon\Carbon::parse($order->order_date)->format('Y年m月d日') : '----' }}</td>
                    <td><a href="{{ route('admin.order.edit', $order->id) }}" class="link-primary">{{ $order->id }}</a></td>
                    <td>{{ $order->orderStatus ? $order->orderStatus->name : '' }}</td>
                    <td>￥{{ $order->payment_total }}</td>
                    <td>￥{{ $order->payment_sum }}</td>
                    <td>{{ $order->payment_date ? \Carbon\Carbon::parse($order->payment_date)->format('Y年m月d日') : '----' }}</td>
                    <td>{{ $order->commit_date ? \Carbon\Carbon::parse($order->commit_date)->format('Y年m月d日') : '----' }}</td>
                    <td>{{ $order->payment_method }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            @else
              <p>注文履歴ありません</p>
            @endif
          </div>
        </div>
      @endif

      <div class="footer fixed-bottom text-center">
        <div class="d-flex">
          <div class="col-2 py-2 bg-white">
          </div>
          <div class="col-10 py-2 px-3 bg-secondary">
            <div class="row align-items-center">
              <div class="col-9 text-start">
                <a href="{{ route('admin.user.index') }}" class="text-white"> 会員一覧へ戻る</a>
              </div>
              <div class="col-3 text-end">
                <div class="row g-2 align-items-center">
                  <div class="col-6">
                    <x-admin.form.status
                      status="status"
                      :select="config('status')"
                      :value_status="old('status') ? old('status') : ($user ? $user->status : '')"/>
                  </div>
                  <div class="col-6">
                    <button type="submit" class="btn btn-primary btn-md w-100">
                      登録する
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </form>

  </div>

</x-admin.layout>