<nav class="navbar navbar-expand-lg navbar-dark">
  <a class="navbar-brand" href="/">DEMO</a>
  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#切り替え表示されるコンテンツ名" aria-controls="切り替え表示されるコンテンツ名" aria-expanded="false" aria-label="ナビゲーション切替">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="切り替え表示されるコンテンツ名">
    <ul class="navbar-nav me-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('mypage.index') }}">Myページ</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('cart.index') }}">カート</a>
      </li>
      @if ($userid) 
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('mypage.twofactor') }}">2要素認証</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            ログアウト
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </li>
      @else
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('login') }}">ログイン</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="{{ route('register') }}">新規登録</a>
        </li>
      @endif
    </ul>
    <form class="row gx-2 my-2 my-lg-0" method="GET" action="{{ route('product.search') }}">
      <div class="col-auto">
        <input class="form-control me-sm-2" type="search" name="keyword" value="{{ Request::get('keyword') }}" placeholder="商品名" aria-label="search">
      </div>
      <div class="col-auto">
        <button class="btn btn-light my-2 my-sm-0" type="submit">検索</button>
      </div>
    </form>
  </div>
</nav>
