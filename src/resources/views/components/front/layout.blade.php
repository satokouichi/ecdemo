<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex,nofollow">

    <title>DEMO</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('assets/front/css/app.css') }}">
  </head>
  <body>
    
    <div id="app">
      <header class="container-fluid">
        <div class="row">
          {{ $header }}
        </div>
      </header>
      @if(\Request::is('/'))
        <img class="w-100" src="http://via.placeholder.com/2000x600" alt="">
      @endif
      <main class="container-fluid">
        <div class="row">
          {{ $slot }}
        </div>
      </main>
      <footer class="container-fluid">
        <div class="row">
          {{ $footer }}
        </div>
      </footer>
    </div>

    <!-- Scripts -->
    <script src="{{ mix('assets/front/js/app.js') }}" defer></script>

  </body>
</html>
