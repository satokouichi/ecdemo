<div class="col-3">
  <span class="pe-2">コメント</span><span class="px-1 rounded bg-primary text-white">必須</span>
</div>
<div class="col-9">
  <x-admin.form.input-textarea
    :name="$comment"
    :value="$valueComment"
    :rows="$rows"/>
  <x-admin.form.errors
    :name="$comment"/>
</div>

