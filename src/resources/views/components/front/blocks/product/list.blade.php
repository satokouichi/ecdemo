@props([
   'listProducts' => []
   ])

@if(count($listProducts) > 0)
  <div class="row row-cols-1 row-cols-md-4 g-4">
    @foreach($listProducts as $listProduct)
      <div class="col">
        <a href="/products/{{ $listProduct['id'] }}">
          <div class="card h-100 border-0 shadow-sm">
            <img class="card-img-top" src="http://via.placeholder.com/300x200" alt="">
            <div class="card-body">
              <div class="card-title">{{ $listProduct['name'] }}</div>
              @if(isset($listProduct['productClasses']))
                @foreach($listProduct['productClasses'] as $productClass)
                  @if(isset($listProduct['categories']))
                    @foreach($listProduct['categories'] as $category)
                      <div class="card-subtitle">{{ $category['category_name'] }}</div>
                    @endforeach
                  @endif
                  @if($productClass['default_flg'] == 1)
                    <div class="card-text">{{ number_format($productClass['price02']) }}</div>
                  @endif
                @endforeach
              @endif
            </div>
          </div>
        </a>
      </div>
    @endforeach
  </div>
  <div class="d-flex justify-content-center mt-4">
    @if(Request::get('keyword'))
      {{ $listProducts->appends(['keyword' => Request::get('keyword')])->links() }}
    @else
      {{ $listProducts->links() }}
    @endif
  </div>
@else
  <p>検索条件に一致した商品はありません</p>
@endif
