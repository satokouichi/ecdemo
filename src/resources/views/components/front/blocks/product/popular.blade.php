@props([
   'popularProducts' => []
   ])

@if(count($popularProducts) > 0)
  <div class="row row-cols-1 row-cols-md-4 g-4">
    @foreach($popularProducts as $popularProduct)
      <div class="col">
        <a href="/products/{{ $popularProduct['id'] }}">
          <div class="card h-100 border-0 shadow-sm">
            <img class="card-img-top" src="http://via.placeholder.com/300x200" alt="">
            <div class="card-body">
              <div class="card-title">{{ $popularProduct['name'] }}</div>
              @if(isset($popularProduct['productClasses']))
                @foreach($popularProduct['productClasses'] as $productClass)
                  @if(isset($popularProduct['categories']))
                    @foreach($popularProduct['categories'] as $category)
                      <div class="card-subtitle">{{ $category['category_name'] }}</div>
                    @endforeach
                  @endif
                  @if($productClass['default_flg'] == 1)
                    <div class="card-text">{{ number_format($productClass['price02']) }}</div>
                  @endif
                @endforeach
              @endif
            </div>
          </div>
        </a>
      </div>
    @endforeach
  </div>
@endif