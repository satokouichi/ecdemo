<div class="col-2 sticky-top vh-100 side bg-white p-0">
  <div class="accordion accordion-flush" id="accordionFlushExample">
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOrder" aria-expanded="false" aria-controls="flush-collapseOrder">
      受注管理
    </div>
    <div id="flush-collapseOrder" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.order') ? 'show' : '' }}" aria-labelledby="flush-headingOrder" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.order.index') }}" class="{{ Route::currentRouteName() == 'admin.order.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">受注一覧</a>
      <a href="{{ route('admin.order.create') }}" class="{{ Route::currentRouteName() == 'admin.order.create' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">受注登録</a>
    </div>
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseUser" aria-expanded="false" aria-controls="flush-collapseUser">
      会員管理
    </div>
    <div id="flush-collapseUser" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.user') ? 'show' : '' }}" aria-labelledby="flush-headingUser" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.user.index') }}" class="{{ Route::currentRouteName() == 'admin.user.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">会員一覧</a>
      <a href="{{ route('admin.user.create') }}" class="{{ Route::currentRouteName() == 'admin.user.create' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">会員登録</a>
    </div>
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseProduct" aria-expanded="false" aria-controls="flush-collapseProduct">
      商品管理
    </div>
    <div id="flush-collapseProduct" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.product') ? 'show' : '' }}" aria-labelledby="flush-headingProduct" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.product.index') }}" class="{{ Route::currentRouteName() == 'admin.product.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">商品一覧</a>
      <a href="{{ route('admin.product.csv') }}" class="{{ Route::currentRouteName() == 'admin.product.csv' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">CSV登録</a>
    </div>
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseClassCategory" aria-expanded="false" aria-controls="flush-collapseClassCategory">
      規格分類管理
    </div>
    <div id="flush-collapseClassCategory" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.classcategory') ? 'show' : '' }}" aria-labelledby="flush-headingClassCategory" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.classcategory.index') }}" class="{{ Route::currentRouteName() == 'admin.classcategory.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">一覧</a>
      <a href="{{ route('admin.classcategory.csv') }}" class="{{ Route::currentRouteName() == 'admin.classcategory.csv' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">CSV登録</a>
    </div>
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseCategory" aria-expanded="false" aria-controls="flush-collapseCategory">
      カテゴリ管理
    </div>
    <div id="flush-collapseCategory" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.category') ? 'show' : '' }}" aria-labelledby="flush-headingCategory" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.category.index') }}" class="{{ Route::currentRouteName() == 'admin.category.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">一覧</a>
      <a href="{{ route('admin.category.csv') }}" class="{{ Route::currentRouteName() == 'admin.category.csv' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">CSV登録</a>
    </div>
    <div class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseReview" aria-expanded="false" aria-controls="flush-collapseReview">
      レビュー管理
    </div>
    <div id="flush-collapseReview" class="accordion-collapse collapse {{ strpos(Route::currentRouteName(), '.review') ? 'show' : '' }}" aria-labelledby="flush-headingReview" data-bs-parent="#accordionFlushExample">
      <a href="{{ route('admin.review.index') }}" class="{{ Route::currentRouteName() == 'admin.review.index' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">検索</a>
      <a href="{{ route('admin.review.create') }}" class="{{ Route::currentRouteName() == 'admin.review.create' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">レビュー登録</a>
      <a href="{{ route('admin.review.csv') }}" class="{{ Route::currentRouteName() == 'admin.review.csv' ? 'fw-bold' : '' }} d-block accordion-body border-bottom">CSV登録</a>
    </div>
  </div>
</div>