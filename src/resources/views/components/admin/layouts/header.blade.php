<div class="col d-flex align-items-center p-2">
  <a class="fs-5 text-white" href="/admin">DEMO ADMIN</a>
</div>
<div class="col d-flex align-items-center justify-content-end p-2">
  <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
    @csrf
  </form>
  <a class="text-white" href="{{ route('admin.logout') }}"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();">
    ログアウト
  </a>
</div>
