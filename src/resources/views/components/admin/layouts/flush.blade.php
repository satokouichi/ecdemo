@if (session('flash_message'))
  <div class="mb-3 p-3 bg-success text-white rounded-2">
    {{ session('flash_message') }}
  </div>
@endif
@if (session('error_message'))
  <div class="mb-3 p-3 bg-danger text-white rounded-2">
    {{ session('error_message') }}
  </div>
@endif