<select class="form-select" name="{{ $pref }}">
  @foreach($select as $key => $val)
    <option value="{{ $key }}" {{ $valuePref == $key ? "selected" : "" }}>{{ $val }}</option>
  @endforeach 
</select>