<div class="col-3">
  メルマガ受信設定
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      @foreach ($select as $key => $val)
        <div class="form-check form-check-inline">
          <input
          id="mailmaga-flg{{ $key }}"
          class="form-check-input"
          type="radio"
          name="{{ $mailmagaFlg }}"
          value="{{ $key }}"
          {{ $valueMailmagaFlg == $key ? "checked" : "" }}
          >
          <label class="form-check-label" for="mailmaga-flg{{ $key }}">
            {{ $val }}
          </label>
        </div>
      @endforeach
      <x-admin.form.errors
        :name="$valueMailmagaFlg"/>
    </div>
  </div>
</div>
