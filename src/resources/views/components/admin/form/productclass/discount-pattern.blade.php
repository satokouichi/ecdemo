<select class="form-select" name="{{ $discountPattern }}">
  @foreach($select as $key => $val)
    <option value="{{ $val }}" {{ $valueDiscountPattern == $val ? "selected" : "" }}>{{ $val }}</option>
  @endforeach 
</select>