@if ($errors->has($name))
  @foreach($errors->get($name) as $message)
    <div class="text-danger">{{ $message }}</div>
  @endforeach
@endif
