<div class="col-3">
  メールアドレス
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        :name="$email"
        :value="$valueEmail"/>
      <x-admin.form.errors
        :name="$email"/>
    </div>
  </div>
</div>
