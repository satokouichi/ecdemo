<div class="col-3">
  お名前
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        :name="$name01"
        :value="$valueName01"/>
      <x-admin.form.errors
        :name="$name01"/>
    </div>
    <div class="col">
      <x-admin.form.input-text
        :name="$name02"
        :value="$valueName02"/>
      <x-admin.form.errors
        :name="$name02"/>
    </div>
  </div>
</div>

