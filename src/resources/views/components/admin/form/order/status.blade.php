<select class="form-select" name="status">
  @foreach($select as $val)
    <option value="{{ $val['id'] }}" {{ $valueStatus == $val['id'] ? "selected" : "" }}>{{ $val['name'] }}</option>
  @endforeach 
</select>
