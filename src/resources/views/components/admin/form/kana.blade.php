<div class="col-3">
  フリナガ
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        :name="$kana01"
        :value="$valueKana01"/>
      <x-admin.form.errors
        :name="$kana01"/>
    </div>
    <div class="col">
      <x-admin.form.input-text
        :name="$kana02"
        :value="$valueKana02"/>
      <x-admin.form.errors
        :name="$kana02"/>
    </div>
  </div>
</div>

