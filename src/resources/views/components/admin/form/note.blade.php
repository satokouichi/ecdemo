<div class="col-3">
  備考欄
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$note"
        :value="$valueNote"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$note"/>
    </div>
  </div>
</div>
