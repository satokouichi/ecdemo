<div class="col-3">
  発送業者
</div>
<div class="col-3">
  <div class="row">
    <div class="col">
      <select class="form-select" name="{{ $trader }}">
        @foreach($select as $key => $val)
          <option
            value="{{ $key }}"
            {{ $valueTrader == $key ? "selected" : "" }}>
            {{ $val }}
          </option>
        @endforeach 
      </select>
      <x-admin.form.errors
        :name="$trader"/>
    </div>
  </div>
</div>