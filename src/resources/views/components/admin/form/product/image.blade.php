<div class="col-3">
  <span class="pe-2">画像</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <div class="mb-2">
        @if($images)
          @foreach($images as $image)
            {{ $image->file_name }}
          @endforeach
        @else
          画像は登録されていません
        @endif
      </div>
      <div class="mb-2">画像ファイル名をカンマ区切りで入力してください</div>
      <x-admin.form.input-textarea
        :name="$image"
        :value="$valueImage"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$image"/>
      <div class="mb-2">画像ALT名をカンマ区切りで入力してください</div>
      <x-admin.form.input-textarea
        :name="$alt"
        :value="$valueAlt"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$alt"/>
    </div>
  </div>
</div>
