<div class="col-3">
  <span class="pe-2">詳細</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$descriptionDetail"
        :value="$valueDescriptionDetail"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$descriptionDetail"/>
    </div>
  </div>
</div>
