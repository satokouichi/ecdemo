<div class="col-3">
  <span class="pe-2">概要</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$descriptionList"
        :value="$valueDescriptionList"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$descriptionList"/>
    </div>
  </div>
</div>
