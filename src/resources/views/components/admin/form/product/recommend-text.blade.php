<div class="col-3">
  <span class="pe-2">おすすめ</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$recommendText"
        :value="$valueRecommendText"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$recommendText"/>
    </div>
  </div>
</div>
