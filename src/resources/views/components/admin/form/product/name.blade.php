<div class="col-3">
  <span class="pe-2">商品名</span><span class="px-1 rounded bg-primary text-white">必須</span>
</div>
<div class="col-9">
  <x-admin.form.input-text
    :name="$name"
    :value="$valueName"/>
  <x-admin.form.errors
    :name="$name"/>
</div>
