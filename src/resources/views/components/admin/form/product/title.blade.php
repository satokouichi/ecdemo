<div class="col-3">
  <span class="pe-2">タイトル</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$title"
        :value="$valueTitle"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$title"/>
    </div>
  </div>
</div>
