<div class="col-3">
  <span class="pe-2">検索ワード</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$searchWord"
        :value="$valueSearchWord"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$searchWord"/>
    </div>
  </div>
</div>
