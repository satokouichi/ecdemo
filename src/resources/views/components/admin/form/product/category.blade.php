<div class="col-3">
  <span class="pe-2">カテゴリID</span><span class="px-1 rounded bg-primary text-white">必須</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        :name="$category"
        :value="$valueCategory"/>
      <x-admin.form.errors
        :name="$category"/>
    </div>
  </div>
</div>
