<div class="col-3">
  <span class="pe-2">ディスクリプション</span>
</div>
<div class="col-9">
  <div class="row">
    <div class="col">
      <x-admin.form.input-textarea
        :name="$description"
        :value="$valueDescription"
        :rows="$rows"/>
      <x-admin.form.errors
        :name="$description"/>
    </div>
  </div>
</div>
