<input
  id="create-date"
  type="date"
  class="form-control"
  name="{{ $createDate }}"
  value="{{ $valueCreateDate }}"
  data-target="#create-date"
  data-toggle="datetimepicker"
/>
<x-admin.form.errors
  name="{{ $createDate }}"/>


