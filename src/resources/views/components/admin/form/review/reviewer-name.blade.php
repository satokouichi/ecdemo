<div class="col-3">
  <span class="pe-2">投稿者名</span>
</div>
<div class="col-9">
  <x-admin.form.input-text
    :name="$reviewerName"
    :value="$valueReviewerName"/>
  <x-admin.form.errors
    :name="$reviewerName"/>
</div>

