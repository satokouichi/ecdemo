<div class="col-3">
  画像表示ステータス
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      @foreach ($select as $key => $val)
        <div class="form-check form-check-inline">
          <input
          id="image-status{{ $key }}"
          class="form-check-input"
          type="radio"
          name="{{ $imageStatus }}"
          value="{{ $key }}"
          {{ $valueImageStatus == $key ? "checked" : "" }}
          >
          <label class="form-check-label" for="image-status{{ $key }}">
            {{ $val }}
          </label>
        </div>
      @endforeach    
    </div>
    <x-admin.form.errors
        name="{{ $imageStatus }}"/>
  </div>
</div>