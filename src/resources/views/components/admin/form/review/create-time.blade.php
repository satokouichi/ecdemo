<input
  id="create-time"
  type="time"
  class="form-control"
  name="{{ $createTime }}"
  value="{{ $valueCreateTime }}"
  data-target="#create-time"
  data-toggle="datetimepicker"
/>

