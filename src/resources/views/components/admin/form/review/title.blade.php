<div class="col-3">
  <span class="pe-2">タイトル</span><span class="px-1 rounded bg-primary text-white">必須</span>
</div>
<div class="col-9">
  <x-admin.form.input-text
    :name="$title"
    :value="$valueTitle"/>
  <x-admin.form.errors
    :name="$title"/>
</div>

