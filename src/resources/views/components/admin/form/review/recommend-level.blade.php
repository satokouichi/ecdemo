<div class="col-3">
  <span class="pe-2">おすすめレベル</span>
</div>
<div class="col-9">
  <select class="form-select" name="{{ $recommendLevel }}">
    @foreach($select as $key => $val)
      <option value="{{ $key }}" {{ $valueRecommendLevel == $key ? "selected" : "" }}>{{ $val }}</option>
    @endforeach 
  </select>
</div>
