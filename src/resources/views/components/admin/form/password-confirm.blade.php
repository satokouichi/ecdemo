<div class="col-3">
  パスワード(確認用)
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        type="password"
        :name="$password"
        :value="$valuePasswordConfirm"/>
    </div>
  </div>
</div>
