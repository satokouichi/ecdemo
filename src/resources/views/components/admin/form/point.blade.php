<div class="col-3">
  所持ポイント
</div>
<div class="col-2">
  <div class="row align-items-center">
    <div class="col">
      <x-admin.form.input-text
        :name="$point"
        :value="$valuePoint"/>
      <x-admin.form.errors
        :name="$point"/>
    </div>
  </div>
</div>
