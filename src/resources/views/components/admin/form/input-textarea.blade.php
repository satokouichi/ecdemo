<textarea
  class="form-control mb-2"
  name="{{ $name }}"
  value="{{ $value ?? '' }}"
  rows="{{ $rows ?? '5' }}"
  ></textarea>
