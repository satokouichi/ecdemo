<div class="col-3">
  パスワード
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      <x-admin.form.input-text
        type="password"
        :name="$password"
        :value="$valuePassword"/>
    </div>
  </div>
</div>
<x-admin.form.errors
  name="addr02"/>
