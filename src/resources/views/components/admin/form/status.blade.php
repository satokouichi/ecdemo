<select class="form-select" name="status">
  @foreach($select as $key => $val)
    <option value="{{ $key }}" {{ $valueStatus == $key ? "selected" : "" }}>{{ $val }}</option>
  @endforeach 
</select>
