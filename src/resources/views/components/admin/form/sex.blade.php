<div class="col-3">
  性別
</div>
<div class="col-6">
  <div class="row">
    <div class="col">
      @foreach ($select as $key => $val)
        <div class="form-check form-check-inline">
          <input
          id="sex{{ $key }}"
          class="form-check-input"
          type="radio"
          name="sex"
          value="{{ $key }}"
          {{ $valueSex == $key ? "checked" : "" }}
          >
          <label class="form-check-label" for="sex{{ $key }}">
            {{ $val }}
          </label>
        </div>
      @endforeach    
    </div>
    <x-admin.form.errors
        :name="$sex"/>
  </div>
</div>