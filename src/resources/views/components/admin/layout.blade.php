<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>DEMO</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('assets/admin/css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ mix('assets/admin/js/app.js') }}" defer></script>
  </head>
  <body>
    <div id="app">
      <header class="container-fluid">
        <div class="row">
          {{ $header }}
        </div>
      </header>
      <main class="container-fluid">
        <div class="row">
          {{ $slot }}
        </div>
      </main>
    </div>
  </body>
</html>
