<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.footer></x-front.layouts.footer>
  </x-slot>
  <x-slot name="right"></x-slot>

  <x-front.layouts.side></x-front.layouts.side>

  <div class="col-10 p-4">

    <div class="card mb-5 border-0 shadow-sm">
      <div class="card-body">
        <form method="POST" action="{{ route('cart.store') }}">
          @csrf
          <product-detail :product='@json($detailProduct)' :id='@json(1)'></product-detail>
        </form>
      </div>
    </div>
    
  </div>

</x-front.layout>