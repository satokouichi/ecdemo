<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.footer></x-front.layouts.footer>
  </x-slot>
  
  <x-front.layouts.side></x-front.layouts.side>

  <div class="col-10 p-4">

    <div class="mb-2 fs-2">
      @if($category)
        {{ $category['category_name'] }}
      @elseif(Request::get('keyword'))
        {{ Request::get('keyword') }}の検索結果
      @else
        全商品
      @endif
    </div>
    <x-front.blocks.product.list :listProducts="$listProducts"></x-front.blocks.product.list>
    
  </div>

</x-front.layout>