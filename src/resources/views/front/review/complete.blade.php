<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <x-front.layouts.side></x-front.layouts.side>

  <div class="col-10 p-4">
    
    <div class="col-8 mx-auto">
      <div class="card border-0 shadow-sm">
        <div class="card-header">
          レビュー投稿完了
        </div>
        <div class="card-body">
          <span class="pe-3">レビュー投稿完了いたしました</span>
          <a class="btn btn-sm btn-primary text-white" href="{{ route('product.search') }}" role="button">買い物を続ける</a>
        </div>
      </div>
    </div>

  </div>

</x-front.layout>