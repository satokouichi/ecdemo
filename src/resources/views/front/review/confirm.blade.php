<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <x-front.layouts.side></x-front.layouts.side>

  <div class="col-10 p-4">
    
    <div class="col-8 mx-auto">
      <div class="card border-0 shadow-sm">
        <div class="card-header">
          レビュー確認
        </div>
        <div class="card-body">
          <form method="POST" action="?" enctype="multipart/form-data" novalidate>
            @csrf
            <input type="hidden" name="product_id" value="{{ $productId }}">
            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">おすすめレベル</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="recommend_level" value="{{ $inputs ? $inputs['recommend_level'] : '' }}">
                <div>{{ $inputs ? $inputs['recommend_level'] : '' }}</div>
              </div>
            </div>
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">タイトル</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="title" value="{{ $inputs ? $inputs['title'] : '' }}">
                <div>{{ $inputs ? $inputs['title'] : '' }}</div>
              </div>
            </div>
            <div class="row align-items-center mb-3">
              <div class="col-3">
                <span class="pe-2">コメント</span>
              </div>
              <div class="col-9">
                <input type="hidden" name="comment" value="{{ $inputs ? $inputs['comment'] : '' }}">
                <div>{{ $inputs ? $inputs['comment'] : '' }}</div>
              </div>
            </div>
            <div class="row align-items-center mb-4">
              <div class="col-3">
                <span class="pe-2">レビュー画像</span>
              </div>
              <div class="col-9">
                  @if ( $fileName and $reviewerUrl )
                    <img src="{{ asset('storage/images/review/temp/'.$fileName) }}">
                    <input type="hidden" name="reviewer_url" value="{{ $reviewerUrl }}">
                  @endif
              </div>
            </div>
            <div class="row align-items-center">
              <div class="col-10 text-start">
                <button type='submit' name="_method" value="post" formaction="{{ route('review.back', $productId) }}" class="btn btn-secondary btn-md">
                  修正する
                </button>
              </div>
              <div class="col-2 text-end">
                <button type='submit' name="_method" value="post" formaction="{{ route('review.complete' ,$productId) }}" class="btn btn-primary btn-md">
                  登録する
                </button>
              </div>
            </div>            
          </div>
        </form>      
      </div>
    </div>

  </div>

</x-front.layout>