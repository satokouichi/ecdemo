<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <x-front.layouts.side></x-front.layouts.side>

  <div class="col-10 p-4">
    
    <div class="col-8 mx-auto">
      <div class="card border-0 shadow-sm">
        <div class="card-header">
          レビュー投稿
        </div>
        <div class="card-body">
          <form method="POST" action="?" enctype="multipart/form-data" novalidate>
            @csrf
            <input type="hidden" name="product_id" value="{{ $productId }}">
            <input type="hidden" name="user_id" value="{{ Auth::id() }}">
            <div class="row align-items-center mb-3">
              <x-front.form.review.recommend-level
                recommend_level="recommend_level"
                :select="config('star')"
                :value_recommend_level="old('recommend_level') ? old('recommend_level') : ''"/>
            </div>
            <div class="row align-items-center mb-3">
              <x-front.form.review.title
                title="title"
                :value_title="old('title') ? old('title') : ''"/>
            </div>
            <div class="row align-items-center mb-3">
              <x-front.form.review.comment
                comment="comment"
                :value_comment="old('comment') ? old('comment') : ''"
                :rows="10"/>
            </div>
            <div class="row align-items-center mb-4">
              <div class="col-3">
                <span class="pe-2">レビュー画像</span>
              </div>
              <div class="col-9">
                <input type="file" name="image" accept="image/png,image/jpeg,image/jpg,image/gif" />
              </div>
            </div>
            <div class="row align-items-center">
              <div class="col-10 text-start">
                <button type="button" onclick="history.back()" class="btn btn-secondary btn-md">
                  戻る
                </button>
              </div>
              <div class="col-2 text-end">
                <button type="submit" name="_method" value="post" formaction="{{ route('review.confirm', $productId) }}" class="btn btn-primary btn-md">
                  確認する
                </button>
              </div>
            </div>            
          </div>
        </form>      
      </div>
    </div>

  </div>

</x-front.layout>