<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.footer></x-front.layouts.footer>
  </x-slot>
  <x-slot name="left"></x-slot>
  <x-slot name="right"></x-slot>
  
  <div id="content">
    フロント
  </div>

</x-front.layout>