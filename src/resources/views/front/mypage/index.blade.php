<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-8 mx-auto py-4">
    <div class="mb-3 fs-2">購入履歴</div>
    @if(count($orders) > 0)
      @foreach ($orders as $order)
        <div class="card mb-4 border-0 shadow-sm">
          <div class="card-header">
            <div class="row">
              <div class="col-10">
                <span class="pe-3">{{ $order->create_date }}</span>
                <span class="pe-3">注文番号：{{ $order->order_no }}</span>
                <span class="pe-3">請求額：￥{{ number_format($order->payment_total) }}</span>
                <span>注文状況：注文中</span>
              </div>
              <div class="col text-end">
                <a href="{{ route('mypage.show', $order->id) }}">詳細を見る</a>
              </div>
            </div>
          </div>
          <div class="card-body pt-0">
            <table class="table m-0">
              <thead>
                <tr class="border-bottom-0">
                  <th class="w-50">商品名</th>
                  <th class="w-10">価格</th>
                  <th class="w-10">数量</th>
                  <th class="w-10">小計</th>
                  <th class="text-end">&nbsp;</th>
                </tr>
              </thead>
              <tbody class="border-top-0">
                @foreach ($order->orderDetails as $orderDetail)
                  <tr>
                    <td class="align-middle">
                      <img src="http://via.placeholder.com/50x50" alt="">
                      <a href="{{ route('product.show', $orderDetail->product_id) }}" class="link-primary">
                        <span class="p-2">{{ $orderDetail->product_name }}</span>
                      </a>
                    </td>
                    <td class="align-middle">￥{{ number_format($orderDetail->price) }}</td>
                    <td class="align-middle">{{ $orderDetail->quantity }}</td>
                    <td class="align-middle">￥{{ number_format($orderDetail->price * $orderDetail->quantity) }}</td>
                    <td class="align-middle text-end">
                      <div class="d-inline-flex">
                        <div class="pe-2"><a href="{{ route('review.create', $orderDetail->product_id) }}" class="link-primary">口コミ投稿</a></div>
                        <div><a href="">再購入</a></div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      @endforeach

      <div class="d-flex justify-content-center mt-4">
        {{ $orders->links() }}
      </div>
    @else
      <p>ご注文はありません</p>
    @endif
  </div>

</x-front.layout>