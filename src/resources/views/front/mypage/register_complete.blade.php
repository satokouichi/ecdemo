<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="d-flex align-items-center justify-content-center vh-100 text-center">
    <div class="text-center">
      <p>新規会員登録完了いたしました。お買い物をお楽しみください。</p>
      <a class="btn px-5 btn-lg btn-main" href="{{ route('mypage.index') }}" role="button">Myページを見る</a>
    </div>
  </div>

</x-front.layout>