<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>
  
  <div class="col-8 mx-auto py-4">
    <div class="mb-3 fs-2">購入履歴詳細</div>
    <div class="card mb-4 border-0 shadow-sm">
      <div class="card-header">
        <div class="row">
          <div class="col">
            <span class="pe-3">ご注文日：{{ $order->create_date }}</span>
            <span class="pe-3">ご注文番号：￥{{ number_format($order->payment_total) }}</span>
            <span>ご注文状況：注文中</span>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-4">
            <div class="mb-2 fs-6 fw-bold">お届け先住所</div>
            <div>{{ $order->order_name01 }} {{ $order->order_name02 }}</div>
            <div>〒036-8227</div>
            <div>青森県(弘前桔梗野町郵便局留め)弘前市桔梗野2丁目8-11</div>
          </div>            
          <div class="col-4">
            <div class="mb-2 fs-6 fw-bold">お支払い方法</div>
            <div class="mb-2">銀行振込<a href="" class="btn btn-light border ms-2">変更</a></div>
            <a href="">ご注文明細書をダウンロード</a>
          </div>            
          <div class="col-4">
            <div class="mb-2 fs-6 fw-bold">ご注文内容</div>
            <table class="table m-0">
              <tr>
                <td class="text-start">送料</td>
                <td class="text-end">￥{{ number_format($order->delivery_fee_total) }}</td>
              </tr>
              <tr>
                <td class="text-start">使用pt</td>
                <td class="text-end">{{ $order->use_point }}pt</td>
              </tr>
              <tr>
                <td class="text-start">還元pt</td>
                <td class="text-end">{{ $order->add_point }}pt</td>
              </tr>
              <tr>
                <td class="text-start border-0 fw-bold">ご請求</td>
                <td class="border-0 text-end">￥{{ number_format($order->payment_total) }}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card border-0 shadow-sm">
      <div class="card-header">
        ご購入商品
      </div>
      <div class="card-body pt-0">
        <div class="row">
          <table class="table m-0">
            <thead>
              <tr class="border-bottom-0">
                <th class="w-50">商品名</th>
                <th class="w-10">価格</th>
                <th class="w-10">数量</th>
                <th class="w-10">小計</th>
                <th class="text-end">&nbsp;</th>
              </tr>
            </thead>
            <tbody class="border-top-0">
              @foreach ($order->orderDetails as $orderDetail)
                <tr>
                  <td class="align-middle">
                    <img src="http://via.placeholder.com/50x50" alt="">
                    <span class="p-2">{{ $orderDetail->name }}</span>
                  </td>
                  <td class="align-middle">￥{{ number_format($orderDetail->price) }}</td>
                  <td class="align-middle">{{ $orderDetail->quantity }}</td>
                  <td class="align-middle">￥{{ number_format($orderDetail->price * $orderDetail->quantity) }}</td>
                  <td class="align-middle text-end">
                    <span>口コミ投稿</span> <span>再購入</span>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</x-front.layout>