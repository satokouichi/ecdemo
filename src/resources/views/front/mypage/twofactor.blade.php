<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-8 mx-auto py-4">
    <div class="card">
      <div class="card-header">
        2要素認証の設定
      </div>
      <div class="card-body">
        @if (session('status') === "two-factor-authentication-disabled")
          <div class="alert alert-success mb-2" role="alert">
            2要素認証が無効になっています。
          </div>
        @endif
        @if (session('status') === "two-factor-authentication-enabled")
          <div class="alert alert-success mb-2" role="alert">
            2要素認証が有効になっています。
          </div>
        @endif
        <form method="POST" action ="/user/two-factor-authentication">
          @csrf
          @if (auth()->user()->two_factor_secret)
            <div class="mb-2">スマートフォンにてQRコードを読み込んでください</div>
            <div class="mb-2">
              @method('DELETE')
              {!! auth()->user()->twoFactorQrCodeSvg() !!}
              @foreach (json_decode(decrypt(auth()->user()->two_factor_recovery_codes)) as $code)
                <div>{{ $code }}</div>
              @endforeach
            </div>
            <div class="mb-2">2要素認証を無効にしたい場合は下記をクリックしてください</div>
            <button class="btn btn-danger">2段階認証を無効にする</button>
          @else
            <div class="mb-2">スマートフォンにてQRコードを読み込む認証を追加できます</div>
            <button class="btn btn-main">2段階認証を有効にする</button>
          @endif
        </form>
      </div>
    </div>
  </div>

</x-front.layout>