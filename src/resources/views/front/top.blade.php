<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.footer></x-front.layouts.footer>
  </x-slot>

  <div class="p-4">
    <div class="mb-2 fs-2">ランキング</div>
    <x-front.blocks.product.popular :popularProducts="$popularProducts"></x-front.blocks.product.popular>
  </div>

  <div class="p-4">
    <div class="mb-2 fs-2">おすすめ商品</div>
    <x-front.blocks.product.popular :popularProducts="$popularProducts"></x-front.blocks.product.popular>
  </div>

</x-front.layout>