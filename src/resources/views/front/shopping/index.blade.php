<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>
  
  <form method="post" action="{{ route('shopping.store') }}">
    @csrf
    <input type="hidden" id="card-token" name="card-token">
    <input type="hidden" name="mode" value="complete">
    <div class="col-8 mx-auto py-4">
      <div class="mb-3 fs-2">ご購入手続き</div>
      <shopping-index
      :user-point='@json($userPoint)'
      :order='@json($order)'
      :order-details='@json($orderDetails)'
      :payments='@json($payments)'
      :php-errors='@json($errors->all())'
      >
      </shopping-index>
      <div class="row w-75 mx-auto">
        <div class="col-6 text-center">
          <a class="btn px-5 btn-lg btn-secondary text-white" href="{{ route('cart.index') }}" role="button">カートへ戻る</a>
        </div>
        <div class="col-6 text-center">
          <button class="btn px-5 btn-lg btn-main" type="button" onclick="submit();">注文を完了する</button>
        </div>
      </div>
    </div>
  </form>

</x-front.layout>