<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>
  <x-slot name="left"></x-slot>
  <x-slot name="right"></x-slot>

  <div class="col-8 mx-auto py-4">
    <div class="mb-3 fs-2">ご注文ありがとうございました</div>
    <div class="card">
      <div class="card-header">
        ご請求明細
      </div>
      <div class="card-body">
        <p>ご注文承りました。詳細はMyページでご確認ください。</p>
        <table class="table table-bordered">
          <tr>
            <th class="align-middle p-2 fw-normal bg-light col-3 fs-4">請求額</th>
            <td class="align-middle p-2 fw-normal bg-white fs-4">￥{{ number_format($order->payment_total) }}</td>
          </tr>
          <tr>
            <th class="align-middle p-2 fw-normal bg-light col-3">送料</th>
            <td class="align-middle p-2 fw-normal bg-white">￥{{ number_format($order->delivery_fee_total) }}</td>
          </tr>
          <tr>
            <th class="align-middle p-2 fw-normal bg-light col-3">使用pt</th>
            <td class="align-middle p-2 fw-normal bg-white">{{ $order->use_point }}pt</td>
          </tr>
          <tr>
            <th class="align-middle p-2 fw-normal bg-light col-3">還元pt</th>
            <td class="align-middle p-2 fw-normal bg-white">{{ $order->add_point }}pt</td>
          </tr>
        </table>
        <div class="col-3 d-grid mx-auto text-center">
          <a class="btn btn-md btn-main mb-3" href="{{ route('mypage.index') }}" role="button">Myページを見る</a>
          <a class="btn btn-md btn-light border" href="{{ route('product.search') }}" role="button">お買い物を続ける</a>
        </div>
      </div>
    </div>
  </div>

</x-front.layout>