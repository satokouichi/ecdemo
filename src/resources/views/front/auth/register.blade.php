<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-6 col-11 col-sm-7 col-md-8 col-lg-6 mx-auto mb-5 py-4">

    <div class="card border-0 shadow-sm">

      <div class="card-header">
        新規会員登録
      </div>
      <div class="card-body">
        <form method="POST" action="{{ route('register') }}" novalidate>
          @csrf
          <entry-name
          :old-name01='@json(old('name01'))'
          :old-name02='@json(old('name02'))'
          :error-name01='@json($errors->get('name01'))'
          :error-name02='@json($errors->get('name02'))'
          >
          </entry-name>
          <entry-kana
          :old-kana01='@json(old('kana01'))'
          :old-kana02='@json(old('kana02'))'
          :error-kana01='@json($errors->get('kana01'))'
          :error-kana02='@json($errors->get('kana02'))'
          >
          </entry-kana>
          <entry-sex
          :old-sex='@json(old('sex'))'
          :error-sex='@json($errors->get('sex'))'
          :sexs='@json(config('sex'))'
          >
          </entry-sex>
          <entry-age
          :old-year='@json(old('year'))'
          :error-year='@json($errors->get('year'))'
          :years='@json(config('year'))'
          :old-month='@json(old('month'))'
          :error-month='@json($errors->get('month'))'
          :months='@json(config('month'))'
          :old-day='@json(old('day'))'
          :error-day='@json($errors->get('day'))'
          :days='@json(config('day'))'
          >
          </entry-age>          
          <entry-address
          :old-zip01='@json(old('zip01'))'
          :old-zip02='@json(old('zip02'))'
          :old-zipcode='@json(old('zipcode'))'
          :old-pref='@json(old('pref'))'
          :old-addr01='@json(old('addr01'))'
          :error-zip01='@json($errors->get('zip01'))'
          :error-zip02='@json($errors->get('zip02'))'
          :error-zipcode='@json($errors->get('zipcode'))'
          :error-pref='@json($errors->get('pref'))'
          :error-addr01='@json($errors->get('addr01'))'
          :prefs='@json(config('pref'))'
          >
          </entry-address>
          <entry-email
          :old-email='@json(old('email'))'
          :error-email='@json($errors->get('email'))'
          >
          </entry-email>
          <entry-pass
          :error-pass='@json($errors->get('password'))'
          >
          </entry-pass>
          <entry-nic
          :old-nic='@json(old('company_name'))'
          :error-nic='@json($errors->get('company_name'))'
          >
          </entry-nic>
          <div class="row w-50 mx-auto">
            <button type="submit" class="btn btn-main w-100 btn-md">登録する</button>
          </div>
        </form>
      </div>
    </div>

  </div>

</x-front.layout>