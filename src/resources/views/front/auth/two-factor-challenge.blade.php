<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-8 mx-auto py-4">

    <div class="card mb-4">
      <div class="card-header">
        認証コード
      </div>
      <div class="card-body">
        <form method="POST" action="{{ route('two-factor.login') }}">
          @csrf
          <div class="mb-1">お手持ちのスマートフォンから認証コードを入力してください</div>
          <label for="code" class="col-md-4 col-form-label text-md-right">認証コード</label>
          <div class="row g-2">
            <div class="col-3">
              <input id="code" type="code" class="form-control @error('code') is-invalid @enderror" name="code" required autocomplete="current-code">
            </div>
            <div class="col-auto">
              <button type="submit" class="btn btn-main">送信</button>
            </div>
          </div>
          @error('code')
            <strong>{{ $message }}</strong>
          @enderror
        </form>
      </div>
    </div>

    <div class="card">
      <div class="card-header">
        リカバリーコード
      </div>
      <div class="card-body">
        <form method="POST" action="{{ route('two-factor.login') }}">
          @csrf
          <div class="mb-1">認証コードをお忘れの場合はリカバリーコードを入力してください</div>
          <label for="recovery_code" class="col-md-4 col-form-label text-md-right">リカバリーコード</label>
          <div class="row g-2">
            <div class="col-3">
              <input id="recovery_code" type="recovery_code" class="form-control @error('recovery_code') is-invalid @enderror" name="recovery_code" required autocomplete="current-recovery_code">
            </div>
            <div class="col-auto">
              <button type="submit" class="btn btn-primary">送信</button>
            </div>
          </div>
          @error('code')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
          @enderror
        </form>
      </div>
    </div>

  </div>

</x-front.layout>