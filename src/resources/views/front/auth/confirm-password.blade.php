<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.header></x-front.layouts.header>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>

  <div class="col-8 mx-auto py-4">
    <div class="card">
      <div class="card-header">
        2要素認証の設定
      </div>
      <div class="card-body">
        <div class="mb-2">2要素認証の設定には会員登録時に設定したパスワードが必要です</div>
        <form method="POST" action="{{ route('password.confirm') }}">
          @csrf
          <div class="row g-2 mb-2">
            <label for="password" class="">パスワード</label>
            <div class="col-3">
              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            </div>
            <div class="col-auto">
              <button type="submit" class="btn btn-main">送信する</button>
            </div>
          </div>
          @error('password')
            <strong>{{ $message }}</strong>
          @enderror
          @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
              パスワードを忘れた場合
            </a>
          @endif
        </form>
      </div>
    </div>
  </div>

</x-front.layout>