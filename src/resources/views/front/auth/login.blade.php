<x-front.layout>
  
  <x-slot name="header">
  </x-slot>
  <x-slot name="footer">
  </x-slot>
  
  <div class="d-flex align-items-center justify-content-center vh-100 login-bg">
    <div class="mx-auto col-11 col-sm-7 col-md-6 col-lg-4 col-xl-3 col-xxl-3">

      <div class="fs-2 text-center text-white">DEMO LOGIN</div>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="mb-2">
          <label for="email" class="col-md-4 col-form-label text-white">メールアドレス</label>
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          @error('email')
            {{ $message }}
          @enderror
        </div>
        <div class="mb-4">
          <label for="password" class="col-md-4 col-form-label text-white">パスワード</label>
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
          @error('password')
            {{ $message }}
          @enderror
          <div class="mt-2">
            <span class="pe-1"><input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}></span>
            <label class="form-check-label text-white" for="remember">ログインを記憶する</label>
          </div>
        </div>
        <button type="submit" class="btn btn-main w-100 btn-md mb-3">
          ログインする
        </button>
        <a href="{{ route('register') }}" class="btn bg-light w-100 btn-md">
          新規登録する
        </a>
        @if (Route::has('password.request'))
          <div class="mt-2 text-center">
            <a href="{{ route('password.request') }}">
              パスワードを忘れた場合
            </a>
          </div>
        @endif
      </form>

    </div>
  </div>

</x-front.layout>