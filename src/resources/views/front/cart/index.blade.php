<x-front.layout>
  
  <x-slot name="header">
    <x-front.layouts.cartheader></x-front.layouts.cartheader>
  </x-slot>
  <x-slot name="footer">
    <x-front.layouts.cartfooter></x-front.layouts.cartfooter>
  </x-slot>
  
  <div class="col-8 mx-auto mb-5 py-4">
    <div class="mb-3 fs-2">ショッピングカート</div>
    @if($cart and array_key_exists('cartDetails', $cart))
      <div class="card mb-5 border-0 shadow-sm">
        <div class="card-body">
          <table class="table table-hover m-0">
            <thead>
              <tr class="border-bottom-0">
                <th>&nbsp;</th>
                <th class="w-50">商品名</th>
                <th class="w-15">価格</th>
                <th>数量</th>
                <th class="w-25 text-end">小計</th>
              </tr>
            </thead>
            <tbody class="border-top-0">
              @foreach($cart['cartDetails'] as $cartDetail)
                <tr>
                  <td class="align-middle">
                    <form method="post" action="{{ route('cart.delete') }}">
                      @method('DELETE')
                      @csrf
                      <input type="hidden" name="product_class_id" value="{{ $cartDetail['product_class_id'] }}"/>
                      <button class="btn btn-sm btn-light border rounded" type="submit">削除</button>
                    </form>
                  </td>
                  <td class="align-middle">
                    <img src="http://via.placeholder.com/50x50" alt="">
                    <span class="p-2">{{ $cartDetail['name'] }} {{ $cartDetail['dose'] }} {{ $cartDetail['shape'] }}</span>
                  </td>
                  <td class="align-middle">￥{{ number_format($cartDetail['price']) }}</td>
                  <td class="align-middle">
                    <form method="POST" action="{{ route('cart.update') }}">
                      @method('PUT')
                      @csrf
                      <input type="hidden" name="product_class_id" value="{{ $cartDetail['product_class_id'] }}"/>
                      <select name="quantity" onchange="submit(this.form)">
                        @for ($i = 1; $i <= 10; $i++)
                          @if($cartDetail['quantity'] == $i)
                            <option value="{{ $i }}" selected>{{ $i }}</option>
                          @else
                            <option value="{{ $i }}">{{ $i }}</option>
                          @endif
                        @endfor
                      </select>
                    </form>
                  </td>
                  <td class="align-middle text-end">￥{{ number_format($cartDetail['subtotal']) }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <div class="row">
            <div class="col-8 pt-2">
              <div>・7000円以上で送料無料になります</div>
              <div>・ご購入金額の5%をポイント還元いたします</div>
            </div>            
            <div class="col-4 text-end">
              <table class="table m-0">
                <tr>
                  <td class="fs-4 text-start">ご請求</td>
                  <td class="fs-4">￥{{ $cart['total'] }}</td>
                </tr>
                <tr>
                  <td class="text-start">送料</td>
                  <td>￥{{ $cart['delivery_fee_total'] }}</td>
                </tr>
                <tr>
                  <td class="text-start border-0">還元pt</td>
                  <td class="border-0">{{ $cart['add_point'] }}pt</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row w-75 mx-auto">
        <div class="col-6 text-center">
          <a class="btn px-5 btn-lg btn-secondary text-white" href="{{ route('product.search') }}" role="button">買い物を続ける</a>
        </div>
        <div class="col-6 text-center">
          <a class="btn px-5 btn-lg btn-main" href="{{ route('shopping.index') }}" role="button">ご購入手続きへ</a>
        </div>
      </div>
    @else
      <p>カートに商品が入っていません</p>
      <a class="btn px-5 btn-lg btn-secondary text-white" href="{{ route('product.search') }}" role="button">買い物を続ける</a>
    @endif
  </div>

</x-front.layout>