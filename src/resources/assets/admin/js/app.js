require('./bootstrap');

import PaymentCreate from './components/PaymentCreate.vue';
import PaymentDelete from './components/PaymentDelete.vue';
import TrackingCreate from './components/TrackingCreate.vue';
import TrackingDelete from './components/TrackingDelete.vue';
import OrderNote from './components/OrderNote.vue';
import SearchProduct from './components/SearchProduct.vue';
import SearchUser from './components/SearchUser.vue';
import ProductStock from './components/ProductStock.vue';

import { createApp } from 'vue';

const app = createApp({
  components: {
    PaymentCreate,
    PaymentDelete,
    TrackingCreate,
    TrackingDelete,
    OrderNote,
    SearchProduct,
    SearchUser,
    ProductStock
  }
}).mount('#app');