require('./bootstrap');

// カテゴリナビ
import CategoryNav from './components/CategoryNav.vue';

// 商品ページ
import ProductDetail from './components/product-detail/ProductDetail.vue';
import ProductType from './components/product-detail/ProductType.vue';
import ProductImage from './components/product-detail/ProductImage.vue';
import ProductPrice from './components/product-detail/ProductPrice.vue';
import ProductQuantity from './components/product-detail/ProductQuantity.vue';
import ProductCart from './components/product-detail/ProductCart.vue';

// 購入ページ
import ShoppingIndex from './components/shopping-index/ShoppingIndex.vue';
import ShoppingPayment from './components/shopping-index/ShoppingPayment.vue';
import ShoppingUsepoint from './components/shopping-index/ShoppingUsepoint.vue';
import ShoppingClaim from './components/shopping-index/ShoppingClaim.vue';

// 登録フォーム
import EntryName from './components/entry-form/EntryName.vue';
import EntryKana from './components/entry-form/EntryKana.vue';
import EntrySex from './components/entry-form/EntrySex.vue';
import EntryAge from './components/entry-form/EntryAge.vue';
import EntryAddress from './components/entry-form/EntryAddress.vue';
import EntryEmail from './components/entry-form/EntryEmail.vue';
import EntryPass from './components/entry-form/EntryPass.vue';
import EntryNic from './components/entry-form/EntryNic.vue';

import { createApp } from 'vue';

const app = createApp({
  components: {
    CategoryNav,
    ProductDetail,
    ProductType,
    ProductImage,
    ProductPrice,
    ProductQuantity,
    ProductCart,
    ShoppingIndex,
    ShoppingPayment,
    ShoppingUsepoint,
    ShoppingClaim,
    EntryName,
    EntryKana,
    EntrySex,
    EntryAge,
    EntryEmail,
    EntryAddress,
    EntryPass,
    EntryNic,
  }
}).mount('#app');