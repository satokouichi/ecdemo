<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\OrderController;
use App\Http\Controllers\Api\Admin\OrderPaymentController;
use App\Http\Controllers\Api\Admin\TrackingNumberController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/

Route::prefix('admin')->group(function () {
  // 注文
  Route::prefix('order')->group(function () {
    // 備考欄
    Route::post('/note', [OrderController::class, 'note'])->name('note');
    // 会員検索
    Route::post('/user', [OrderController::class, 'user'])->name('user');
    // 商品検索
    Route::post('/products', [OrderController::class, 'products'])->name('products');
  });
  // 入金
  Route::prefix('payment')->name('payment.')->group(function () {
    // 登録
    Route::post('/create', [OrderPaymentController::class, 'create'])->name('create');
    // 削除
    Route::post('/delete', [OrderPaymentController::class, 'delete'])->name('delete');
  });
  // 追跡番号
  Route::prefix('tracking')->name('tracking.')->group(function () {
    // 登録
    Route::post('/create', [TrackingNumberController::class, 'create'])->name('create');
    // 削除
    Route::post('/delete', [TrackingNumberController::class, 'delete'])->name('delete');
  });
});