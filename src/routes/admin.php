<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\TopController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ClassNameController;
use App\Http\Controllers\Admin\ClassCategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProductClassController;
use App\Http\Controllers\Admin\ProductReviewController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\OrderPaymentController;
use App\Http\Controllers\Admin\TrackingNumberController;
use App\Http\Controllers\Admin\MailController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\Auth\RegisteredUserController;
// use App\Http\Controllers\Admin\Auth\ConfirmablePasswordController;
// use App\Http\Controllers\Admin\Auth\EmailVerificationNotificationController;
// use App\Http\Controllers\Admin\Auth\EmailVerificationPromptController;
// use App\Http\Controllers\Admin\Auth\NewPasswordController;
// use App\Http\Controllers\Admin\Auth\PasswordResetLinkController;
// use App\Http\Controllers\Admin\Auth\VerifyEmailController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:admins')->group(function () {

    // トップページ
    Route::get('/', [TopController::class, 'index'])->name('index');

    // カテゴリ
    Route::prefix('category')->name('category.')->group(function () {
        // 一覧
        Route::get('/', [CategoryController::class, 'index'])->name('index');
        // 登録
        Route::post('/store', [CategoryController::class, 'store'])->name('store');
        // CSVエクスポート
        Route::post('/export', [CategoryController::class, 'export'])->name('export');
        // CSV雛形ダウンロード
        Route::post('/template', [CategoryController::class, 'template'])->name('template');
        // CSVインポート
        Route::get('/csv', [CategoryController::class, 'csv'])->name('csv');
        Route::post('/import', [CategoryController::class, 'import'])->name('import');
    });

    // 規格名
    Route::prefix('classname')->name('classname.')->group(function () {
        // CSVエクスポート
        Route::post('/export', [ClassNameController::class, 'export'])->name('export');
        // CSV雛形ダウンロード
        Route::post('/template', [ClassNameController::class, 'template'])->name('template');
        // CSVインポート
        Route::post('/import', [ClassNameController::class, 'import'])->name('import');
    });

    // 規格カテゴリ
    Route::prefix('classcategory')->name('classcategory.')->group(function () {
        // 一覧
        Route::get('/', [ClassCategoryController::class, 'index'])->name('index');
        // 登録
        Route::post('/store', [ClassCategoryController::class, 'store'])->name('store');
        // CSVエクスポート
        Route::post('/export', [ClassCategoryController::class, 'export'])->name('export');
        // CSV雛形ダウンロード
        Route::post('/template', [ClassCategoryController::class, 'template'])->name('template');
        // CSVインポート
        Route::get('/csv', [ClassCategoryController::class, 'csv'])->name('csv');
        Route::post('/import', [ClassCategoryController::class, 'import'])->name('import');
    });

    // 商品
    Route::prefix('product')->name('product.')->group(function () {
        // 一覧
        Route::match(['get', 'post'], '/', [ProductController::class, 'index'])->name('index');
        // 作成
        Route::get('/create', [ProductController::class, 'create'])->name('create');
        // 登録
        Route::post('/store', [ProductController::class, 'store'])->name('store');
        // 編集
        Route::get('/{productId}/edit', [ProductController::class, 'edit'])->name('edit');
        // CSVエクスポート
        Route::post('/export', [ProductController::class, 'export'])->name('export');
        // CSV雛形ダウンロード
        Route::post('/template', [ProductController::class, 'template'])->name('template');
        // CSVインポート
        Route::get('/csv', [ProductController::class, 'csv'])->name('csv');
        Route::post('/import', [ProductController::class, 'import'])->name('import');
    });

    // 商品規格
    Route::prefix('{productId}/productclass')->name('productclass.')->group(function () {
        // 一覧
        Route::get('/', [ProductClassController::class, 'index'])->name('index');
        // 登録
        Route::post('/store', [ProductClassController::class, 'store'])->name('store');
    });

    // レビュー
    Route::prefix('review')->name('review.')->group(function () {
        // 一覧
        Route::match(['get', 'post'], '/', [ProductReviewController::class, 'index'])->name('index');
        // 作成
        Route::get('/create', [ProductReviewController::class, 'create'])->name('create');
        // 登録
        Route::post('/store', [ProductReviewController::class, 'store'])->name('store');
        // 編集
        Route::get('/{reviewId}/edit', [ProductReviewController::class, 'edit'])->name('edit');
        // CSVエクスポート
        Route::post('/export', [ProductReviewController::class, 'export'])->name('export');
        // CSV雛形ダウンロード
        Route::post('/template', [ProductReviewController::class, 'template'])->name('template');
        // CSVインポート
        Route::get('/csv', [ProductReviewController::class, 'csv'])->name('csv');
        Route::post('/import', [ProductReviewController::class, 'import'])->name('import');
    });

    // 受注
    Route::prefix('order')->name('order.')->group(function () {
        // 一覧
        Route::match(['get', 'post'], '/', [OrderController::class, 'index'])->name('index');
        // 作成
        Route::get('/create', [OrderController::class, 'create'])->name('create');
        // 登録
        Route::post('/store', [OrderController::class, 'store'])->name('store');
        // 編集
        Route::get('/edit/{orderId}', [OrderController::class, 'edit'])->name('edit');
        // CSVエクスポート
        Route::post('/export', [OrderController::class, 'export'])->name('export');
    });

    // 会員
    Route::prefix('user')->name('user.')->group(function () {
        // 一覧
        Route::match(['get', 'post'], '/', [UserController::class, 'index'])->name('index');
        // 作成
        Route::get('/create', [UserController::class, 'create'])->name('create');
        // 登録
        Route::post('/store', [UserController::class, 'store'])->name('store');
        // 編集
        Route::get('/edit/{userId}', [UserController::class, 'edit'])->name('edit');
        // CSVエクスポート
        Route::post('/export', [UserController::class, 'export'])->name('export');
    });

    // 入金処理
    Route::prefix('payment/{orderId}')->name('payment.')->group(function () {
        Route::get('/', [OrderPaymentController::class, 'index'])->name('index');
    });

    // 発送処理
    Route::prefix('tracking/{orderId}')->name('tracking.')->group(function () {
        Route::get('/', [TrackingNumberController::class, 'index'])->name('index');
    });

    // メール
    Route::prefix('mail/{orderId}/{templateId}')->name('mail.')->group(function () {
        Route::get('/', [MailController::class, 'index'])->name('index');
        Route::post('/confirm', [MailController::class, 'confirm'])->name('confirm');
        Route::post('/back', [MailController::class, 'back'])->name('back');
        Route::post('/send', [MailController::class, 'send'])->name('send');
    });

});

// 認証
Route::middleware('guest')->group(function () {
    Route::get('login', [AuthenticatedSessionController::class, 'create'])->name('login');
    Route::post('login', [AuthenticatedSessionController::class, 'store']);
    Route::get('register', [RegisteredUserController::class, 'create'])->name('register');
    Route::post('register', [RegisteredUserController::class, 'store']);
});
Route::middleware('auth:admins')->group(function () {
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');
});

//     Route::get('forgot-password', [PasswordResetLinkController::class, 'create'])
//                 ->name('password.request');

//     Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])
//                 ->name('password.email');

//     Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])
//                 ->name('password.reset');

//     Route::post('reset-password', [NewPasswordController::class, 'store'])
//                 ->name('password.update');
// });

// Route::middleware('auth:admin')->group(function () {
//     Route::get('verify-email', [EmailVerificationPromptController::class, '__invoke'])
//                 ->middleware('auth:admin')
//                 ->name('verification.notice');

//     Route::get('verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
//                 ->middleware(['auth:admin', 'signed', 'throttle:6,1'])
//                 ->name('verification.verify');

//     Route::post('email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
//                 ->middleware(['auth:admin', 'throttle:6,1'])
//                 ->name('verification.send');

//     Route::get('confirm-password', [ConfirmablePasswordController::class, 'show'])
//                 ->middleware('auth:admin')
//                 ->name('password.confirm');

//     Route::post('confirm-password', [ConfirmablePasswordController::class, 'store'])
//                 ->middleware('auth:admin');
// });