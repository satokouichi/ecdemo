<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\TopController;
use App\Http\Controllers\Front\ProductController;
use App\Http\Controllers\Front\ProductReviewController;
use App\Http\Controllers\Front\CartController;
use App\Http\Controllers\Front\ShoppingController;
use App\Http\Controllers\Front\MypageController;
use App\Http\Controllers\Front\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Front\Auth\RegisteredUserController;
// use App\Http\Controllers\Front\Auth\ConfirmablePasswordController;
// use App\Http\Controllers\Front\Auth\EmailVerificationNotificationController;
// use App\Http\Controllers\Front\Auth\EmailVerificationPromptController;
// use App\Http\Controllers\Front\Auth\NewPasswordController;
// use App\Http\Controllers\Front\Auth\PasswordResetLinkController;
// use App\Http\Controllers\Front\Auth\VerifyEmailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// トップページ
Route::get('/', [TopController::class, 'index'])->name('index');

// 商品一覧
Route::get('/category{categoryId}', [ProductController::class, 'index'])->name('product.index');

// 商品検索
Route::get('/search', [ProductController::class, 'search'])->name('product.search');

// 商品詳細
Route::prefix('products')->name('product.')->group(function () {
    Route::get('/{productId}', [ProductController::class, 'show'])->name('show');
});

// 口コミ
Route::prefix('products/{productId}/comments')->name('review.')->group(function () {
    // 一覧
    Route::get('/', [ProductReviewController::class, 'index'])->name('index');
    // 詳細
    Route::get('/{reviewId}', [ProductReviewController::class, 'show'])->name('show')->where(['productId' => '[0-9]+', 'reviewId' => '\w{10,}']);
});

// 口コミ投稿
Route::prefix('products/{productId}/comments')->name('review.')->middleware('auth:users')->group(function () {
    Route::get('/create', [ProductReviewController::class, 'create'])->name('create');
    Route::post('/confirm', [ProductReviewController::class, 'confirm'])->name('confirm');
    Route::post('/back', [ProductReviewController::class, 'back'])->name('back');
    Route::post('/complete', [ProductReviewController::class, 'complete'])->name('complete');
});

// カート
Route::prefix('cart')->name('cart.')->group(function () {
    // 一覧
    Route::get('/', [CartController::class, 'index'])->name('index');
    // 登録
    Route::post('/store', [CartController::class, 'store'])->name('store');
    // 更新
    Route::put('/update', [CartController::class, 'update'])->name('update');
    // 削除
    Route::delete('/delete', [CartController::class, 'delete'])->name('delete');
});

// 購入
Route::prefix('shopping')->name('shopping.')->middleware('auth:users')->group(function () {
    // 一覧
    Route::get('/', [ShoppingController::class, 'index'])->name('index');
    // 登録
    Route::post('/', [ShoppingController::class, 'store'])->name('store');
    // 完了
    Route::get('/complete', [ShoppingController::class, 'complete'])->name('complete');
});

// 購入履歴
Route::prefix('mypage')->name('mypage.')->middleware('auth:users')->group(function () {
    // 一覧
    Route::get('/', [MypageController::class, 'index'])->name('index');
    // 詳細
    Route::get('/{orderId}', [MypageController::class, 'show'])->name('show')->where(['orderId' => '[0-9]+']);
    // 2要素認証
    Route::get('/twofactor', [MypageController::class, 'twofactor'])->name('twofactor');
    // 新規登録完了
    Route::get('/register_complete', [MypageController::class, 'registerComplete'])->name('register_complete');
});

// 認証
/*
Route::middleware('guest')->group(function () {
    Route::get('login', [AuthenticatedSessionController::class, 'create'])->name('login');
    Route::post('login', [AuthenticatedSessionController::class, 'store']);
    Route::get('register', [RegisteredUserController::class, 'create'])->name('register');
    Route::post('register', [RegisteredUserController::class, 'store']);
});
Route::middleware('auth:users')->group(function () {
    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');
});
*/
