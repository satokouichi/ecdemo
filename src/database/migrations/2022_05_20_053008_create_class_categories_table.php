<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('class_name_id')->index();
            $table->string('name');
            $table->unsignedInteger('rank');
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent();
            $table->unsignedSmallInteger('del_flg')->default(0);

            $table->foreign('class_name_id')->references('id')->on('class_names')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_categories');
    }
};
