<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->nullable()->default(null);
            $table->unsignedBigInteger('user_id');
            $table->unsignedSmallInteger('order_pref')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('order_sex')->nullable()->default(null)->index();
            $table->unsignedInteger('payment_id')->nullable()->default(null)->index();
            $table->longText('pre_order_id')->nullable()->default(null);
            $table->longText('message')->nullable()->default(null);
            $table->longText('order_name01')->nullable()->default(null);
            $table->longText('order_name02')->nullable()->default(null);
            $table->longText('order_kana01')->nullable()->default(null);
            $table->longText('order_kana02')->nullable()->default(null);
            $table->longText('order_company_name')->nullable()->default(null);
            $table->longText('order_email')->nullable()->default(null);
            $table->longText('order_tel01')->nullable()->default(null);
            $table->longText('order_tel02')->nullable()->default(null);
            $table->longText('order_tel03')->nullable()->default(null);
            $table->longText('order_tel')->nullable()->default(null);
            $table->unsignedInteger('order_post_office_id')->nullable()->default(null);
            $table->longText('order_fax01')->nullable()->default(null);
            $table->longText('order_fax02')->nullable()->default(null);
            $table->longText('order_fax03')->nullable()->default(null);
            $table->longText('order_zip01')->nullable()->default(null);
            $table->longText('order_zip02')->nullable()->default(null);
            $table->longText('order_zipcode')->nullable()->default(null);
            $table->longText('order_addr01')->nullable()->default(null);
            $table->longText('order_addr02')->nullable()->default(null);
            $table->dateTimeTz('order_birth')->nullable()->default(null);
            $table->unsignedDecimal('subtotal', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('discount', $precision = 10, $scale = 0)->nullable()->default(0);
            $table->unsignedDecimal('delivery_fee_total', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('charge', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('use_point', $precision = 10, $scale = 0)->default(0);
            $table->unsignedDecimal('add_point', $precision = 10, $scale = 0)->default(0);
            $table->unsignedDecimal('total', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('payment_total', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->longText('payment_method')->nullable()->default(null);
            $table->longText('note')->nullable()->default(null);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent()->index();
            $table->dateTimeTz('order_date')->nullable()->default(null)->index();
            $table->dateTimeTz('commit_date')->nullable()->default(null)->index();
            $table->dateTimeTz('payment_date')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('status')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('del_flg')->default(0);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
