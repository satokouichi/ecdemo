<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('cart_key')->unique();
            $table->string('pre_order_id')->unique()->nullable();
            $table->unsignedDecimal('delivery_fee_total', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('total', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('use_point', $precision = 10, $scale = 0)->default(0);
            $table->unsignedDecimal('add_point', $precision = 10, $scale = 0)->default(0);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent()->index();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
};
