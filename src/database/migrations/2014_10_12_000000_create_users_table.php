<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('status')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('sex')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('pref')->nullable()->default(null)->index();
            $table->string('name01')->nullable()->default(null);
            $table->string('name02')->nullable()->default(null);
            $table->string('kana01')->nullable()->default(null);
            $table->string('kana02')->nullable()->default(null);
            $table->string('company_name')->nullable()->default(null);
            $table->string('zip01')->nullable()->default(null);
            $table->string('zip02')->nullable()->default(null);
            $table->string('zipcode')->nullable()->default(null);
            $table->string('addr01')->nullable()->default(null);
            $table->string('addr02')->nullable()->default(null);
            $table->longText('tel01')->nullable()->default(null);
            $table->longText('tel02')->nullable()->default(null);
            $table->longText('tel03')->nullable()->default(null);
            $table->longText('tel')->nullable()->default(null);
            $table->dateTimeTz('birth')->nullable()->default(null);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->dateTimeTz('first_buy_date')->nullable()->default(null);
            $table->dateTimeTz('last_buy_date')->nullable()->default(null);
            $table->unsignedDecimal('buy_times', $precision = 10, $scale = 0)->nullable()->default(0);
            $table->unsignedDecimal('buy_total', $precision = 10, $scale = 0)->nullable()->default(0);
            $table->unsignedDecimal('point', $precision = 10, $scale = 0)->default(0);
            $table->longText('note')->nullable()->default(null);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent()->index();
            $table->unsignedInteger('post_office_id')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('mailmaga_flg')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('del_flg')->default(0);   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
