<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('status')->nullable()->default(null)->index();
            $table->string('name')->unique();
            $table->string('discount_pattern');
            $table->string('note')->nullable()->default(null);
            $table->string('description_list')->nullable()->default(null);
            $table->string('description_detail')->nullable()->default(null);
            $table->string('search_word')->nullable()->default(null);
            $table->string('free_area')->nullable()->default(null);
            $table->string('recommend_text')->nullable()->default(null);
            $table->string('title')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('shape')->nullable()->default(null);
            $table->unsignedInteger('trader_id')->nullable()->default(null);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent();
            $table->unsignedSmallInteger('del_flg')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
