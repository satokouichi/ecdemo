<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('class_category_id1')->index();
            $table->unsignedInteger('class_category_id2')->index();
            $table->string('product_code')->nullable()->default(null);
            $table->string('zaiko_id')->nullable()->default(null);
            $table->unsignedInteger('stock')->nullable()->default(null)->index();
            $table->unsignedSmallInteger('stock_unlimited')->index();
            $table->unsignedInteger('price01')->nullable()->default(null);
            $table->unsignedInteger('price02')->index();
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent();
            $table->dateTimeTz('expire_date')->useCurrent()->nullable()->default(null);
            $table->unsignedSmallInteger('status')->nullable()->default(null);
            $table->unsignedInteger('dose');
            $table->unsignedInteger('handling_flg');
            $table->unsignedSmallInteger('default_flg')->default(1);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('class_category_id1')->references('id')->on('class_categories');
            $table->foreign('class_category_id2')->references('id')->on('class_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_classes');
    }
};
