<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->string('product_review_id_random')->nullable()->default(null);
            $table->longText('reviewer_url')->nullable()->default(null);
            $table->string('reviewer_name',50)->nullable()->default(null);
            $table->unsignedSmallInteger('recommend_level');
            $table->string('title',50);
            $table->longText('comment');
            $table->unsignedSmallInteger('del_flg')->default(0);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent();
            $table->unsignedInteger('class_category_id')->nullable()->default(null);
            $table->unsignedSmallInteger('status')->nullable()->default(null);
            $table->unsignedSmallInteger('image_status')->nullable()->default(2);
            $table->unsignedInteger('favorite_count')->default(0);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_reviews');
    }
};
