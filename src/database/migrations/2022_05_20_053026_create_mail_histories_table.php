<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_histories', function (Blueprint $table) {
            $table->increments('send_id');
            $table->unsignedInteger('order_id')->index();
            $table->unsignedInteger('template_id')->nullable()->default(null)->index();
            $table->dateTimeTz('send_date')->nullable()->default(null);
            $table->longText('subject')->nullable()->default(null);
            $table->longText('mail_body')->nullable()->default(null);

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_histories');
    }
};
