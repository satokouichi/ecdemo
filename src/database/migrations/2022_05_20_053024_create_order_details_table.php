<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->index();
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('product_class_id')->index();
            $table->longText('product_name');
            $table->longText('product_code')->nullable()->default(null);
            $table->longText('zaiko_id')->nullable()->default(null);
            $table->longText('class_name1')->nullable()->default(null);
            $table->longText('class_name2')->nullable()->default(null);
            $table->longText('class_category_name1')->nullable()->default(null);
            $table->longText('class_category_name2')->nullable()->default(null);
            $table->unsignedDecimal('price', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('quantity', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedDecimal('tax_rate', $precision = 10, $scale = 0)->nullable()->default(null);
            $table->unsignedSmallInteger('tax_rule')->nullable()->default(null);
            $table->unsignedTinyInteger('pending')->default(0);

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
};
