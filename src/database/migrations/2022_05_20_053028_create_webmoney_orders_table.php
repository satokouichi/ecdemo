<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webmoney_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->string('process_number',50)->nullable()->default(null);
            $table->string('result_code',50)->nullable()->default(null);
            $table->string('settlement_code',50)->nullable()->default(null);
            $table->string('card_number',50)->nullable()->default(null);
            $table->dateTimeTz('create_date')->useCurrent();
            $table->dateTimeTz('update_date')->useCurrent();

            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webmoney_orders');
    }
};
