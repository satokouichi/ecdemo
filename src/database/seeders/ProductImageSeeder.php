<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 4; $i++) {
            $arr = [
                'product_id' => 2,
                'file_name' => 'image' . $i,
                'file_text' => '商品ALT' . $i,
                'rank' => $i,
                'create_date' => now(),
            ];
            \Illuminate\Support\Facades\DB::table('product_images')->insert($arr);
        }
    }
}
