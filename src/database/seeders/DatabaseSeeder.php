<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            AdminSeeder::class,
            DiscountSeeder::class,
            // CategorySeeder::class,
            // ClassNameSeeder::class,
            // ClassCategorySeeder::class,
            // ProductSeeder::class,
            // ProductCategorySeeder::class,
            // ProductClassSeeder::class,
            // ProductImageSeeder::class,
            // ProductMixtureMergeSeeder::class,
            // ProductReviewSeeder::class,
            MailTemplateSeeder::class,
            OrderSeeder::class,
            OrderDetailSeeder::class,
            ConveniOrderSeeder::class,
            TrackingNumberSeeder::class,
            WebmoneyOrderSeeder::class,
            OrderStatusSeeder::class,
            PaymentSeeder::class,
            OrderPaymentSeeder::class,
            OrderCreditSeeder::class,
            MailHistorySeeder::class,
        ]);

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
