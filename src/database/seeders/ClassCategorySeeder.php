<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClassCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'class_name_id' => 1,
                'name' => '1',
                'rank' => 1,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 2,
                'class_name_id' => 1,
                'name' => '2',
                'rank' => 2,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 3,
                'class_name_id' => 1,
                'name' => '3',
                'rank' => 3,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 4,
                'class_name_id' => 2,
                'name' => '50',
                'rank' => 4,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 5,
                'class_name_id' => 2,
                'name' => '100',
                'rank' => 5,
                'create_date' => now(),
                'update_date' => now(),
            ],
        ];
        \Illuminate\Support\Facades\DB::table('class_categories')->insert($arr);
    }
}
