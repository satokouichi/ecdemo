<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $arr = [
                'status' => 1,
                'name' => '商品' . $i,
                'description_list' => 'リスト' . $i,
                'description_detail' => 'ディテイル' . $i,
                'search_word' => 'ワード' . $i,
                'recommend_text' => 'おすすめ' . $i,
                'title' => 'タイトル' . $i,
                'description' => 'ディスクリプション' . $i,
                'shape' => '錠' . $i,
                'trader_id' => $i,
                'create_date' => now(),
                'update_date' => now(),
            ];
            \Illuminate\Support\Facades\DB::table('products')->insert($arr);
        }
    }
}
