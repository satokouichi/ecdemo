<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'status' => 1,
                'name' => '注文中',
                'rank' => 1,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 3,
                'status' => 1,
                'name' => 'キャンセル',
                'rank' => 2,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 5,
                'status' => 1,
                'name' => '発送済み',
                'rank' => 3,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 6,
                'status' => 1,
                'name' => '入金済み',
                'rank' => 4,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 7,
                'status' => 1,
                'name' => '決済処理中',
                'rank' => 5,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 8,
                'status' => 1,
                'name' => '購入処理中',
                'rank' => 6,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 9,
                'status' => 1,
                'name' => '不足金',
                'rank' => 7,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 10,
                'status' => 1,
                'name' => '一部未発送',
                'rank' => 8,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 11,
                'status' => 1,
                'name' => '期限切れ',
                'rank' => 9,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 12,
                'status' => 1,
                'name' => '発注保留',
                'rank' => 10,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 13,
                'status' => 1,
                'name' => 'カード決済申請中',
                'rank' => 11,
                'create_date' => now(),
                'update_date' => now(),
            ]
        ];

        \Illuminate\Support\Facades\DB::table('order_statuses')->insert($arr);
    }
}
