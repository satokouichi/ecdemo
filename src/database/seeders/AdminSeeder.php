<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'name' => '管理画面ユーザー',
            'email' => 'admin@test.test',
            'email_verified_at' => now(),
            'password' => Hash::make('aaa123456'),
        ];
        \Illuminate\Support\Facades\DB::table('admins')->insert($arr);
    }
}