<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClassNameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'name' => 'セット',
                'rank' => 1,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 2,
                'name' => 'バイアグラ',
                'rank' => 2,
                'create_date' => now(),
                'update_date' => now(),
            ],
        ];
        \Illuminate\Support\Facades\DB::table('class_names')->insert($arr);
    }
}
