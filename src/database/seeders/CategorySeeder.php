<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $arr = [
                'parent_category_id' => $i <= 2 ? null : '1',
                'category_name' => 'カテゴリ' . $i,
                'description' => 'ディスクリプション' . $i,
                'level' => '1',
                'rank' => $i,
                'create_date' => now(),
                'update_date' => now(),
            ];
            \Illuminate\Support\Facades\DB::table('categories')->insert($arr);
        }
    }
}
