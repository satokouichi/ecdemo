<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 4; $i++) {
            $arr = [
                'product_id' => $i,
                'class_category_id1' => $i,
                'class_category_id2' => ($i+1),
                'product_code' => 'product-code' . $i,
                'zaiko_id' => 'ZAIKOID' . $i,
                'stock' => null,
                'stock_unlimited' => 1,
                'price01' => $i.'000',
                'price02' => ($i+1).'000',
                'create_date' => now(),
                'update_date' => now(),
                'status' => 1,
                'dose' => 4,
                'handling_flg' => 1,
            ];
            \Illuminate\Support\Facades\DB::table('product_classes')->insert($arr);
        }
    }
}
