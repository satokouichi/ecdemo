<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'status' => 2,
                'name' => '郵便振替',
                'rank' => 1,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 2,
                'status' => 2,
                'name' => '現金書留',
                'rank' => 2,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 3,
                'status' => 1,
                'name' => '銀行振込',
                'rank' => 3,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 4,
                'status' => 2,
                'name' => '代金引換',
                'rank' => 4,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 5,
                'status' => 1,
                'name' => 'コンビニ決済',
                'rank' => 5,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 6,
                'status' => 1,
                'name' => 'クレジットカード',
                'rank' => 6,
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 7,
                'status' => 1,
                'name' => 'WebMoney',
                'rank' => 7,
                'create_date' => now(),
                'update_date' => now(),
            ]
        ];

        \Illuminate\Support\Facades\DB::table('payments')->insert($arr);
    }
}
