<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $arr = [
                'product_id' => 1,
                'user_id' => 1,
                'product_review_id_random' => 'GDFDGDFGF' . $i,
                'reviewer_url' => 'fdsfdsfdsfd' . $i,
                'reviewer_name' => 'ネーム' . $i,
                'recommend_level' => 5,
                'title' => 'タイトル' . $i,
                'comment' => 'コメント' . $i,
                'del_flg' => '0',
                'create_date' => now(),
                'update_date' => now(),
                'class_category_id' => null,
                'status' => 1,
                'image_status' => 1,
                'favorite_count' => 1,
            ];
            \Illuminate\Support\Facades\DB::table('product_reviews')->insert($arr);
        }
    }
}
