<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            'status' => 1,
            'sex' => 1,
            'pref' => 2,
            'name01' => '佐藤',
            'name02' => '光一',
            'kana01' => 'サトウ',
            'kana02' => 'コウイチ',
            'zip01' => null,
            'zip02' => null,
            'zipcode' => null,
            'addr01' => '北海道札幌市',
            'addr02' => '西区2丁目',
            'tel01' => null,
            'tel02' => null,
            'tel03' => null,
            'tel' => '09000112244',
            'birth' => null,
            'email' => 'user@test.test',
            'email_verified_at' => now(),
            'password' => Hash::make('aaa123456'),
            'first_buy_date' => null,
            'last_buy_date' => null,
            'buy_times' => null,
            'buy_total' => null,
            'point' => '300',
            'note' => null,
            'create_date' => now(),
            'update_date' => now(),
        ];
        \Illuminate\Support\Facades\DB::table('users')->insert($arr);
    }
}