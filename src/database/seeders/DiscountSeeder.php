<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'pattern' => 'A',
                'order_num' => 1,
                'discount' => 0,
            ],
            [
                'id' => 2,
                'pattern' => 'A',
                'order_num' => 2,
                'discount' => 10,
            ],
            [
                'id' => 3,
                'pattern' => 'A',
                'order_num' => 3,
                'discount' => 13,
            ],
            [
                'id' => 4,
                'pattern' => 'A',
                'order_num' => 5,
                'discount' => 18,
            ],
            [
                'id' => 5,
                'pattern' => 'A',
                'order_num' => 6,
                'discount' => 19,
            ],
            [
                'id' => 6,
                'pattern' => 'A',
                'order_num' => 7,
                'discount' => 20,
            ],
            [
                'id' => 7,
                'pattern' => 'A',
                'order_num' => 9,
                'discount' => 22,
            ],
            [
                'id' => 8,
                'pattern' => 'A',
                'order_num' => 10,
                'discount' => 24,
            ],
            [
                'id' => 9,
                'pattern' => 'A',
                'order_num' => 12,
                'discount' => 28,
            ],
            [
                'id' => 10,
                'pattern' => 'A',
                'order_num' => 24,
                'discount' => 32,
            ],
            [
                'id' => 11,
                'pattern' => 'B',
                'order_num' => 1,
                'discount' => 0,
            ],
            [
                'id' => 12,
                'pattern' => 'B',
                'order_num' => 2,
                'discount' => 38,
            ],
            [
                'id' => 13,
                'pattern' => 'B',
                'order_num' => 3,
                'discount' => 54,
            ],
            [
                'id' => 14,
                'pattern' => 'B',
                'order_num' => 5,
                'discount' => 64,
            ],
            [
                'id' => 15,
                'pattern' => 'B',
                'order_num' => 6,
                'discount' => 66,
            ],
            [
                'id' => 16,
                'pattern' => 'B',
                'order_num' => 7,
                'discount' => 70,
            ],
            [
                'id' => 17,
                'pattern' => 'B',
                'order_num' => 9,
                'discount' => 71,
            ],
            [
                'id' => 18,
                'pattern' => 'B',
                'order_num' => 10,
                'discount' => 72,
            ],
            [
                'id' => 19,
                'pattern' => 'B',
                'order_num' => 12,
                'discount' => 74,
            ],
            [
                'id' => 20,
                'pattern' => 'B',
                'order_num' => 24,
                'discount' => 76,
            ],
            [
                'id' => 21,
                'pattern' => 'C',
                'order_num' => 1,
                'discount' => 0,
            ],
            [
                'id' => 22,
                'pattern' => 'C',
                'order_num' => 2,
                'discount' => 12,
            ],
            [
                'id' => 23,
                'pattern' => 'C',
                'order_num' => 3,
                'discount' => 25,
            ],
            [
                'id' => 24,
                'pattern' => 'C',
                'order_num' => 5,
                'discount' => 40,
            ],
            [
                'id' => 25,
                'pattern' => 'C',
                'order_num' => 6,
                'discount' => 50,
            ],
            [
                'id' => 26,
                'pattern' => 'C',
                'order_num' => 7,
                'discount' => 54,
            ],
            [
                'id' => 27,
                'pattern' => 'C',
                'order_num' => 9,
                'discount' => 60,
            ],
            [
                'id' => 28,
                'pattern' => 'C',
                'order_num' => 10,
                'discount' => 64,
            ],
            [
                'id' => 29,
                'pattern' => 'C',
                'order_num' => 12,
                'discount' => 68,
            ],
            [
                'id' => 30,
                'pattern' => 'C',
                'order_num' => 24,
                'discount' => 70,
            ],
            [
                'id' => 31,
                'pattern' => 'D',
                'order_num' => 1,
                'discount' => 0,
            ],
            [
                'id' => 32,
                'pattern' => 'D',
                'order_num' => 2,
                'discount' => 20,
            ],
            [
                'id' => 33,
                'pattern' => 'D',
                'order_num' => 3,
                'discount' => 25,
            ],
            [
                'id' => 34,
                'pattern' => 'D',
                'order_num' => 5,
                'discount' => 32,
            ],
            [
                'id' => 35,
                'pattern' => 'D',
                'order_num' => 6,
                'discount' => 34,
            ],
            [
                'id' => 36,
                'pattern' => 'D',
                'order_num' => 7,
                'discount' => 36,
            ],
            [
                'id' => 37,
                'pattern' => 'D',
                'order_num' => 9,
                'discount' => 37,
            ],
            [
                'id' => 38,
                'pattern' => 'D',
                'order_num' => 10,
                'discount' => 38,
            ],
            [
                'id' => 39,
                'pattern' => 'D',
                'order_num' => 12,
                'discount' => 40,
            ],
            [
                'id' => 40,
                'pattern' => 'D',
                'order_num' => 24,
                'discount' => 46,
            ],
            [
                'id' => 41,
                'pattern' => 'A',
                'order_num' => 4,
                'discount' => 16,
            ],
            [
                'id' => 42,
                'pattern' => 'A',
                'order_num' => 8,
                'discount' => 21,
            ],
            [
                'id' => 43,
                'pattern' => 'F',
                'order_num' => 1,
                'discount' => 0,
            ],
            [
                'id' => 44,
                'pattern' => 'F',
                'order_num' => 2,
                'discount' => 16,
            ],
            [
                'id' => 45,
                'pattern' => 'F',
                'order_num' => 3,
                'discount' => 19,
            ],
            [
                'id' => 46,
                'pattern' => 'F',
                'order_num' => 4,
                'discount' => 0,
            ],
            [
                'id' => 47,
                'pattern' => 'F',
                'order_num' => 5,
                'discount' => 0,
            ],
            [
                'id' => 48,
                'pattern' => 'F',
                'order_num' => 6,
                'discount' => 0,
            ],
            [
                'id' => 49,
                'pattern' => 'F',
                'order_num' => 7,
                'discount' => 0,
            ],
            [
                'id' => 50,
                'pattern' => 'F',
                'order_num' => 8,
                'discount' => 0,
            ],
            [
                'id' => 51,
                'pattern' => 'F',
                'order_num' => 9,
                'discount' => 0,
            ],
            [
                'id' => 52,
                'pattern' => 'F',
                'order_num' => 12,
                'discount' => 0,
            ],
            [
                'id' => 53,
                'pattern' => 'F',
                'order_num' => 24,
                'discount' => 0,
            ],
        ];
        \Illuminate\Support\Facades\DB::table('discounts')->insert($arr);
    }
}
