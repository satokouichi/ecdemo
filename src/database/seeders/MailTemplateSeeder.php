<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'id' => 1,
                'name' => '注文受付メール',
                'file_name' => 'order',
                'subject' => '注文完了のお知らせ',
                'header' => 'メールヘッダー',
                'footer' => 'メールフッター',
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 6,
                'name' => '発送メール',
                'file_name' => 'commit',
                'subject' => '発送完了いたしました',
                'header' => 'メールヘッダー',
                'footer' => 'メールフッター',
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 7,
                'name' => '入金メール',
                'file_name' => 'payment',
                'subject' => 'お支払い完了',
                'header' => 'メールヘッダー',
                'footer' => 'メールフッター',
                'create_date' => now(),
                'update_date' => now(),
            ],
            [
                'id' => 8,
                'name' => '不足金メール',
                'file_name' => 'not_pay',
                'subject' => 'お支払い金額が不足しておりました',
                'header' => 'メールヘッダー',
                'footer' => 'メールフッター',
                'create_date' => now(),
                'update_date' => now(),
            ],
        ];

        \Illuminate\Support\Facades\DB::table('mail_templates')->insert($arr);
    }
}
