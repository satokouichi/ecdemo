<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            $arr = [
                'product_id' => $i,
                'category_id' => 3,
                'rank' => $i,
            ];
            \Illuminate\Support\Facades\DB::table('product_category')->insert($arr);
        }
    }
}
