<?php

namespace App\View\Components\Front\Blocks;

use Illuminate\View\Component;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;

class Category extends Component
{
    private $categoryRepository;

    public function __construct
    (
        CategoryRepository $categoryRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $categories = $this->categoryRepository->getCategories();
        $categories = $this->categoryRepository->getCategoryArray($categories->get());

        return view('components.front.blocks.category', [
            'categories' => $categories,
        ]);
    }
}
