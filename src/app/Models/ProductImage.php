<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = ['id'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = null;
}
