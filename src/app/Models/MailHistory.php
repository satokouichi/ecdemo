<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MailHistory extends Model
{
    protected $guarded = ['send_id'];

    protected $primaryKey = 'send_id';
}
