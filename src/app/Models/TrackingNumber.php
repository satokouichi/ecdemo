<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingNumber extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = null;
}
