<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassName extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function csvHeader(): array
    {
        return [
            '規格ID' => [
                'id'          => 'class_names-id',
                'description' => '新規登録は空白、更新時は規格名ID',
                'required'    => false,
            ],
            '規格名' => [
                'id'          => 'class_names-name',
                'description' => '必須',
                'required'    => true,
            ],
            '規格ランク' => [
                'id'          => 'class_names-rank',
                'description' => '必須',
                'required'    => true,
            ],
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->id,
            $row->name,
            $row->rank,
        ];
    }
}
