<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClassCategory extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function classNames(): BelongsTo
    {
        return $this->belongsTo(ClassName::class, 'class_name_id', 'id');
    }

    public function csvHeader(): array
    {
        return [
            '規格(ID)' => [
                'id'          => 'class_names-id',
                'description' => '新規登録は空白、更新時は規格名ID',
                'required'    => false,
            ],
            '規格(名称)' => [
                'id'          => 'class_names-name',
                'description' => '必須',
                'required'    => true,
            ],
            '規格(ランク)' => [
                'id'          => 'class_names-rank',
                'description' => '必須',
                'required'    => true,
            ],
            '分類(ID)' => [
                'id'          => 'class_categories-id',
                'description' => '新規登録は空白、更新時は分類ID',
                'required'    => false,
            ],
            '分類(名称)' => [
                'id'          => 'class_categories-name',
                'description' => '必須',
                'required'    => true,
            ],
            '分類(単位)' => [
                'id'          => 'class_categories-unit',
                'description' => '任意',
                'required'    => false,
            ],
            '分類(ランク)' => [
                'id'          => 'class_categories-rank',
                'description' => '必須',
                'required'    => false,
            ],
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->classNames->id,
            $row->classNames->name,
            $row->classNames->rank,
            $row->id,
            $row->name,
            '空白',
            $row->rank,
        ];
    }
}
