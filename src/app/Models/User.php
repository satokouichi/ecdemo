<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Fortify\TwoFactorAuthenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function csvHeader(): array
    {
        return [
            '会員ID',
            'お名前(姓)',
            'お名前(名)',
            'お名前(フリガナ・姓)',
            'お名前(フリガナ・名)',
            '会社名',
            '郵便番号1',
            '郵便番号2',
            '都道府県',
            '住所1',
            '住所2',
            'E-MAIL',
            '性別',
            '職業',
            '誕生日',
            '初回購入日',
            '最終購入日',
            '購入回数',
            'ポイント残高',
            '登録日',
            '更新日',
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->id,
            $row->name01,
            $row->name02,
            $row->kana01,
            $row->kana02,
            $row->company_name,
            $row->zip01,
            $row->zip02,
            $row->pref ? config('pref')[$row->pref] : '',
            $row->addr01,
            $row->addr02,
            $row->email,
            $row->sex ? config('sex')[$row->sex] : '',
            '', // 職業の欄は空欄で固定
            $row->birth,
            $row->first_buy_date,
            $row->last_buy_date,
            $row->buy_times,
            $row->point,
            $row->create_date,
            $row->update_date,
        ];
    }
}
