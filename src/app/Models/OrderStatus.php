<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $guarded = ['id'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';
}
