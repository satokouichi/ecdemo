<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductReview extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function csvHeader(): array
    {
        return [
            'レビューID' => [
                'id'          => 'product_reviews-id',
                'description' => '新規登録は空白、更新時は商品ID',
                'required'    => false,
            ],
            '公開ステータス(ID)' => [
                'id'          => 'product_reviews-status',
                'description' => '必須',
                'required'    => true,
            ],
            'おすすめレベル' => [
                'id'          => 'product_reviews-recommend_level',
                'description' => '必須',
                'required'    => true,
            ],
            'タイトル' => [
                'id'          => 'product_reviews-title',
                'description' => '必須',
                'required'    => true,
            ],
            '本文' => [
                'id'          => 'product_reviews-comment',
                'description' => '必須',
                'required'    => true,
            ],
            '商品ID' => [
                'id'          => 'product_reviews-product_id',
                'description' => '必須',
                'required'    => true,
            ],
            '会員ID' => [
                'id'          => 'product_reviews-user_id',
                'description' => '必須',
                'required'    => true,
            ],
            '投稿日' => [
                'id'          => 'product_reviews-create_date',
                'description' => '必須',
                'required'    => true,
            ],
            '投稿者名' => [
                'id'          => 'product_reviews-reviewer_name',
                'description' => '任意',
                'required'    => true,
            ],
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->id,
            $row->status,
            $row->recommend_level,
            $row->title,
            $row->comment,
            $row->product_id,
            $row->user_id,
            $row->create_date ? $row->create_date->format('Y/m/d H:i:s') : '',
            $row->reviewer_name,
        ];
    }
}
