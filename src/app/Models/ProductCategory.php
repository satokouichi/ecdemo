<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductCategory extends Model
{
    protected $table = 'product_category';

    protected $guarded = ['id'];

    public $timestamps = false;    
}
