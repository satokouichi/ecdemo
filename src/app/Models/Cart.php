<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasMany;

class Cart extends Model
{
    protected $guarded = ['id'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function cartDetails(): hasMany
    {
        return $this->hasMany(CartDetail::class, 'cart_id', 'id');
    }
}
