<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasOne;

class OrderDetail extends Model
{
    protected $guarded = ['id','tax_rate','tax_rule','pending'];

    public $timestamps = false;

    public function product(): hasOne
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function productClass(): hasOne
    {
        return $this->hasOne(ProductClass::class, 'id', 'product_class_id');
    }
}
