<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function productImages(): hasMany
    {
        return $this->hasMany(ProductImage::class, 'product_id', 'id')
            ->orderBy('rank', 'desc');
    }

    public function productClasses(): hasMany
    {
        return $this->hasMany(ProductClass::class, 'product_id', 'id')
            ->whereNotNull('class_category_id1')
            ->whereNotNull('class_category_id2')
            ->orderBy('class_category_id2', 'asc');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function getCategoryIdsAttribute(): string
    {
        $count = 1;
        $categories = "";
        if ($this->categories) {
            foreach ($this->categories->sortByDesc('rank') as $categoty) {
                if ($count != 1) {
                    $categories .= ",";
                }
                $categories .= $categoty->id;
                $count++;
            } unset($categoty);
        }

        return $categories;
    }

    public function getCategoryNamesAttribute(): string
    {
        $count = 1;
        $categories = "";
        if ($this->categories) {
            foreach ($this->categories->sortByDesc('rank') as $categoty) {
                if ($count != 1) {
                    $categories .= ",";
                }
                $categories .= $categoty->category_name;
                $count++;
            } unset($categoty);
        }

        return $categories;
    }

    public function csvHeader(): array
    {
        return [
            '商品ID' => [
                'id'          => 'products-id',
                'description' => '新規登録は空白、更新時は商品ID',
                'required'    => false,
            ],
            '公開ステータス(ID)' => [
                'id'          => 'products-status',
                'description' => '必須',
                'required'    => true,
            ],
            '商品名' => [
                'id'          => 'products-name',
                'description' => '必須',
                'required'    => true,
            ],
            '規格分類1(ID)' => [
                'id'          => 'product_classes-class_category_id1',
                'description' => '必須',
                'required'    => true,
            ],
            '規格分類1(名称)' => [
                'id'          => 'product_classes-class_category_name1',
                'description' => '入力不要',
                'required'    => false,
            ],
            '規格分類1(単位)' => [
                'id'          => 'product_classes-unit',
                'description' => '入力不要',
                'required'    => false,
            ],
            'デフォルト' => [
                'id'          => 'product_classes-default_flg',
                'description' => '必須',
                'required'    => true,
            ],
            'セット数' => [
                'id'          => 'product_classes-set',
                'description' => '必須',
                'required'    => true,
            ],
            '割引パターン' => [
                'id'          => 'products-discount_pattern',
                'description' => '必須',
                'required'    => true,
            ],
            'ショップ用メモ欄' => [
                'id'          => 'products-note',
                'description' => '任意',
                'required'    => false,
            ],
            '商品説明(一覧)' => [
                'id'          => 'products-description_list',
                'description' => '任意',
                'required'    => false,
            ],
            '商品説明(詳細)' => [
                'id'          => 'products-description_detail',
                'description' => '任意',
                'required'    => false,
            ],
            '検索ワード' => [
                'id'          => 'products-search_word',
                'description' => '任意',
                'required'    => false,
            ],
            '商品画像' => [
                'id'          => 'product_images-file_name',
                'description' => '任意',
                'required'    => false,
            ],
            '画像テキスト' => [
                'id'          => 'product_images-file_text',
                'description' => '任意',
                'required'    => false,
            ],
            '商品カテゴリ(ID)' => [
                'id'          => 'product_category-category_id',
                'description' => '任意',
                'required'    => true,
            ],
            '商品コード' => [
                'id'          => 'product_classes-product_code',
                'description' => '任意',
                'required'    => false,
            ],
            '在庫ID' => [
                'id'          => 'product_classes-zaiko_id',
                'description' => '任意',
                'required'    => false,
            ],
            '内容量' => [
                'id'          => 'product_classes-dose',
                'description' => '任意',
                'required'    => false,
            ],
            '助数詞' => [
                'id'          => 'products-shape',
                'description' => '任意',
                'required'    => false,
            ],
            '在庫数' => [
                'id'          => 'product_classes-stock',
                'description' => '任意',
                'required'    => false,
            ],
            '在庫数無制限フラグ' => [
                'id'          => 'product_classes-stock_unlimited',
                'description' => '任意',
                'required'    => false,
            ],
            '取扱い' => [
                'id'          => 'product_classes-handling_flg',
                'description' => '任意',
                'required'    => false,
            ],
            '規格ステータス' => [
                'id'          => 'product_classes-status',
                'description' => '任意',
                'required'    => false,
            ],
            '販売制限数' => [
                'id'          => 'product_classes-sale_limit',
                'description' => '任意',
                'required'    => false,
            ],
            '通常価格' => [
                'id'          => 'product_classes-price01',
                'description' => '任意',
                'required'    => true,
            ],
            '販売価格' => [
                'id'          => 'product_classes-price02',
                'description' => '任意',
                'required'    => false,
            ],
            'おすすめテキスト' => [
                'id'          => 'products-recommend_text',
                'description' => '任意',
                'required'    => false,
            ],
            '発送業者(ID)' => [
                'id'          => 'products-trader_id',
                'description' => '任意',
                'required'    => false,
            ],
            '製薬会社(ID)' => [
                'id'          => 'product_makers-id',
                'description' => '任意',
                'required'    => false,
            ],
            '種別(ID)' => [
                'id'          => 'product_kinds-id',
                'description' => '任意',
                'required'    => false,
            ],
            '同類(ID)' => [
                'id'          => 'product_kinds-id2',
                'description' => '任意',
                'required'    => false,
            ],
            '提案カテゴリ(ID)' => [
                'id'          => 'recommend_category-id',
                'description' => '任意',
                'required'    => false,
            ],
            '疾患(ID)' => [
                'id'          => 'diserses-id',
                'description' => '任意',
                'required'    => false,
            ],
        ];
    }

    public function csvRow($row): array
    {
        $row['set'] = implode(',', $row['set']);
        $row['product_images'] = implode(',', $row['product_images']);
        $row['product_alts'] = implode(',', $row['product_alts']);
        $row['categories'] = implode(',', $row['categories']);

        return [
            $row['product_id'],
            $row['status'],
            $row['product_name'],
            $row['class_category1_id'],
            $row['class_category1_name'],
            '', // 単位に関しての仕様は保留中
            $row['default_flg'],
            $row['set'],
            $row['discount'],
            $row['note'],
            $row['description_list'],
            $row['description_detail'],
            $row['search_word'],
            $row['product_code'],
            $row['zaiko_id'],
            $row['dose'],
            $row['shape'],
            $row['stock'],
            $row['stock_unlimited'],
            $row['handling_flg'],
            $row['sale_limit'],
            $row['price01'],
            $row['price02'],
            $row['trader_id'],
            $row['product_images'],
            $row['product_alts'],
            $row['categories'],
        ];
    }
}
