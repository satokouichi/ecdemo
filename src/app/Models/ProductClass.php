<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductClass extends Model
{
    protected $guarded = ['id'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function classCategories1(): BelongsTo
    {
        return $this->belongsTo(ClassCategory::class, 'class_category_id1', 'id');
    }

    public function classCategories2(): BelongsTo
    {
        return $this->belongsTo(ClassCategory::class, 'class_category_id2', 'id');
    }
}
