<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model
{
    protected $guarded = ['id','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_category')
            ->where('del_flg', 0);
    }

    public function csvHeader(): array
    {
        return [
            'カテゴリID' => [
                'id'          => 'categories-id',
                'description' => '新規登録は空白、更新時は商品ID',
                'required'    => false,
            ],
            'カテゴリ名' => [
                'id'          => 'categories-category_name',
                'description' => '必須',
                'required'    => true,
            ],
            '親カテゴリID' => [
                'id'          => 'categories-parent_category_id',
                'description' => '親カテゴリは空白、子カテゴリは親カテゴリIDを入力',
                'required'    => false,
            ],
            '階層' => [
                'id'          => 'categories-level',
                'description' => '親カテゴリは1、子カテゴリは2を入力',
                'required'    => true,
            ],
            'ディスクリプション' => [
                'id'          => 'categories-description',
                'description' => '任意',
                'required'    => false,
            ],
            'ランク' => [
                'id'          => 'categories-rank',
                'description' => '必須',
                'required'    => true,
            ],
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->id,
            $row->category_name,
            $row->parent_category_id,
            $row->level,
            $row->description,
        ];
    }
}
