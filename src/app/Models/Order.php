<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\hasOne;

class Order extends Model
{
    protected $guarded = ['id','order_tel1','order_tel2','order_tel3','order_fax1','order_fax2','order_fax3','charge','del_flg'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function orderDetails(): hasMany
    {
        return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    }

    public function orderPayments(): hasMany
    {
        return $this->hasMany(OrderPayment::class, 'order_id', 'id');
    }

    public function trackingNumbers(): hasMany
    {
        return $this->hasMany(TrackingNumber::class, 'order_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class);
    }

    public function orderStatus(): hasOne
    {
        return $this->hasOne(OrderStatus::class, 'id', 'status');
    }

    public function getPaymentSumAttribute(): int
    {
        $sum = 0;
        if ($this->orderPayments) {
            foreach ($this->orderPayments as $orderPayment) {
                $sum = $sum + $orderPayment->pay;
            } unset($orderPayment);
        }

        return $sum;
    }

    public function getTrackingSumAttribute(): string
    {
        $cnt = 0;
        $sum = "";
        if ($this->trackingNumbers) {
            foreach ($this->trackingNumbers as $trackingNumber) {
                if ($cnt > 0) {
                    $sum .= ",";
                }
                $sum .= $trackingNumber->number;
                $cnt++;
            } unset($trackingNumber);
        }

        return $sum;
    }

    public function csvHeader(): array
    {
        return [
            '注文番号',
            '会員ID',
            'お名前(姓)',
            'お名前(名)',
            'お名前(フリガナ・姓)',
            'お名前(フリガナ・名)',
            '小計',
            '送料',
            '合計',
            '使用ポイント',
            '値引き',
            'お支払い合計',
            '支払い方法',
            '注文日時',
            '対応状況',
            '発送完了日時',
            '郵便番号1',
            '郵便番号2',
            '都道府県',
            '住所1',
            '住所2',
            '性別',
            '端末種別ID',
            '加算ポイント',
            'SHOPメモ',
            'メールアドレス',
        ];
    }

    public function csvRow($row): array
    {
        return [
            $row->id,
            $row->user_id,
            $row->order_name01,
            $row->order_name02,
            $row->order_kana01,
            $row->order_kana02,
            $row->subtotal,
            $row->delivery_fee_total,
            $row->total,
            $row->use_point,
            $row->discount,
            $row->payment_total,
            $row->payment_method,
            $row->order_date ? $row->order_date->format('Y/m/d H:i:s') : '',
            $row->orderStatus ? $row->orderStatus->name : '',
            $row->commit_date ? $row->commit_date->format('Y/m/d H:i:s') : '',
            $row->order_zip01,
            $row->order_zip02,
            $row->order_pref ? config('pref')[$row->order_pref] : '',
            $row->order_addr01,
            $row->order_addr02,
            $row->order_sex ? config('sex')[$row->order_sex] : '',
            '10', // 端末の欄は10で固定
            $row->add_point,
            $row->note,
            $row->order_email,
        ];
    }
}
