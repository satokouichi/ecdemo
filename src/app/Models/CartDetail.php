<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasOne;

class CartDetail extends Model
{
    protected $guarded = ['id'];

    const CREATED_AT = 'create_date';
    const UPDATED_AT = 'update_date';

    public function product(): hasOne
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function productClass(): hasOne
    {
        return $this->hasOne(ProductClass::class, 'id', 'product_class_id');
    }
}
