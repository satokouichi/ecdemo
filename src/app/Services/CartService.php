<?php

namespace App\Services;

use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ProductClass\ProductClassRepositoryInterface AS ProductClassRepository;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;
use App\Models\Cart;
use App\Models\CartDetail;

class CartService
{
    private $productRepository;
    private $productClassRepository;
    private $cartRepository;
    private $cartDetailRepository;

    public function __construct (
        ProductRepository $productRepository,
        ProductClassRepository $productClassRepository,
        CartRepository $cartRepository,
        CartDetailRepository $cartDetailRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->productClassRepository = $productClassRepository;
        $this->cartRepository = $cartRepository;
        $this->cartDetailRepository = $cartDetailRepository;
    }

    public function setCart($cartKey, $request)
    {
        $productId = $request->input('product_id');
        $productClassId = $request->input('product_class_id');
        $quantity = $request->input('quantity');
        $cart = $this->cartRepository->getCart($cartKey);
        $cartDetail = $this->cartDetailRepository->getCartDetail($cart->id, $productClassId);

        // カート商品を更新
        if ($cartDetail) {
            $cartDetail->quantity += $request->input('quantity');
            $cartDetail->save();
        } else {
            $productClass = $this->productClassRepository->getProductClass([
                    'id'         => $productClassId
                ])->first();
            CartDetail::create([
                'cart_id' => $cart->id,
                'cart_key' => $cartKey,
                'product_id' => $productId,
                'product_class_id' => $productClassId,
                'price' => $productClass->price02,
                'quantity' => $quantity,
            ]);
        }

        // カート商品一覧を取得
        $cartDetails = $this->cartDetailRepository->getCartDetails($cart->id);

        // 合計
        $total = 0;
        foreach ($cartDetails as $cartDetail) {
            $total += ($cartDetail->price * $cartDetail->quantity);
        } unset($cartDetail);

        // 送料
        $deliveryFeeTotal = 700;
        if ($total >= 7000) {
            $deliveryFeeTotal = 0;
        }

        // 加算ポイント
        $addPoint = intval(round($total * 0.05));

        // カートを更新
        $cart->delivery_fee_total = $deliveryFeeTotal;
        $cart->total = $total;
        $cart->add_point = $addPoint;
        $cart->use_point = 0;
        $cart->save();
        
        return $request;
    }

    public function updateCart($cartKey, $request)
    {
        $productId = $request->input('product_id');
        $productClassId = $request->input('product_class_id');
        $quantity = $request->input('quantity');
        $cart = $this->cartRepository->getCart($cartKey);
        
        // カート数量を更新
        $cartDetail = $this->cartDetailRepository->getCartDetail($cart->id, $request->input('product_class_id'));
        if ($cartDetail) {
            $cartDetail->quantity = $quantity;
            $cartDetail->save();
        }

        // カート商品一覧を取得
        $cartDetails = $this->cartDetailRepository->getCartDetails($cart->id);

        // 合計
        $total = 0;
        foreach ($cartDetails as $cartDetail) {
            $total += ($cartDetail->price * $cartDetail->quantity);
        } unset($cartDetail);

        // 送料
        $deliveryFeeTotal = 700;
        if ($total >= 7000) {
            $deliveryFeeTotal = 0;
        }

        // 加算ポイント
        $addPoint = intval(round($total * 0.05));

        // カートを更新
        $cart->delivery_fee_total = $deliveryFeeTotal;
        $cart->total = $total;
        $cart->add_point = $addPoint;
        $cart->use_point = 0;
        $cart->save();
    }
}
