<?php

namespace App\Services;

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ProductReview\ProductReviewRepositoryInterface AS ProductReviewRepository;

class ImageService
{
    private $productRepository;
    private $productReviewRepository;

    public function __construct (
        ProductRepository $productRepository,
        ProductReviewRepository $productReviewRepository,
    )
    {
        $this->productRepository = $productRepository;
        $this->productReviewRepository = $productReviewRepository;
    }

    public function saveImage($request, $type, $w)
    {
        $file = $request->file('image');
        if ( !$file ) {
            return;
        }

        // 画像名を取得
        $fileName = $file->getClientOriginalName();

        // 画像を横幅300px・縦幅アスペクト比維持の自動サイズへリサイズ
        $image = Image::make($file)
            ->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });

        // 画像を保存
        $storage = Storage::put('/public/images/review/main/'.$fileName, (string) $image->encode());

        switch ($type){
            case 'review':
                $request->merge(['reviewer_url' => pathinfo($fileName)['filename']]);
                $this->productReviewRepository->updateProductReviewByAdmin($request, $request->input('id'));
                break;
        }

        return $storage;
    }

    public function saveTempImage($request, $w)
    {
        $file = $request->file('image');
        if ( !$file ) {
            return;
        }

        // 画像名を取得
        $fileName = $file->getClientOriginalName();

        // 画像を横幅300px・縦幅アスペクト比維持の自動サイズへリサイズ
        $image = Image::make($file)
            ->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });

        // 画像を保存
        $storage = Storage::put('/public/images/review/temp/'.$fileName, (string) $image->encode());
        if ( !$storage ) {
            return false;
        }

        return $fileName;
    }

    public function moveImage($review)
    {
        $fileName = $review->reviewer_url;
        $stage = '/public/images/review/temp/'.$fileName;
        $main = '/public/images/review/perm/'.$fileName;

        if (Storage::exists($main) === false) {
            Storage::move($stage, $main);
        }

        return;
    }
}
