<?php

namespace App\Services;

use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;

class ProductService
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getDownloadCsvParams($products)
    {
        $params = [];
        foreach ($products as $product) {
            foreach ($product->productClasses as $productClass) {
                $p = $product->id;
                $cc1 = $productClass->classCategories1->id;
                $cc2 = $productClass->classCategories2->id;

                $params[$p][$cc1]['product_id']           = $product->id; // 商品ID
                $params[$p][$cc1]['status']               = $product->status; // 公開ステータス(ID)
                $params[$p][$cc1]['product_name']         = $product->name; // 商品名
                $params[$p][$cc1]['class_category1_id']   = $productClass->classCategories1->id; // 規格分類1(ID)
                $params[$p][$cc1]['class_category1_name'] = $productClass->classCategories1->name; // 規格分類1(名称)
                $params[$p][$cc1]['default_flg']          = $productClass->default_flg; // デフォルト
                $params[$p][$cc1]['set'][]                = $productClass->classCategories2->name; // セット数
                $params[$p][$cc1]['discount']             = $product->free_area; // 割引パターン
                $params[$p][$cc1]['note']                 = $product->note; // ショップ用メモ欄
                $params[$p][$cc1]['description_list']     = $product->description_list; // 商品説明(一覧)
                $params[$p][$cc1]['description_detail']   = $product->description_detail; // 商品説明(詳細)
                $params[$p][$cc1]['search_word']          = $product->search_word; // 検索ワード
                $params[$p][$cc1]['product_code']         = $productClass->product_code; // 商品コード
                $params[$p][$cc1]['zaiko_id']             = $productClass->zaiko_id; // 在庫ID
                $params[$p][$cc1]['dose']                 = $productClass->dose; // 内容量
                $params[$p][$cc1]['shape']                = $product->shape; // 助数詞
                $params[$p][$cc1]['stock']                = $productClass->stock; // 在庫数
                $params[$p][$cc1]['stock_unlimited']      = $productClass->stock_unlimited; // 在庫数無制限フラグ
                $params[$p][$cc1]['handling_flg']         = $productClass->handling_flg; // 取扱い
                $params[$p][$cc1]['status']               = $productClass->status; // 規格ステータス
                $params[$p][$cc1]['sale_limit']           = ""; // 販売制限数は空白で固定
                $params[$p][$cc1]['price01']              = $productClass->price01; // 通常価格
                $params[$p][$cc1]['price02']              = $productClass->price02; // 販売価格
                $params[$p][$cc1]['trader_id']            = $productClass->trader_id; // 発送業者ID

                // 商品画像・画像テキスト
                $params[$p][$cc1]['product_images'] = [];
                $params[$p][$cc1]['product_alts'] = [];
                if ($product->productImages) {
                    foreach ($product->productImages as $productImage) {
                        if (in_array($productImage->file_name, $params[$p][$cc1]['product_images']) === false) {
                            $params[$p][$cc1]['product_images'][] = $productImage->file_name;
                        }
                        if (in_array($productImage->file_name, $params[$p][$cc1]['product_alts']) === false) {
                            $params[$p][$cc1]['product_alts'][] = $productImage->file_text;
                        }
                    } unset($productImage);
                }
                
                // 商品カテゴリ(ID)
                $params[$p][$cc1]['categories'] = [];
                if ($product->categories) {
                    foreach ($product->categories as $category) {
                        if (in_array($category->id, $params[$p][$cc1]['categories']) === false) {
                            $params[$p][$cc1]['categories'][] = $category->id;
                        }
                    } unset($category);
                }

            } unset($productClass);
        } unset($product);

        return $params;
    }
}
