<?php

namespace App\Services;

use SplFileObject;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Repositories\Discount\DiscountRepositoryInterface AS DiscountRepository;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;
use App\Repositories\ClassName\ClassNameRepositoryInterface AS ClassNameRepository;
use App\Repositories\ClassCategory\ClassCategoryRepositoryInterface AS ClassCategoryRepository;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ProductClass\ProductClassRepositoryInterface AS ProductClassRepository;
use App\Repositories\ProductCategory\ProductCategoryRepositoryInterface AS ProductCategoryRepository;
use App\Repositories\ProductImage\ProductImageRepositoryInterface AS ProductImageRepository;
use App\Repositories\ProductReview\ProductReviewRepositoryInterface AS ProductReviewRepository;
use App\Models\Discount;
use App\Models\Category;
use App\Models\ClassName;
use App\Models\ClassCategory;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductImage;
use App\Models\ProductClass;
use App\Models\ProductReview;

class CsvImportService
{
    private $discountRepository;
    private $categoryRepository;
    private $classNameRepository;
    private $classCategoryRepository;
    private $productRepository;
    private $productClassRepository;
    private $productCategoryRepository;
    private $productImageRepository;
    private $productReviewRepository;

    public function __construct (
        DiscountRepository $discountRepository,
        CategoryRepository $categoryRepository,
        ClassNameRepository $classNameRepository,
        ClassCategoryRepository $classCategoryRepository,
        ProductRepository $productRepository,
        ProductClassRepository $productClassRepository,
        productCategoryRepository $productCategoryRepository,
        productImageRepository $productImageRepository,
        ProductReviewRepository $productReviewRepository
    )
    {
        $this->discountRepository = $discountRepository;
        $this->categoryRepository = $categoryRepository;
        $this->classNameRepository = $classNameRepository;
        $this->classCategoryRepository = $classCategoryRepository;
        $this->productRepository = $productRepository;
        $this->productClassRepository = $productClassRepository;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productReviewRepository = $productReviewRepository;
        $this->errors = [];
    }

    private function getHeaders($csvHeader)
    {
        $headers = [];
        foreach ($csvHeader as $key => $val) {
            $headers['id'][] = $val['id'];
            $headers['name'][$val['id']] = $key;
            $headers['require'][$val['id']] = $val['required'];
        } unset($key, $val);

        return $headers;
    }

    public function csvImport($request, $csvHeader, $csvName)
    {
        // ヘッダーを取得
        $headers = $this->getHeaders($csvHeader);

        // 割引パターンを取得
        $discountPatterns = [];
        $dbDiscounts = $this->discountRepository->getDiscounts()->get();
        if ( !empty($dbDiscounts) ) {
            foreach ($dbDiscounts as $dbDiscount) {
                if (in_array($dbDiscount->pattern, $discountPatterns) === false) {
                    $discountPatterns[] = $dbDiscount->pattern;
                }
            } unset ($dbDiscount);
        }

        // セットパターンを取得
        $setPatterns = [];
        $dbSets = $this->classCategoryRepository->getClassCategory([
                'class_names-id' => 1,
            ])->get();
        if ( !empty($dbSets) ) {
            foreach ($dbSets as $dbset) {
                $setPatterns[] = intval($dbset->name);
            } unset ($dbset);
        }

        setlocale(LC_ALL, 'ja_JP.UTF-8');
        // CSVが選択されていない場合
        if (is_null($request->file($csvName."_csv"))) {
            $this->errors[] = "CSVを選択してください";

            return $this->errors;
        }
        $filePath = $request->file($csvName."_csv")->path();
        // CSVファイル名が誤りの場合
        if ($request->file($csvName."_csv")->getClientOriginalName() != $csvName.".csv") {
            $this->errors[] = "CSVファイル名は「".$csvName.".csv」にしてください";

            return $this->errors;
        }

        $file = new \SplFileObject($filePath);
        $file->setFlags(
            \SplFileObject::READ_CSV     |  // CSVとする
            \SplFileObject::READ_AHEAD   |  // 先読み／巻き戻しで読み込み
            \SplFileObject::SKIP_EMPTY   |  // 空行飛ばす（AHEADも有効である必要がある）
            \SplFileObject::DROP_NEW_LINE   // 行末の改行を読み飛ばす
        );

        $loop = 1;
        $categoryVals = [];
        $classNameVals = [];
        $classCategoryVals = [];
        $productVals = [];
        $productClassVals = [];
        $productCategoryVals = [];
        $productImageVals = [];
        $reviewVals = [];
        foreach ( $file as $num => $row ) {
            // CSVヘッダー数が合わない場合
            if ( $loop == 1 && count($headers['id']) != count($row) ) {
                $this->errors[] = "CSVヘッダー数が一致しません。";

                return $this->errors;
            }
            // データを配列に格納
            $data = [];
            for ( $i = 0; $i < count($row); $i++ ) {
                $value = mb_convert_encoding($row[$i], 'UTF-8', 'SJIS');
                $data[$headers['id'][$i]] = $value ? trim($value) : null;
            }
            // ループ1回目でヘッダーを確認
            if ( $loop == 1 ) {
                // ヘッダー項目が一致しない場合
                if ( !empty(array_diff($headers['name'], $data)) ) {
                    $this->errors[] = "CSVのヘッダー項目を確認してください。";

                    return $this->errors;
                }
            } else {
                // CSVの行数を取得
                $line = $file->key() + 1;
                
                // DB別のバリデーションチェック
                $categoryVal = [];
                $classNameVal = [];
                $classCategoryVal = [];
                $productVal = [];
                $productClassVal = [];
                $productCategoryVal = [];
                $productImageVal = [];
                $reviewVal = [];
                switch ($csvName) {
                    // カテゴリCSV
                    case "category":
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'categories-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $categoryVal[$key] = $val;
                            }
                        } unset( $key, $val );

                        $this->categoryValidate($headers, $categoryVal, $line);

                        break;

                    // 規格名・分類CSV
                    case "classcategory":
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'class_names-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $classNameVal[$key] = $val;
                            }
                        } unset( $key, $val );

                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'class_categories-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $classCategoryVal[$key] = $val;
                            }
                            if ( $key == 'class_names-name' ) {
                                $classCategoryVal['class_names-name'] = $val;
                            }
                        } unset( $key, $val );

                        $this->classNameValidate($headers, $classNameVal, $line);
                        $this->classCategoryValidate($headers, $classCategoryVal, $line);

                        break;

                    // 商品CSV
                    case "product":
                        // 商品
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'products-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $productVal[$key] = $val;
                            }
                            if ( $key == 'product_classes-class_category_id1' ) {
                                $productVal['product_classes-class_category_id1'] = $val;
                            }
                        } unset( $key, $val );

                        // 商品規格
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'product_classes-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $productClassVal[$key] = $val;
                            }
                            if ( $key == 'products-name' ) {
                                $productClassVal['products-name'] = $val;
                            }
                        } unset( $key, $val );
                        
                        // 商品カテゴリ
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'product_category-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $productCategoryVal[$key] = $val;
                            }
                            if ( $key == 'products-name' ) {
                                $productCategoryVal['products-name'] = $val;
                            }
                        } unset( $key, $val );

                        // 商品画像
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'product_images-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $productImageVal[$key] = $val;
                            }
                            if ( $key == 'products-id' ) {
                                $productImageVal['products-id'] = $val;
                            }
                            if ( $key == 'products-name' ) {
                                $productImageVal['products-name'] = $val;
                            }
                            if ( $key == 'product_classes-class_category_id1' ) {
                                $productImageVal['product_classes-class_category_id1'] = $val;
                            }
                        } unset( $key, $val );

                        $this->productValidate($headers, $productVal, $line, $discountPatterns);
                        $this->productClassValidate($headers, $productClassVal, $line, $setPatterns);
                        $this->productCategoryValidate($headers, $productCategoryVal, $line);
                        $this->productImageValidate($headers, $productImageVal, $line);
                        
                        break;

                    // レビューCSV
                    case "review":
                        foreach ( $data as $key => $val ) {
                            if ( strpos($key,'product_reviews-') !== false ) {
                                if ( strpos($key,'-') !== false ) {
                                    $key = substr($key, strrpos($key, '-') + 1, strlen($key));
                                }
                                $reviewVal[$key] = $val;
                            }
                        } unset( $key, $val );

                        $this->reviewValidate($headers, $reviewVal, $line);

                        break;
                }

                // カテゴリに追加
                if ($categoryVal) {
                    $categoryVals[$line] = $categoryVal;
                }

                // 規格に追加
                if ($classNameVal) {
                    $classNameVals[$line] = $classNameVal;
                }

                // 分類に追加
                if ($classCategoryVal) {
                    $classCategoryVals[$line] = $classCategoryVal;
                }

                // 商品に追加
                if ($productVal) {
                    $productVals[$line] = $productVal;
                }

                // 商品規格に追加
                if ($productClassVal) {
                    $productClassVals[$line] = $productClassVal;
                }

                // 商品とカテゴリ紐づけに追加
                if ($productCategoryVal) {
                    $productCategoryVals[$line] = $productCategoryVal;
                }

                // 商品と画像紐づけに追加
                if ($productImageVal) {
                    $productImageVals[$line] = $productImageVal;
                }

                // レビューに追加
                if ($reviewVal) {
                    $reviewVals[$line] = $reviewVal;
                }
            }

            $loop++;
        }

        // dump($categoryVals);
        // dump($classNameVals);
        // dump($classCategoryVals);
        // dump($productVals);
        // dump($productClassVals);
        // dump($productCategoryVals);
        // dump($productImageVals);
        // dump($reviewVals);
        // dump($this->errors);
        // dd('-----');

        // エラーがある場合は処理を抜ける
        if (!empty($errors)) {
            return $this->errors;
        }

        // カテゴリ登録
        $this->importCategory($categoryVals);

        // 規格・分類登録
        $this->importClassName($classNameVals, $classCategoryVals);

        // 商品登録
        $this->importProduct($productVals, $productClassVals, $productCategoryVals, $productImageVals);

        // レビュー登録
        $this->importReview($reviewVals);

        return;
    }

    // カテゴリ登録
    private function importCategory($categoryVals)
    {
        if ( empty($categoryVals) ) {
            return;
        }

        foreach ( $categoryVals as $categoryVal ) {
            if ( $categoryVal['id'] ) {
                $this->categoryRepository->updateCategory($categoryVal, $categoryVal['id']);
            } else {
                $this->categoryRepository->createCategory($categoryVal);
            }
        } unset ( $categoryVal );
    }

    // 規格登録
    private function importClassName($classNameVals, $classCategoryVals)
    {
        if ( empty($classNameVals) || empty($classCategoryVals)) {
            return;
        }

        foreach ($classNameVals as $classNameVal) {
            if ( $classNameVal['id'] ) {
                if ( $this->classNameRepository->updateClassName($classNameVal, $classNameVal['id']) ) {
                    $this->importClassCategory($classCategoryVals);
                }
            } else {
                if ( count($this->classNameRepository->getClassName(['name' => $classNameVal['name']])->get()) < 1 ) {
                    if ( $this->classNameRepository->createClassName($classNameVal) ) {
                        $this->importClassCategory($classCategoryVals);
                    }
                }
            }
        } unset ( $classNameVal );        
    }

    // 分類登録
    private function importClassCategory($classCategoryVals)
    {
        if ( empty($classCategoryVals) ) {
            return;
        }

        foreach ($classCategoryVals as $classCategoryVal) {
            $className = $this->classNameRepository->getClassName(['name' => $classCategoryVal['class_names-name']])->first();
            if ( $className ) {
                $classCategoryVal['class_name_id'] = $className->id;
                if ( $classCategoryVal['id'] ) {
                    $this->classCategoryRepository->updateClassCategory($classCategoryVal, $classCategoryVal['id']);
                } else {
                    $classCategory = $this->classCategoryRepository->getClassCategory([
                            'class_names-name' => $className->name,
                            'name'             => $classCategoryVal['name']
                        ])->get();
                    if ( count($classCategory) < 1 ) {
                        $this->classCategoryRepository->createClassCategory($classCategoryVal);
                    }
                }
            }
        } unset ( $classCategoryVal );

    }

    // 商品登録
    private function importProduct($productVals, $productClassVals, $productCategoryVals, $productImageVals)
    {
        if ( empty($productVals) || empty($productClassVals) || empty($productCategoryVals) || empty($productImageVals) ) {
            return;
        }

        $productIds = [];
        foreach ( $productVals as $line => $productVal ) {
            if ( $productVal['id'] ) {
                if ( $this->productRepository->updateProduct($productVal, $productVal['id']) ) {
                    $product = $this->productRepository->getProduct(['name' => $productVal['name']])->first();
                    $productIds[$line] = $productVal['id'];
                }
            } else {
                if ( count($this->productRepository->getProduct(['name' => $productVal['name']])->get()) < 1 ) {
                    $product = $this->productRepository->createProduct($productVal);
                    if ( $product ) {
                        $productIds[$line] = $product->id;
                    }
                } else {
                    // 登録済みの商品を検索
                    $product = $this->productRepository->getProduct(['name' => $productVal['name']])->first();
                    if ( $product ) {
                        $productIds[$line] = $product->id;
                    }
                }
            }
            $discountPatterns[$line] = $productVal['discount_pattern'];
        }

        // dump($productIds);
        // dump($discountPatterns);

        // 商品規格
        $this->importProductClass($productClassVals, $productIds, $discountPatterns);

        // 商品カテゴリ
        $this->importProductCategory($productCategoryVals, $productIds);

        // 商品画像
        $this->importProductImage($productImageVals, $productIds);

        // dd('----------------------');
    }

    // 商品規格登録
    private function importProductClass($productClassVals, $productIds, $discountPatterns)
    {
        if ( empty($productClassVals) || empty($productIds) || empty($discountPatterns) ) {
            return;
        }

        $params = [];
        foreach ( $productClassVals as $line => $val ) {
            if ( array_key_exists($line, $productIds) === false ) { continue; }
            if ( array_key_exists($line, $discountPatterns) === false ) { continue; }
            $params = $val;
            $sets = preg_replace('/[　]/u', '', explode(',', $val['set']));
            $price = $val['price01'];
            foreach ( $sets as $key => $set ) {
                $discountPattern = $this->discountRepository->getDiscount([
                    'pattern'   => $discountPatterns[$line],
                    'order_num' => $set
                    ])->first();
                $discount = 0;
                if ( $discountPattern ) {
                    $discount = $discountPattern->discount;
                }
                $params['price01'] = $price * $set;
                $params['price02'] = ($price - round($price * ($discount / 100))) * $set;
                $dbset = $this->classCategoryRepository->getClassCategory([
                        'class_names-id' => 1, // セットパターンの規格IDは1で固定
                        'name'           => $set,
                    ])->first();
                if ( !$dbset ) { continue; }
                $productClass = $this->productClassRepository->getProductClass([
                        'product_id'         => $productIds[$line],
                        'class_category_id1' => $val['class_category_id1'],
                        'class_category_id2' => $dbset->id,
                    ])->first();
                if ( $productClass ) {
                    $params['stock']              = $productClass->stock;
                    $params['stock_unlimited']    = $productClass->stock_unlimited;
                    $this->productClassRepository->updateProductClass($params, $productClass->id);
                } else {
                    $params['product_id']         = $productIds[$line];
                    $params['class_category_id2'] = $set;
                    $params['stock']              = 0;
                    $params['stock_unlimited']    = 0;
                    $this->productClassRepository->createProductClass($params);
                }                    
            } unset ( $key, $set );          
        } unset ( $line, $val );
    }

    // 商品カテゴリ登録
    private function importProductCategory($productCategoryVals, $productIds)
    {
        if ( empty($productCategoryVals) || empty($productIds) ) {
            return;
        }

        // 重複分の商品IDを削除
        $productIds = array_unique($productIds);

        // 一旦全削除
        foreach ( $productIds as $productId ) {
            $this->productCategoryRepository->deleteProductCategoryAll($productId);
        } unset( $productId );

        // 登録処理
        $params = [];
        foreach ( $productCategoryVals as $line => $val ) {
            if ( array_key_exists($line, $productIds) === false ) { continue; }
            $params = $val;
            $count = 1;
            $categoryIds = preg_replace('/[　]/u', '', explode(',', $val['category_id']));
            foreach ( $categoryIds as $categoryId ) {
                $params['product_id']  = $productIds[$line];
                $params['category_id'] = $categoryId;
                $params['rank']        = $count;
                $count++;
                $this->productCategoryRepository->createProductCategory($params);
            } unset ( $categoryId );
        } unset( $val );
    }

    // 商品画像登録
    private function importProductImage($productImageVals, $productIds)
    {
        if ( empty($productImageVals) || empty($productIds) ) {
            return;
        }

        // 一旦全削除
        foreach ( array_unique($productIds) as $productId ) {
            $this->productImageRepository->deleteProductImageAll($productId);
        } unset( $productId );

        // 登録処理
        $params = [];
        foreach ( $productImageVals as $line => $val ) {
            if ( array_key_exists($line, $productIds) === false ) { continue; }
            $params = $val;
            $count = 1;
            $fileNames = preg_replace('/[　]/u', '', explode(',', $val['file_name']));
            $fileTexts = preg_replace('/[　]/u', '', explode(',', $val['file_text']));
            foreach ( $fileNames as $key => $fileName ) {
                $params['product_id'] = $productIds[$line];
                $params['file_name']  = $fileName;
                $params['file_text']  = $fileTexts[$key];
                $params['rank']       = $count;
                $count++;
                $this->productImageRepository->createProductImage($params);
            } unset ( $fileName );
        } unset( $val );
    }

    // レビュー登録
    private function importReview($reviewVals)
    {
        if ( empty($reviewVals) ) {
            return;
        }

        foreach ($reviewVals as $reviewVal) {
            if ( $reviewVal['id'] ) {
                $this->productReviewRepository->updateProductReview($reviewVal, $reviewVal['id']);
            } else {
                $this->productReviewRepository->createProductReview($reviewVal);
            }
        } unset ( $reviewVal );
    }

    private function categoryValidate($headers, $data, $line)
    {
        $prefix = "categories-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
            }
        }
        // カテゴリID
        if ( $data['id'] ) {
            if ( !preg_match('/^\d+$/', $data['id']) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."は数字で入力してください。";
            } else {
                $category = $this->categoryRepository->getCategory($data['id'])->get();
                if (!$category) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."が存在しません。";
                }
            }
        }
        // 親カテゴリID
        if ( $data['parent_category_id'] && !preg_match('/^\d+$/', $data['parent_category_id']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'parent_category_id']."は数字で入力してください。";
        }
        // カテゴリ名
        if ( $data['category_name'] && mb_strlen($data['category_name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'category_name']."は50文字以内入力してください。";
        }
        // ディスクリプション
        if ( $data['description'] && mb_strlen($data['description']) > 200 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'description']."は200文字以内入力してください。";
        }
        // 階層
        if ( $data['level'] && !preg_match('/^\d+$/', $data['level']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'level']."は数字で入力してください。";
        }
        // ランク
        if ( $data['rank'] && !preg_match('/^\d+$/', $data['rank']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'rank']."は数字で入力してください。";
        }
    }

    private function classNameValidate($headers, $data, $line)
    {
        $prefix = "class_names-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
            }
        }
        // 規格(ID)
        if ( $data['id'] ) {
            if ( !preg_match('/^\d+$/', $data['id']) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."は数字で入力してください。";
            } else {
                $className = $this->classNameRepository->getClassName(['id' => $data['id']])->first();
                if ( !$className ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."が存在しません。";
                }
            }
        }
        // 規格(名称)
        if ( $data['name'] && mb_strlen($data['name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'name']."は50文字以内入力してください。";
        }
        // 規格(ランク)
        if ( $data['rank'] && !preg_match('/^\d+$/', $data['rank']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'rank']."は数字で入力してください。";
        }
    }

    private function classCategoryValidate($headers, $data, $line)
    {
        $prefix = "class_categories-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'class_names-name' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 分類(ID)
        if ( $data['id'] ) {
            if ( !preg_match('/^\d+$/', $data['id']) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."は数字で入力してください。";
            } else {
                $classCategory = $this->classCategoryRepository->getClassCategory([
                    'id' => $data['id']
                    ])->first();
                if ( !$classCategory ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."が存在しません。";
                }
            }
        }
        // 分類(名称)
        if ( $data['name'] && mb_strlen($data['name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'name']."は50文字以内入力してください。";
        }
        // 分類(ランク)
        if ( $data['rank'] && !preg_match('/^\d+$/', $data['rank']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'rank']."は数字で入力してください。";
        }
    }

    private function productValidate($headers, $data, $line, $discountPatterns)
    {
        $prefix = "products-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'products-name' && $key != 'product_classes-class_category_id1' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 商品ID
        if ( $data['id'] ) {
            if ( !preg_match('/^\d+$/', $data['id']) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."は数字で入力してください。";
            } else {
                $product = $this->productRepository->getProduct(['id' => $data['id']])->first();
                if ( !$product ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."が存在しません。";
                }
            }
        } else {
            $product = $this->productRepository->getProduct(['name' => $data['name']])->first();
            if ( $product ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'id']."を入力してください（登録済みの商品名です）";
            }
        }
        // 公開ステータス(ID)
        if ( $data['status'] && array_key_exists($data['status'], config('status')) === false ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'status']."は公開なら1、非公開なら2で入力してください。";
        }
        // 商品名
        if ( $data['name'] && mb_strlen($data['name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'name']."は50文字以内入力してください。";
        }
        // 割引パターン
        if ( in_array($data['discount_pattern'], $discountPatterns) === false ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'discount_pattern']."が存在しません。";
        }
        // ショップ用メモ欄
        if ( $data['note'] && mb_strlen($data['note']) > 200 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'note']."は200文字以内入力してください。";
        }
        // 商品説明(一覧)
        if ( $data['description_list'] && mb_strlen($data['description_list']) > 200 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'description_list']."は200文字以内入力してください。";
        }
        // 商品説明(詳細)
        if ( $data['description_detail'] && mb_strlen($data['description_detail']) > 200 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'description_detail']."は200文字以内入力してください。";
        }
        // 検索ワード
        if ( $data['search_word'] && mb_strlen($data['search_word']) > 200 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'search_word']."は200文字以内入力してください。";
        }
        // 助数詞
        if ( $data['search_word'] && mb_strlen($data['search_word']) > 10 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'search_word']."は10文字以内入力してください。";
        }           
    }

    private function productClassValidate($headers, $data, $line, $setPatterns)
    {
        $prefix = "product_classes-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'products-name' && $key != 'product_classes-class_category_id1' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 規格分類1(ID)
        if ( $data['class_category_id1'] && !preg_match('/^\d+$/', $data['class_category_id1']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'class_category_id1']."は数字で入力してください。";
        }
        // デフォルト
        if ( $data['default_flg'] != 1 && $data['default_flg'] != 2 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'default_flg']."フラグはデフォルト規格は1、それ以外は2で入力してください。";
        }
        // セット数
        if ( $data['set'] ) {
            if ( empty($setPatterns) ) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'set']."が存在しません。";
            } else {
                $sets = preg_replace('/[　]/u', '', $data['set']);
                $sets = explode(',', $sets);
                foreach ($sets as $set) {
                    if ( in_array($set, $setPatterns) === false ) {
                        $this->errors[] = $line."行目の".$headers['name'][$prefix.'set']."が存在しません。";
                    }
                } unset ( $set );
            }
        }
        // 商品コード
        if ( $data['product_code'] && mb_strlen($data['product_code']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'product_code']."は50文字以内入力してください。";
        }
        // 在庫ID
        if ( $data['zaiko_id'] && mb_strlen($data['zaiko_id']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'zaiko_id']."は50文字以内入力してください。";
        }
        // 内容量
        if ( $data['dose'] && !preg_match('/^\d+$/', $data['dose']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'dose']."は数字で入力してください。";
        }
        // 取扱い
        if ( $data['handling_flg'] != 1 && $data['handling_flg'] != 2 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'handling_flg']."は有りなら1、無しは2で入力してください。";
        }
        // 規格ステータス
        if ( $data['status'] && array_key_exists($data['status'], config('status')) === false ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'status']."は公開なら1、非公開なら2で入力してください。";
        }
        // 通常価格
        if ( $data['price01'] && !preg_match('/^\d+$/', $data['price01']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'price01']."は数字で入力してください。";
        }
        // 販売価格
        if ( $data['price02'] && !preg_match('/^\d+$/', $data['price02']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'price02']."は数字で入力してください。";
        }
    }

    private function productCategoryValidate($headers, $data, $line)
    {
        $prefix = "product_category-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'products-name' && $key != 'product_classes-class_category_id1' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 商品カテゴリ(ID)
        if ( $data['category_id'] ) {
            $categoryIds = explode(',', $data['category_id']);
            if (empty($categoryIds)) {
                $this->errors[] = $line."行目の".$headers['name'][$prefix.'category_id']."を正しく入力してください。";
            } else {
                foreach ($categoryIds as $categoryId) {
                    $category = $this->categoryRepository->getCategory(['id' => $categoryId])->first();
                    if ( !$category ) {
                        $this->errors[] = $line."行目の".$headers['name'][$prefix.'category_id']."が存在しません。";
                    }
                }
            }
        }
    }

    private function productImageValidate($headers, $data, $line)
    {
        $prefix = "product_images-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'products-id' && $key != 'products-name' && $key != 'product_classes-class_category_id1' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 画像ファイル
        if ( $data['file_name'] && mb_strlen($data['file_name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'file_name']."は50文字以内で入力してください。";
        }
        // 画像テキスト
        if ( $data['file_text'] && mb_strlen($data['file_text']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'file_text']."は50文字以内で入力してください。";
        }
    }

    private function reviewValidate($headers, $data, $line)
    {
        $prefix = "product_reviews-";

        // 入力必須確認
        foreach ( $data as $key => $val ) {
            if ( $key != 'products-name' && $key != 'product_classes-class_category_id1' ) {
                if ( $headers['require'][$prefix.$key] === true && is_null($val) ) {
                    $this->errors[] = $line."行目の".$headers['name'][$prefix.$key]."を入力してください。";
                }
            }
        }
        // 公開ステータス(ID)
        if ( $data['status'] && array_key_exists($data['status'], config('status')) === false ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'status']."は公開なら1、非公開なら2で入力してください。";
        }
        // おすすめレベル
        if ( $data['recommend_level'] && !preg_match('/^\d+$/', $data['recommend_level']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'recommend_level']."は数字で入力してください。";
        }
        // タイトル
        if ( $data['title'] && mb_strlen($data['title']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'title']."は50文字以内入力してください。";
        }
        // 本文
        if ( $data['comment'] && mb_strlen($data['comment']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'comment']."は50文字以内入力してください。";
        }
        // 商品ID
        if ( $data['product_id'] && !preg_match('/^\d+$/', $data['product_id']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'product_id']."は数字で入力してください。";
        }
        // 会員ID
        if ( $data['user_id'] && !preg_match('/^\d+$/', $data['user_id']) ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'user_id']."は数字で入力してください。";
        }
        // 投稿者名
        if ( $data['reviewer_name'] && mb_strlen($data['reviewer_name']) > 50 ) {
            $this->errors[] = $line."行目の".$headers['name'][$prefix.'reviewer_name']."は50文字以内入力してください。";
        }
    }

    // バルクインサートは保留
    // private function insert($dbName, $insert, $errors)
    // {
    //     if ( empty($insert) || !empty($errors) ) {
    //         return;
    //     }

    //     switch ($dbName) {
    //         case "products":
    //             if ( count($insert) < 500 ) {
    //                 Product::insert($insert);
    //             } else {
    //                 $insertPart = array_chunk($insert, 500);
    //                 $insertPartCount = count($insertPart);
    //                 for ( $i = 0; $i <= $insertPartCount - 1; $i++ ){
    //                     Product::insert($insertPart[$i]);
    //                 }
    //             }

    //             break;
    //     }
    // }
}
