<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UsePointUserMaxRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // 使用ポイントが会員ポイントを超えた場合
        if ($value > $this->order->user->point) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ご使用ポイントが保有ポイントを超えています。';
    }
}
