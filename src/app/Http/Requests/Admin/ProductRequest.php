<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use App\Models\Product;

class ProductRequest extends FormRequest
{
    public function __construct(){
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // 商品ID
        //dd(Request::input('id'));
        return [
            'name'       => ['required', 'string', 'max:255'],
            'category'       => ['required', 'string', 'max:255'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '商品名を入力してください。',
        ];
    }
}
