<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Rules\Password;
use App\Models\User;

class OrderRequest extends FormRequest
{
    public function __construct(){
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // 注文ID
        //dd(Request::input('id'));

        return [
            // 'order_name01' => ['required', 'string', 'max:255'],
            // 'order_name02' => ['required', 'string', 'max:255'],
            // 'order_kana01' => ['required', 'string', 'max:255'],
            // 'order_kana02' => ['required', 'string', 'max:255'],
            // 'order_email'  => ['required', 'string', 'email', 'max:255'],
            // 'order_tel'    => ['string', 'max:255'],
            // 'order_zip01'  => ['required', 'string', 'max:255'],
            // 'order_zip02'  => ['required', 'string', 'max:255'],
            // 'order_addr01' => ['required', 'string', 'max:255'],
            // 'order_addr02' => ['max:255'],
        ];
    }

    public function messages()
    {
        return [
            // 'order_name01.required' => '姓を入力してください。',
            // 'order_name02.required' => '名を入力してください。',
            // 'order_kana01.required' => 'セイを入力してください。',
            // 'order_kana02.required' => 'メイを入力してください。',
            // 'order_email.required'  => 'メールアドレスを入力してください。',
            // 'order_zip01.required'  => '郵便番号1を入力してください。',
            // 'order_zip02.required'  => '郵便番号2を入力してください。',
            // 'order_addr01.required' => '住所を入力してください。',
        ];
    }
}
