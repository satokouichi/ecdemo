<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use App\Models\ProductClass;

class ProductClassRequest extends FormRequest
{
    public function __construct(){
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // 商品規格ID
        //dd(Request::input('id'));
        return [
            // 'id'                 => ['required', 'integer', 'max:10'],
            // 'class_category_id1' => ['required', 'integer', 'max:10'],
            // 'class_category_id2' => ['required', 'integer', 'max:10'],
            // 'product_code'       => ['required'],
            // 'zaiko_id'           => ['max:50'],
            // 'stock'              => ['max:5'],
            // 'stock_unlimited'    => ['integer', 'max:1'],
            // 'price01'            => ['integer', 'max:10'],
            // 'price02'            => ['integer', 'max:10'],
            // 'status'             => ['integer', 'max:1'],
            // 'dose'               => ['integer', 'max:10'],
            // 'handling_flg'       => ['integer', 'max:1'],
        ];
    }

    public function messages()
    {
        return [
            // 'product_code.required' => 'あああ',
        ];
    }
}
