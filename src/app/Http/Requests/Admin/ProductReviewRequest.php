<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use App\Models\ProductReview;

class ProductReviewRequest extends FormRequest
{
    public function __construct(){
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // レビューID
        //dd(Request::input('id'));
        return [
            'product_id' => ['required'],
            'create_date' => ['required'],
            'user_id'    => ['required'],
            'title'      => ['required'],
            'comment'    => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'product_id.required'  => '商品IDを入力してください。',
            'create_date.required' => '投稿日を入力してください。',
            'user_id.required'     => '会員IDを入力してください。',
            'title.required'       => 'タイトルを入力してください。',
            'comment.required'     => 'コメントを入力してください。',
        ];
    }
}
