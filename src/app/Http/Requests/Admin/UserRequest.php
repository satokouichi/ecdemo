<?php

namespace App\Http\Requests\Admin;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Rules\Password;
use App\Models\User;

class UserRequest extends FormRequest
{
    public function __construct(){
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // 会員ID
        //dd(Request::input('id'));
        return [
            'name01'       => ['required', 'string', 'max:255'],
            'name02'       => ['required', 'string', 'max:255'],
            'kana01'       => ['required', 'string', 'max:255'],
            'kana02'       => ['required', 'string', 'max:255'],
            'zip01'        => ['required', 'string', 'max:255'],
            'zip02'        => ['required', 'string', 'max:255'],
            'pref'         => ['required'],
            'addr01'       => ['required', 'string', 'max:255'],
            'addr02'       => ['max:255'],
            'email'        => ['required', 'string', 'email', 'max:255'],
            'password'     => ['required', 'string', new Password, 'confirmed'],
            'sex'          => ['required'],
            'mailmaga_flg' => ['required'],
            'point'        => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'name01.required' => '姓を入力してください。',
            'name02.required' => '名を入力してください。',
            'kana01.required' => 'セイを入力してください。',
            'kana02.required' => 'メイを入力してください。',
            'zip01.required'  => '郵便番号1を入力してください。',
            'zip02.required'  => '郵便番号2を入力してください。',
            'addr01.required' => '住所を入力してください。',
            'email.required'  => 'メールアドレスを入力してください。',
        ];
    }
}
