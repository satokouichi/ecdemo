<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;
use App\Rules\UsePointUserMaxRule;
use App\Rules\UsePointPaymentTotalMaxRule;
use App\Models\Order;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;

class UsePointRequest extends FormRequest
{
    private $cartRepository;
    private $orderRepository;

    public function __construct(
        CartRepository $cartRepository,
        OrderRepository $orderRepository
    ){
        $this->cartRepository = $cartRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cart = $this->cartRepository->getCart(Session::get('cart_key'));
        $order = $this->orderRepository->getPreOrder($cart->pre_order_id);

        return [
            'use_point' => [
                'numeric',
                new UsePointUserMaxRule($order),
                new UsePointPaymentTotalMaxRule($order)
            ],
        ];
    }

    public function messages()
    {
        return [
            'use_point.numeric' => 'ポイントは正しく入力してください。',
            'use_point.max' => 'ポイントは正しく入力してください。',
        ];
    }
}
