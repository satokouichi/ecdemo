<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;
use App\Services\CsvImportService;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CategoryController extends Controller
{
    private $categoryRepository;
    private $csvImportService;

    public function __construct(
        CategoryRepository $categoryRepository,
        CsvImportService $csvImportService
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->csvImportService = $csvImportService;
    }
    
    /**
     * カテゴリ一覧
     */
    public function index(Request $request)
    {
        $categories = $this->categoryRepository->getCategories()->get();
        
        return view('admin.category/index', [
            'categories' => $categories
        ]);
    }

    /**
     * カテゴリ登録
     */
    public function store(Request $request)
    {
        //dd("カテゴリ登録");
        
        return;
    }

    /**
     * カテゴリ編集
     */
    public function edit(Request $request)
    {
        //dd("カテゴリ編集");
        
        return view('front.category/edit', [
            'products' => []
        ]);
    }

    /**
     * カテゴリ削除
     */
    public function delete(Request $request)
    {
        //dd("カテゴリ削除");
        
        return;
    }

    /**
     * カテゴリCSVエクスポート
     */
    public function export(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $Category = new Category;

            $qb = $this->categoryRepository->getCategories();
            $categories = $qb->get();

            // ヘッダー行を追加
            $headers = [];
            foreach ($Category->csvHeader() as $key => $header) {
                $headers[] = $key;
            } unset($key, $header);
            fputcsv($stream, $headers);
            
            if (empty($categories)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($categories as $row) {
                    fputcsv($stream, $Category->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=category.csv');

        return $response;
    }

    /**
     * カテゴリCSVテンプレート
     */
    public function template(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $Category = new Category;

            // ヘッダー行を追加
            $headers = [];
            foreach ($Category->csvHeader() as $key => $val) {
                $headers[] = $key;
            } unset($key, $val);
            fputcsv($stream, $headers);

            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=category.csv');

        return $response;
    }

    /**
     * カテゴリCSV登録画面
     */
    public function csv(Request $request)
    {
        $Category = new Category;

        return view('admin.category/csv', [
            'headers' => $Category->csvHeader(),
        ]);
    }

    /**
     * カテゴリCSVインポート
     */
    public function import(Request $request)
    {
        $Category = new Category;
        $errors = $this->csvImportService->csvImport($request, $Category->csvHeader(), 'category');
        if (!$errors) {
            \Session::flash('flash_message', config('params')['flash_message']);
        } else {
            \Session::flash('error_message', config('params')['error_message']);
        }
        
        return redirect()
                ->route('admin.category.csv')
                ->with([
                    'errors' => $errors,
                ]);
    }
}