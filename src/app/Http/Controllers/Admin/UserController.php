<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDetail;
use App\Repositories\User\UserRepositoryInterface AS UserRepository;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;
use App\Http\Requests\Admin\UserRequest;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\StreamedResponse;

class UserController extends Controller
{
    private $userRepository;
    private $orderRepository;
    private $categoryRepository;

    public function __construct(
        UserRepository $userRepository,
        OrderRepository $orderRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
        $this->categoryRepository = $categoryRepository;
    }
    
    /**
     * 会員一覧
     */
    public function index(Request $request)
    {
        $query = $this->userRepository->searchUserAdminQuery($request->all());
        $users = $query->paginate(2);

        $categories = $this->categoryRepository->getCategories()->get();
        $categorySelect = [];
        foreach ($categories as $category) {
            $categorySelect[$category->id] = $category->category_name;
        } unset($category);

        return view('admin.user/index', [
            'users' => $users,
            'categorySelect' => $categorySelect,
        ]);
    }

    /**
     * 会員作成
     */
    public function create(Request $request)
    {
        return view('admin.user/edit', [
            'user' => "",
        ]);
    }

    /**
     * 会員編集
     */
    public function edit(Request $request, $userId)
    {
        $user = $this->userRepository->getUser($userId);
        $orders = $this->orderRepository->getUserOrders($userId)->get();

        $year = \Carbon\Carbon::parse($user->birth)->format('Y');
        $month = \Carbon\Carbon::parse($user->birth)->format('m');
        $day = \Carbon\Carbon::parse($user->birth)->format('d');

        return view('admin.user/edit', [
            'user' => $user,
            'orders' => $orders,
            'year' => $year,
            'month' => $month,
            'day' => $day,
        ]);
    }

    /**
     * 会員登録
     */
    public function store(UserRequest $request)
    {
        if ($request->input('id')) {
            if ( $this->userRepository->updateUserByAdmin($request) ) {
                \Session::flash('flash_message', config('params')['flash_message']);
            } else {
                \Session::flash('error_message', config('params')['error_message']);
            }
            return redirect(route('admin.user.edit', $request->input('id')));
        } else {
            $user = $this->userRepository->createUserByAdmin($request);
            if ( $user ) {
                \Session::flash('flash_message', config('params')['flash_message']);
                return redirect(route('admin.user.edit', $user->id));
            } else {
                \Session::flash('error_message', config('params')['error_message']);
                return redirect(route('admin.user.create'));
            }
        }
    }

    /**
     * 会員更新
     */
    public function update(Request $request)
    {
        dd("会員更新");
        
        return;
    }

    /**
     * 会員削除
     */
    public function delete(Request $request)
    {
        dd("会員削除");
        
        return;
    }

    /**
     * 会員CSV出力
     */
    public function export(Request $request)
    {
        $params = $request->all();
        $response = new StreamedResponse(function () use ($request, $params) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $User = new User;

            $qb = $this->userRepository->searchUserAdminQuery($params);
            $users = $qb->get();

            // ヘッダー行を追加
            fputcsv($stream, $User->csvHeader());
            
            if (empty($users)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($users as $row) {
                    fputcsv($stream, $User->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=user.csv');

        return $response;
    }
}