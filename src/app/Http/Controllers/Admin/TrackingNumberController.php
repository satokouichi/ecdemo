<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;

class TrackingNumberController extends Controller
{
    private $orderRepository;

    public function __construct(
        OrderRepository $orderRepository
    )
    {
        $this->orderRepository = $orderRepository;
    }
    
    /**
     * 追跡番号一覧
     */
    public function index(Request $request, $orderId)
    {
        $order = $this->orderRepository->getOrder($orderId);

        return view('admin.tracking/index', [
            'order' => $order,
        ]);
    }
}