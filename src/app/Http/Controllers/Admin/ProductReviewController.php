<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductReview;
use App\Repositories\ProductReview\ProductReviewRepositoryInterface AS ProductReviewRepository;
use App\Http\Requests\Admin\ProductReviewRequest;
use App\Services\ImageService;
use App\Services\CsvImportService;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductReviewController extends Controller
{
    private $productReviewRepository;
    private $imageService;
    private $csvImportService;

    public function __construct(
        ProductReviewRepository $productReviewRepository,
        ImageService $imageService,
        CsvImportService $csvImportService
    )
    {
        $this->productReviewRepository = $productReviewRepository;
        $this->imageService = $imageService;
        $this->csvImportService = $csvImportService;
    }
    
    /**
     * レビュー一覧
     */
    public function index(Request $request)
    {
        $query = $this->productReviewRepository->searchReviewAdminQuery($request->all());
        $reviews = $query->paginate(5);
        
        return view('admin.review/index', [
            'reviews' => $reviews,
        ]);
    }

    /**
     * レビュー作成
     */
    public function create(Request $request)
    {
        return view('admin.review/edit', [
            'review' => "",
        ]);
    }

    /**
     * レビュー編集
     */
    public function edit(Request $request, $reviewId)
    {
        $review = $this->productReviewRepository->getProductReview($reviewId);

        return view('admin.review/edit', [
            'review' => $review,
        ]);
    }

    /**
     * 商品登録
     */
    public function store(ProductReviewRequest $request)
    {       
        if ($request->input('id')) {
            if ( $this->productReviewRepository->updateProductReviewByAdmin($request) ) {
                \Session::flash('flash_message', config('params')['flash_message']);
                // 画像のアップロード
                $this->imageService->saveImage($request, 'review', 300);
            } else {
                \Session::flash('error_message', config('params')['error_message']);
            }
            
            return redirect(route('admin.review.edit', $request->input('id')));
        } else {
            $review = $this->productReviewRepository->createProductReviewByCommon($request);
            if ( $review ) {
                \Session::flash('flash_message', config('params')['flash_message']);
                // 画像のアップロード
                $this->imageService->saveImage($request, 'review', 300);

                return redirect(route('admin.review.edit', $review->id));
            } else {
                \Session::flash('error_message', config('params')['error_message']);

                return redirect(route('admin.review.create'));
            }            
        }
    }

    /**
     * レビューCSVエクスポート
     */
    public function export(Request $request)
    {
        $params = $request->all();
        $response = new StreamedResponse(function () use ($request, $params) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ProductReview = new ProductReview;

            $qb = $this->productReviewRepository->searchReviewAdminQuery($params);
            $reviews = $qb->get();

            // ヘッダー行を追加
            $headers = [];
            foreach ($ProductReview->csvHeader() as $key => $header) {
                $headers[] = $key;
            } unset($key, $header);
            fputcsv($stream, $headers);
            
            if (empty($reviews)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($reviews as $row) {
                    fputcsv($stream, $ProductReview->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=review.csv');

        return $response;
    }

    /**
     * レビューCSVテンプレート
     */
    public function template(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ProductReview = new ProductReview;

            // ヘッダー行を追加
            $headers = [];
            foreach ($ProductReview->csvHeader() as $key => $val) {
                $headers[] = $key;
            } unset($key, $val);
            fputcsv($stream, $headers);

            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=review.csv');

        return $response;
    }

    /**
     * レビューCSV登録画面
     */
    public function csv(Request $request)
    {
        $ProductReview = new ProductReview;

        return view('admin.review/csv', [
            'headers' => $ProductReview->csvHeader(),
        ]);
    }

    /**
     * レビューCSインポート
     */
    public function import(Request $request)
    {
        $ProductReview = new ProductReview;
        $errors = $this->csvImportService->csvImport($request, $ProductReview->csvHeader(), 'review');
        if (!$errors) {
            \Session::flash('flash_message', config('params')['flash_message']);
        } else {
            \Session::flash('error_message', config('params')['error_message']);
        }
        
        return redirect()
                ->route('admin.review.csv')
                ->with([
                    'errors' => $errors,
                ]);
    }
}