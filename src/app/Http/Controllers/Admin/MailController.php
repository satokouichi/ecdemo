<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;
use App\Http\Controllers\Controller;
use App\Mail\OrderMail;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\MailHistory\MailHistoryRepositoryInterface AS MailHistoryRepository;
use App\Repositories\MailTemplate\MailTemplateRepositoryInterface AS MailTemplateRepository;

class MailController extends Controller
{
    private $orderRepository;
    private $mailHistoryRepository;
    private $mailTemplateRepository;

    public function __construct(
        OrderRepository $orderRepository,
        MailHistoryRepository $mailHistoryRepository,
        MailTemplateRepository $mailTemplateRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->mailHistoryRepository = $mailHistoryRepository;
        $this->mailTemplateRepository = $mailTemplateRepository;
    }
    
    /**
     * 入力画面
     */
    public function index(Request $request, $orderId, $templateId)
    {
        // メール配信履歴
        $mailHistories = $this->mailHistoryRepository->getOrderMailHistories($orderId)
            ->limit(5)
            ->get();

        // テンプレート
        if ($templateId) {
            $mailTemplate = $this->mailTemplateRepository->getMailTemplate($templateId)->toArray();
        } else {
            $mailTemplate = $this->mailTemplateRepository->getMailTemplates()->toArray();
        }

        return view('admin.mail/index', [
            'orderId' => $orderId,
            'templateId' => $templateId,
            'mailHistories' => $mailHistories,
            'mailTemplate' => $mailTemplate,
        ]);
    }

    /**
     * 確認画面
     */
    public function confirm(Request $request, $orderId, $templateId)
    {
        return view('admin.mail/confirm', [
            'orderId' => $orderId,
            'templateId' => $templateId,
            'inputs' => $request->all(),
        ]);
    }

    /**
     * 入力画面へ戻る
     */
    public function back(Request $request, $orderId, $templateId)
    {
        return redirect()
                ->route('admin.mail.index', [
                        'orderId' => $orderId,
                        'templateId' => $templateId
                    ]);
    }

    /**
     * 送信する
     */
    public function send(Request $request, $orderId, $templateId, Mailer $mailer)
    {
        $mailer->to('test@example.com')
            ->send(new OrderMail());

        return redirect()
                ->route('admin.order.index');
    }
        
}