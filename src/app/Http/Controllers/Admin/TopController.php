<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TopController extends Controller
{
    public function __construct(
        //
    )
    {
        //
    }
    
    /**
     * トップページ
     */
    public function index()
    {
        return redirect(route('admin.order.index'));
    }
}