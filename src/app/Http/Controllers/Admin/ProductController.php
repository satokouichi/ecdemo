<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Http\Requests\Admin\ProductRequest;
use App\Services\ProductService;
use App\Services\CsvImportService;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductController extends Controller
{
    private $productRepository;
    private $productService;
    private $csvImportService;

    public function __construct(
        ProductRepository $productRepository,
        ProductService $productService,
        CsvImportService $csvImportService
    )
    {
        $this->productRepository = $productRepository;
        $this->productService = $productService;
        $this->csvImportService = $csvImportService;
    }
    
    /**
     * 商品一覧
     */
    public function index(Request $request)
    {
        $query = $this->productRepository->searchProductAdminQuery($request->all());
        $products = $query->paginate(5);

        return view('admin.product/index', [
            'products' => $products,
        ]);
    }

    /**
     * 商品作成
     */
    public function create(Request $request)
    {
        // return view('admin.product/create', []);
    }

    /**
     * 商品編集
     */
    public function edit(Request $request, $productId)
    {
        $product = $this->productRepository->getProduct(['id' => $productId])
            ->first();

        return view('admin.product/edit', [
            'product' => $product,
        ]);
    }

    /**
     * 商品登録
     */
    public function store(ProductRequest $request)
    {
        if ($request->input('id')) {
            if ( $this->productRepository->updateProductByAdmin($request) ) {
                \Session::flash('flash_message', config('params')['flash_message']);
            } else {
                \Session::flash('error_message', config('params')['error_message']);
            }

            return redirect(route('admin.product.edit', $request->input('id')));
        } else {
            return redirect(route('admin.product.index'));
        }
    }

    /**
     * 商品CSVダウンロード
     */
    public function export(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $Product = new Product;

            $qb = $this->productRepository->searchProductAdminQuery($request->all());
            $products = $qb->get();

            // ヘッダー行を追加
            $headers = [];
            foreach ($Product->csvHeader() as $key => $header) {
                $headers[] = $key;
            } unset($key, $header);
            fputcsv($stream, $headers);
            
            if (empty($products)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                $products = $this->productService->getDownloadCsvParams($products);
                foreach ($products as $product) {
                    foreach ($product as $row) {
                        fputcsv($stream, $Product->csvRow($row));
                    } unset($row);
                } unset($product);
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=product.csv');

        return $response;
    }

    /**
     * 商品CSVテンプレート
     */
    public function template(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $Product = new Product;

            // ヘッダー行を追加
            $headers = [];
            foreach ($Product->csvHeader() as $key => $val) {
                $headers[] = $key;
            } unset($key, $val);
            fputcsv($stream, $headers);

            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=product.csv');

        return $response;
    }

    /**
     * 商品CSV登録画面
     */
    public function csv(Request $request)
    {
        $Product = new Product;

        return view('admin.product/csv', [
            'headers' => $Product->csvHeader(),
        ]);
    }

    /**
     * 商品CSVインポート
     */
    public function import(Request $request)
    {
        $Product = new Product;
        $errors = $this->csvImportService->csvImport($request, $Product->csvHeader(), 'product');
        if (!$errors) {
            \Session::flash('flash_message', config('params')['flash_message']);
        } else {
            \Session::flash('error_message', config('params')['error_message']);
        }
        
        return redirect()
                ->route('admin.product.csv')
                ->with([
                    'errors' => $errors,
                ]);
    }
}