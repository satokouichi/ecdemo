<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClassName;
use App\Models\ClassCategory;
use App\Repositories\ClassCategory\ClassCategoryRepositoryInterface AS ClassCategoryRepository;
use App\Services\CsvImportService;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ClassCategoryController extends Controller
{
    private $classCategoryRepository;
    private $csvImportService;
    
    public function __construct(
        ClassCategoryRepository $classCategoryRepository,
        CsvImportService $csvImportService
    )
    {
        $this->classCategoryRepository = $classCategoryRepository;
        $this->csvImportService = $csvImportService;
    }
    
    /**
     * 規格一覧
     */
    public function index(Request $request)
    {        
        return view('admin.classcategory/index', []);
    }

    /**
     * 規格登録
     */
    public function store(Request $request)
    {
        //dd("規格登録");
        
        return;
    }

    /**
     * 規格編集
     */
    public function edit(Request $request)
    {
        //dd("規格編集");
        
        return view('admin.classcategory/edit', [
            'products' => []
        ]);
    }

    /**
     * 規格削除
     */
    public function delete(Request $request)
    {
        //dd("規格削除");
        
        return;
    }

    /**
     * 規格分類CSVエクスポート
     */
    public function export(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ClassCategory = new ClassCategory;

            $qb = $this->classCategoryRepository->getClassCategories();
            $classCategories = $qb->get();

            // ヘッダー行を追加
            $headers = [];
            foreach ($ClassCategory->csvHeader() as $key => $header) {
                $headers[] = $key;
            } unset($key, $header);
            fputcsv($stream, $headers);
            
            if (empty($classCategories)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($classCategories as $row) {
                    fputcsv($stream, $ClassCategory->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=classcategory.csv');

        return $response;
    }

    /**
     * 規格分類CSVテンプレート
     */
    public function template(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ClassCategory = new ClassCategory;

            // ヘッダー行を追加
            $headers = [];
            foreach ($ClassCategory->csvHeader() as $key => $val) {
                $headers[] = $key;
            } unset($key, $val);
            fputcsv($stream, $headers);

            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=classcategory.csv');

        return $response;
    }

    /**
     * 規格分類CSV画面
     */
    public function csv(Request $request)
    {
        $ClassName = new ClassName;
        $ClassCategory = new ClassCategory;

        return view('admin.classcategory/csv', [
            'classNameHeaders' => $ClassName->csvHeader(),
            'classCategoryHeaders' => $ClassCategory->csvHeader(),
        ]);
    }

    /**
     * 規格分類CSVインポート
     */
    public function import(Request $request)
    {
        $ClassCategory = new ClassCategory;
        $errors = $this->csvImportService->csvImport($request, $ClassCategory->csvHeader(), 'classcategory');
        if (!$errors) {
            \Session::flash('flash_message', config('params')['flash_message']);
        } else {
            \Session::flash('error_message', config('params')['error_message']);
        }
        
        return redirect()
                ->route('admin.classcategory.csv')
                ->with([
                    'errors' => $errors,
                ]);
    }
}