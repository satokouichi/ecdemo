<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\OrderDetail\OrderDetailRepositoryInterface AS OrderDetailRepository;
use App\Repositories\OrderStatus\OrderStatusRepositoryInterface AS OrderStatusRepository;
use App\Repositories\Payment\PaymentRepositoryInterface AS PaymentRepository;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;
use App\Http\Requests\Admin\OrderRequest;
use Session;
use Symfony\Component\HttpFoundation\StreamedResponse;

class OrderController extends Controller
{
    private $orderRepository;
    private $orderDetailRepository;
    private $orderStatusRepository;
    private $paymentRepository;
    private $categoryRepository;

    public function __construct(
        OrderRepository $orderRepository,
        OrderDetailRepository $orderDetailRepository,
        OrderStatusRepository $orderStatusRepository,
        PaymentRepository $paymentRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->paymentRepository = $paymentRepository;
        $this->categoryRepository = $categoryRepository;
    }
    
    /**
     * 注文一覧
     */
    public function index(Request $request)
    {
        $query = $this->orderRepository->searchOrderAdminQuery($request->all());
        $orders = $query->paginate(2);

        return view('admin.order/index', [
            'orders' => $orders,
        ]);
    }

    /**
     * 注文作成
     */
    public function create(Request $request)
    {
        $orderStatuses = $this->orderStatusRepository->getAll();
        $payments = $this->paymentRepository->getAll();
        $categories = $this->categoryRepository->getCategories()->get();
        $categorySelect = [];
        foreach ($categories as $category) {
            $categorySelect[$category->id] = $category->category_name;
        } unset($category);

        return view('admin.order/edit', [
            'order' => "",
            'orderStatuses' => $orderStatuses,
            'payments' => $payments,
            'categorySelect' => $categorySelect,
        ]);
    }

    /**
     * 注文編集
     */
    public function edit(Request $request, $orderId)
    {
        $order = $this->orderRepository->getOrder($orderId);
        $orderStatuses = $this->orderStatusRepository->getAll();
        $payments = $this->paymentRepository->getAll();
        $categories = $this->categoryRepository->getCategories()->get();
        $categorySelect = [];
        foreach ($categories as $category) {
            $categorySelect[$category->id] = $category->category_name;
        } unset($category);

        return view('admin.order/edit', [
            'order' => $order,
            'orderStatuses' => $orderStatuses,
            'payments' => $payments,
            'categorySelect' => $categorySelect,
        ]);
    }

    /**
     * 注文登録
     */
    public function store(OrderRequest $request)
    {
        if ($request->input('id')) {
            $this->orderRepository->updateOrderByAdmin($request);
            \Session::flash('flash_message', config('params')['flash_message']);
        } else {
            $order = $this->orderRepository->createOrderByAdmin($request);
            if ($order) {
                \Session::flash('flash_message', config('params')['flash_message']);
            } else {
                \Session::flash('error_message', config('params')['error_message']);
            }

            return redirect(route('admin.order.edit', $request->input('id')));
        }

        return redirect(route('admin.order.edit', $request->input('id')));
    }

    /**
     * 注文削除
     */
    public function delete(Request $request)
    {
        //dd("注文削除");
        
        return;
    }

    /**
     * 注文CSV出力
     */
    public function export(Request $request)
    {
        $params = $request->all();
        $response = new StreamedResponse(function () use ($request, $params) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $Order = new Order;

            $qb = $this->orderRepository->searchOrderAdminQuery($params);
            $orders = $qb->get();

            // ヘッダー行を追加
            fputcsv($stream, $Order->csvHeader());
            
            if (empty($orders)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($orders as $row) {
                    fputcsv($stream, $Order->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=order.csv');

        return $response;
    }

}