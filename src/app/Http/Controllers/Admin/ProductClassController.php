<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Discount\DiscountRepositoryInterface AS DiscountRepository;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ProductClass\ProductClassRepositoryInterface AS ProductClassRepository;
use App\Http\Requests\Admin\ProductClassRequest;

class ProductClassController extends Controller
{
    private $discountRepository;
    private $productRepository;
    private $productClassRepository;

    public function __construct(
        DiscountRepository $discountRepository,
        ProductRepository $productRepository,
        ProductClassRepository $productClassRepository
    )
    {
        $this->discountRepository = $discountRepository;
        $this->productRepository = $productRepository;
        $this->productClassRepository = $productClassRepository;
    }
    
    /**
     * 規格一覧
     */
    public function index(Request $request, $productId)
    {
        $product = $this->productRepository->getProduct(['id' => $productId])
            ->first();
        $productClasses = $this->productClassRepository->getProductClasses($productId);
        // 割引パターンを取得
        $discountPatterns = [];
        $discounts = $this->discountRepository->getDiscounts()->get();
        if ( !empty($discounts) ) {
            foreach ($discounts as $discount) {
                if (in_array($discount->pattern, $discountPatterns) === false) {
                    $discountPatterns[] = $discount->pattern;
                }
            } unset ($discount);
        }

        return view('admin.productclass/index', [
            'product' => $product,
            'productClasses' => $productClasses,
            'discountPatterns' => $discountPatterns,
        ]);
    }

    /**
     * 規格登録
     */
    public function store(ProductClassRequest $request, $productId)
    {
        if ( !empty($request->all()) ) {
            if ( $this->productClassRepository->updateProductClassByAdmin($request->all())) {
                \Session::flash('flash_message', config('params')['flash_message']);
            } else {
                \Session::flash('error_message', config('params')['error_message']);
            }
        }

        return redirect(route('admin.productclass.index', $productId));
    }
}