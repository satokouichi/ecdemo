<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ClassName;
use App\Repositories\ClassName\ClassNameRepositoryInterface AS ClassNameRepository;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ClassNameController extends Controller
{
    private $classNameRepository;
    
    public function __construct(
        ClassNameRepository $classNameRepository,
    )
    {
        $this->classNameRepository = $classNameRepository;
    }

    /**
     * 規格名CSVエクスポート
     */
    public function export(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ClassName = new ClassName;

            $qb = $this->classNameRepository->getClassNames();
            $classNames = $qb->get();

            // ヘッダー行を追加
            $headers = [];
            foreach ($ClassName->csvHeader() as $key => $header) {
                $headers[] = $key;
            } unset($key, $header);
            fputcsv($stream, $headers);
            
            if (empty($classNames)) {
                fputcsv($stream, [
                    'データが存在しませんでした。',
                ]);
            } else {
                foreach ($classNames as $row) {
                    fputcsv($stream, $ClassName->csvRow($row));
                }
            }
            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=class_name.csv');

        return $response;
    }

    /**
     * 規格名CSVテンプレート
     */
    public function template(Request $request)
    {
        $response = new StreamedResponse(function () use ($request) {
            $stream = fopen('php://output','w');

            // 文字化け回避
            stream_filter_prepend($stream, 'convert.iconv.utf-8/cp932//TRANSLIT');

            $ClassName = new ClassName;

            // ヘッダー行を追加
            $headers = [];
            foreach ($ClassName->csvHeader() as $key => $val) {
                $headers[] = $key;
            } unset($key, $val);
            fputcsv($stream, $headers);

            fclose($stream);
        });
        $response->headers->set('Content-Type', 'application/octet-stream'); 
        $response->headers->set('content-disposition', 'attachment; filename=class_name.csv');

        return $response;
    }
    
}