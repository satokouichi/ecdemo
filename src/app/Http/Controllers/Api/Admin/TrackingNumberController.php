<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TrackingNumber\TrackingNumberRepositoryInterface AS TrackingNumberRepository;

class TrackingNumberController extends Controller
{
    private $trackingNumberRepository;

    public function __construct(
        TrackingNumberRepository $trackingNumberRepository
    )
    {
        $this->trackingNumberRepository = $trackingNumberRepository;
    }

    /**
     * 登録
     */
    public function create(Request $request)
    {
        $this->trackingNumberRepository->createTrackingNumber($request->only(['order_id', 'number']));
        
        return response()->json(['success'=>"OK"]);
    }

    /**
     * 削除
     */
    public function delete(Request $request)
    {
        $this->trackingNumberRepository->deleteTrackingNumber($request->input('id'));
        
        return response()->json(['success'=>"OK"]);
    }
}