<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\User\UserRepositoryInterface AS UserRepository;

class OrderController extends Controller
{
    private $orderRepository;
    private $productRepository;
    private $userRepository;

    public function __construct(
        OrderRepository $orderRepository,
        ProductRepository $productRepository,
        UserRepository $userRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
    }
    
    /**
     * 備考欄登録
     */
    public function note(Request $request)
    {
        $params = [];
        $params['note'] = $request->input('note');
        $this->orderRepository->updateOrder($params, $request->input('order_id'));

        return response()->json(['success'=>"OK"]);
    }

    /**
     * 会員検索
     */
    public function user(Request $request)
    {
        $params = [];
        $params['user'] = $request->input('user');
        $user = $this->userRepository->getUser($params);

        return response()->json([
            'result' => true,
            'user' => $user,
        ], 200);
    }

    /**
     * 商品検索
     */
    public function products(Request $request)
    {
        $params = [];
        $params['category'] = $request->input('category');
        $params['product'] = $request->input('product');
        $products = $this->productRepository->searchProductAdminOrder($params);

        return response()->json([
            'result' => true,
            'products' => $products,
        ], 200);
    }
}