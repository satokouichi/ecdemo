<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\OrderPayment\OrderPaymentRepositoryInterface AS OrderPaymentRepository;

class OrderPaymentController extends Controller
{
    private $orderPaymentRepository;

    public function __construct(
        OrderPaymentRepository $orderPaymentRepository
    )
    {
        $this->orderPaymentRepository = $orderPaymentRepository;
    }

    /**
     * 登録
     */
    public function create(Request $request)
    {
        $this->orderPaymentRepository->createOrderPayment($request->only(['order_id', 'pay']));
        
        return response()->json(['success'=>"OK"]);
    }

    /**
     * 削除
     */
    public function delete(Request $request)
    {
        $this->orderPaymentRepository->deleteOrderPayment($request->input('id'));
        
        return response()->json(['success'=>"OK"]);
    }
}