<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\Category\CategoryRepositoryInterface AS CategoryRepository;

class ProductController extends Controller
{
    private $searchData = [];

    private $productRepository;

    private $categoryRepository;

    public function __construct
    (
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository
    )
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }
    
    /**
     * 商品一覧
     */
    public function index(Request $request, $categoryId)
    {
        $category = $this->categoryRepository->getCategory($categoryId)->first();
        if (!$category) {
            abort(404);
        }

        // カテゴリID検索結果の商品情報
        $params['category_ids'] = [$categoryId];
        $qb = $this->productRepository->searchProductFrontQuery($params);
        $listProducts = $qb->paginate(5);
        
        return view('front.product/index', [
            'category' => $category->toArray(),
            'listProducts' => $listProducts
        ]);
    }

    /**
     * 商品検索
     */
    public function search(Request $request)
    {
        // キーワード検索結果の商品情報
        $params['product_name'] = $request->input('product_name');
        $qb = $this->productRepository->searchProductFrontQuery($params);
        $listProducts = $qb->paginate(20);
        
        return view('front.product/index', [
            'category' => [],
            'listProducts' => $listProducts
        ]);
    }

    /**
     * 商品詳細
     */
    public function show(Request $request, $productId)
    {
        // 商品情報
        $params['product_ids'] = [$productId];
        $qb = $this->productRepository->searchProductFrontQuery($params);
        $detailProduct = $this->productRepository->arrayProducts($qb->get());
        if (!empty($detailProduct)) {
            $detailProduct = $detailProduct[$productId];
        }
        
        return view('front.product/show', [
            'productId' => $productId,
            'detailProduct' => $detailProduct,
        ]);
    }
}