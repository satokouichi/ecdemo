<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ProductReview\ProductReviewRepositoryInterface AS ProductReviewRepository;
use App\Services\ImageService;
use Session;

class ProductReviewController extends Controller
{
    private $productRepository;
    private $productReviewRepository;
    private $imageService;
        
    public function __construct (
        ProductRepository $productRepository,
        ProductReviewRepository $productReviewRepository,
        ImageService $imageService,
    )
    {
        $this->productRepository = $productRepository;
        $this->productReviewRepository = $productReviewRepository;
        $this->imageService = $imageService;
    }
    
    /**
     * 商品口コミ一覧
     */
    public function index(Request $request)
    {
        return view('front.review/index', [
            'products' => []
        ]);
    }

    /**
     * 商品口コミ詳細
     */
    public function show(Request $request, $productId, $reviewId)
    {
        return view('front.review/show', [
            'products' => []
        ]);
    }

    /**
     * 口コミ入力
     */
    public function create(Request $request, $productId)
    {
        return view('front.review/create', [
            'productId' => $productId
        ]);
    }

    /**
     * 口コミ確認
     */
    public function confirm(Request $request, $productId)
    {
        $fileName = $this->imageService->saveTempImage($request, 300);
        
        return view('front.review/confirm', [
            'fileName' => $fileName,
            'reviewerUrl' => pathinfo($fileName)['filename'],
            'productId' => $productId,
            'inputs' => $request->all(),
        ]);
    }

    /**
     * 入力へ戻る
     */
    public function back(Request $request, $productId)
    {
        return redirect()
                ->route('review.create', [
                        'productId' => $productId,
                    ])->withInput();
    }

    /**
     * 口コミ投稿完了
     */
    public function complete(Request $request)
    {
        $params = $request->except(['_token', '_method']);

        $this->productReviewRepository->createProductReview($params);

        return view('front.review/complete');
    }
}