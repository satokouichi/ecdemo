<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\OrderDetail\OrderDetailRepositoryInterface AS OrderDetailRepository;
use Illuminate\Support\Facades\Auth;
use Session;

class MypageController extends Controller
{
    private $searchData = [];
    
    private $orderRepository;

    private $orderDetailRepository;
    
    public function __construct(
        OrderRepository $orderRepository,
        OrderDetailRepository $orderDetailRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->orderDetailRepository = $orderDetailRepository;
    }
    
    /**
     * 購入履歴一覧
     */
    public function index(Request $request)
    {
        $orders = $this->orderRepository->getUserOrders(Auth::id());
        $orders = $orders->orderBy('id', 'DESC')->paginate(3);

        Session::put('userid', Auth::id());

        return view('front.mypage/index', [
            'orders' => $orders
        ]);
    }

    /**
     * 購入履歴詳細
     */
    public function show(Request $request, $orderId)
    {
        $order = $this->orderRepository->getOrder($orderId);
        
        return view('front.mypage/show', [
            'order' => $order
        ]);
    }

    /**
     * 2要素認証
     */
    public function twofactor()
    {
        return view('front.mypage/twofactor');
    }

    /**
     * 2要素認証
     */
    public function registerComplete()
    {
        return view('front.mypage/register_complete');
    }
}