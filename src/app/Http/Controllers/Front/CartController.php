<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Services\CartService;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;

class CartController extends Controller
{
    private $cartService;
    private $cartRepository;
    private $cartDetailRepository;

    public function __construct
    (
        CartService $cartService,
        CartRepository $cartRepository,
        CartDetailRepository $cartDetailRepository
    )
    {
        $this->middleware('cart');
        $this->cartService = $cartService;
        $this->cartRepository = $cartRepository;
        $this->cartDetailRepository = $cartDetailRepository;
    }
    
    /**
     * カート一覧
     */
    public function index(Request $request)
    {
        $cart = $this->cartRepository->getCart(Session::get('cart_key'));
        $cart = $this->cartRepository->arrayCarts($cart);

        return view('front.cart/index', [
            'cart' => $cart
        ]);
    }

    /**
     * カート登録
     */
    public function store(Request $request)
    {
        $this->cartService->setCart(Session::get('cart_key'), $request);

        return redirect(route('cart.index'));
    }

    /**
     * カート更新
     */
    public function update(Request $request)
    {
        $this->cartService->updateCart(Session::get('cart_key'), $request);
        
        return redirect(route('cart.index'));
    }

    /**
     * カート削除
     */
    public function delete(Request $request)
    {
        $this->cartRepository->deleteCartByCart(Session::get('cart_key'), $request->input('product_class_id'));

        return redirect(route('cart.index'));
    }
}