<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Services\CartService;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\OrderDetail\OrderDetailRepositoryInterface AS OrderDetailRepository;
use App\Repositories\Payment\PaymentRepositoryInterface AS PaymentRepository;
use App\Http\Requests\Front\UsePointRequest;
use Carbon\Carbon;

class ShoppingController extends Controller
{
    private $cartService;
    private $cartRepository;
    private $cartDetailRepository;
    private $orderRepository;
    private $orderDetailRepository;
    private $paymentRepository;

    public function __construct(
        CartService $cartService,
        CartRepository $cartRepository,
        CartDetailRepository $cartDetailRepository,
        OrderRepository $orderRepository,
        OrderDetailRepository $orderDetailRepository,
        PaymentRepository $paymentRepository
    )
    {
        $this->middleware('shopping')->only('index');
        $this->cartService = $cartService;
        $this->cartRepository = $cartRepository;
        $this->cartDetailRepository = $cartDetailRepository;
        $this->orderRepository = $orderRepository;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->paymentRepository = $paymentRepository;
    }
    
    /**
     * 購入一覧
     */
    public function index(Request $request)
    {
        $cart = $this->cartRepository->getCart(Session::get('cart_key'));
        $order = $this->orderRepository->getPreOrder($cart->pre_order_id);
        $order = $this->orderRepository->arrayOrders($order);
        $payments = $this->paymentRepository->getAll()->toArray();

        return view('front.shopping/index', [
            'userPoint' => intval(Auth::user()->point),
            'order' => $order,
            'orderDetails' => $order['orderDetails'],
            'payments' => $payments
        ]);
    }

    /**
     * 購入登録
     */
    public function store(UsePointRequest $request)
    {
        $mode = $request->input('mode');

        if ($mode != 'complete') {
            $request->flash();

            return $this->index();
        }

        $cart = $this->cartRepository->getCart(Session::get('cart_key'));

        // 注文を取得
        $order = Order::where('pre_order_id', $cart->pre_order_id)
            ->first();

        if (!$order) {
            abort(404);
        }

        DB::beginTransaction();

        try {

            // 注文IDをセッションに保存
            Session::put('order_id', $order->id);

            $lockUser = User::lockForUpdate()->find(Auth::id());
            $lockOrder = Order::lockForUpdate()->find($order->id);

            if ($lockOrder->status != config('order_status_id.processing')) {
                throw new \Exception('二重注文');
            }

            // 注文番号の乱数バージョン作成
            // 【Z】【月日】【注文番号下4桁】【年】【時分】
            // 【Z】【0607】【2071】【2022】【1351】→ 2022年6月7日13時51分 下4桁：2071
            $date = Carbon::now();
            $monthday = $date->format('md');
            $createId = sprintf('%04d', $order->id);
            $year = $date->format('Y');
            $timeminute = $date->format('Hi');
            $orderNo = "Z".$monthday.$createId.$year.$timeminute;
            $lockOrder->order_no = $orderNo;

            $lockOrder->use_point = $request->input('use_point');
            $lockOrder->payment_total = $lockOrder->payment_total - $request->input('use_point');
            $lockOrder->add_point = (($lockOrder->payment_total - $request->input('use_point')) - $lockOrder->delivery_fee_total) * 0.05;
            $lockOrder->status = config('order_status_id.new');
            $lockOrder->payment_id = $request->input('payment');
            $lockOrder->payment_method = $this->paymentRepository->get($request->input('payment'))->name;
            $lockOrder->save();

            $lockUser->point = $lockUser->point - $request->input('use_point');
            //$lockOrder->first_buy_date = "";
            $lockUser->last_buy_date = Carbon::now();
            //$lockOrder->buy_times = "";
            $lockUser->buy_total += $lockOrder->total;
            $lockUser->save();

        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();

        // カート詳細削除
        foreach ($order->orderDetails as $orderDetail) {
            CartDetail::where('cart_id', Session::get('cart_key'))
                ->where('product_class_id', $orderDetail->product_class_id)
                ->delete();
        }
        
        // カート削除
        Cart::where('id', Session::get('cart_key'))
            ->delete();

        // 注文完了メール送信
        // MailクラスとBuyクラスを使ってメールを送信する
        // Mail::to(Auth::user()->email)->send(new Buy());

        return redirect(route('shopping.complete'));
    }

    /**
     * 購入完了
     */
    public function complete(Request $request)
    {
        // 注文を取得
        $orderId = Session::get('order_id');
        $order = Order::find($orderId);
        
        // カートセッション削除
        Session::forget('cart_key');
        
        return view('front.shopping/complete', [
            'order' => $order
        ]);
    }
}