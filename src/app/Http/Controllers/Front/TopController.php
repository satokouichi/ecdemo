<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;

class TopController extends Controller
{
    private $productRepository;

    public function __construct
    (
        ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }
    
    /**
     * トップページ
     */
    public function index()
    {
        // 人気の商品情報
        $params['product_ids'] = [3,2,4,1];
        $qb = $this->productRepository->searchProductFrontQuery($params);
        $popularProducts = $this->productRepository->arrayProducts($qb->get());

        return view('front.top', [
            'popularProducts' => $popularProducts
        ]);
    }
}