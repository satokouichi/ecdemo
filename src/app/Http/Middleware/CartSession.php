<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\Cart;
use App\Models\CartDetail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;

class CartSession
{
    private $cartRepository;

    private $cartDetailRepository;

    public function __construct (
        CartRepository $cartRepository,
        CartDetailRepository $cartDetailRepository
    )
    {
        $this->cartRepository = $cartRepository;
        $this->cartDetailRepository = $cartDetailRepository;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('cart_key')) {
            $cartKey = Str::random(32);
            $cart = Cart::create([
                'user_id' => Auth::id(),
                'cart_key' => $cartKey,
                'delivery_fee_total' => 0,
                'total' => 0,
                'add_point' => 0,
                'use_point' => 0,
            ]);
            Session::put('cart_key', $cartKey);
        } else {
            // セッションにcartが存在していてもDBになければcart作り直し
            $cartKey = Session::get('cart_key');
            $cart = $this->cartRepository->getCart($cartKey);
            if ($cart) {
                $cart->user_id = Auth::id();
                $cart->save();   
            } else {
                $cartKey = Str::random(32);
                $cart = Cart::create([
                    'user_id' => Auth::id(),
                    'cart_key' => $cartKey,
                    'delivery_fee' => 0,
                    'total' => 0,
                    'add_point' => 0,
                    'use_point' => 0,
                ]);
                Session::put('cart_key', $cartKey);
            }
        }

        return $next($request);
    }
}
