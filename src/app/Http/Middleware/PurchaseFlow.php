<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Str;
use App\Repositories\Cart\CartRepositoryInterface AS CartRepository;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;
use App\Repositories\Order\OrderRepositoryInterface AS OrderRepository;
use App\Repositories\OrderDetail\OrderDetailRepositoryInterface AS OrderDetailRepository;
use App\Repositories\Payment\PaymentRepositoryInterface AS PaymentRepository;

class PurchaseFlow
{
    private $cartRepository;

    private $cartDetailRepository;

    private $orderRepository;

    private $orderDetailRepository;

    private $paymentRepository;
    
    public function __construct (
        CartRepository $cartRepository,
        CartDetailRepository $cartDetailRepository,
        OrderRepository $orderRepository,
        OrderDetailRepository $orderDetailRepository,
        PaymentRepository $paymentRepository
    )
    {
        $this->cartRepository = $cartRepository;
        $this->cartDetailRepository = $cartDetailRepository;
        $this->orderRepository = $orderRepository;
        $this->orderDetailRepository = $orderDetailRepository;
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // カートを取得
        $cartKey = Session::get('cart_key');
        $cart = $this->cartRepository->getCart($cartKey);
        if ( !$cart ) {
            abort(404);
        }
        if ( $cart->pre_order_id ) {
            $preOrderId = $cart->pre_order_id;
        } else {
            $preOrderId = Str::random(32);
            $cart->pre_order_id = $preOrderId;
            $cart->save();   
        }

        // 会員を取得
        $userId = Auth::id();
        $user = User::find($userId);
        if ( !$user ) {
            abort(404);
        }

        $total = $cart->total + $cart->delivery_fee_total;
        $addPoint = $total * 0.05;
        $orderParams = [
            'order_no'           => $userId,
            'user_id'            => $userId,
            'order_pref'         => $user->pref,
            'order_sex'          => $user->sex,
            'pre_order_id'       => $preOrderId,
            'order_name01'       => $user->name01,
            'order_name02'       => $user->name02,
            'order_kana01'       => $user->kana01,
            'order_kana02'       => $user->kana02,
            'order_company_name' => $user->company_name,
            'order_email'        => $user->email,
            'order_zip01'        => $user->zip01,
            'order_zip02'        => $user->zip02,
            'order_addr01'       => $user->addr01,
            'order_birth'        => $user->birth,
            'subtotal'           => $cart->total,
            'discount'           => 0,
            'delivery_fee_total' => $cart->delivery_fee_total,
            'user_point'         => 0,
            'add_point'          => $addPoint,
            'total'              => $total,
            'payment_total'      => $total,
            'status'             => config('order_status_id.processing'),
        ];

        $order = $this->orderRepository->getPreOrder($preOrderId);
        if ($order) {
            $this->orderRepository->updateOrderByCart($orderParams, $cart, $order);           
        } else {
            $this->orderRepository->createOrderByCart($orderParams, $cart);
        }

        return $next($request);
    }
}
