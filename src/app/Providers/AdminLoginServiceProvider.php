<?php

namespace App\Providers;

use Laravel\Fortify\Fortify;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use App\Actions\Admin\CreateNewUser;
use App\Actions\Admin\ResetUserPassword;
use App\Actions\Admin\UpdateUserPassword;
use App\Actions\Admin\UpdateUserProfileInformation;
use App\Actions\Admin\AttemptToAuthenticate;
use App\Http\Controllers\Admin\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\Auth\RegisteredUserController;

class AdminLoginServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app
            ->when([AttemptToAuthenticate::class, AuthenticatedSessionController::class])
            ->needs(StatefulGuard::class)
            ->give(function () {
                return Auth::guard('admins');
            });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);

        RateLimiter::for('login', function (Request $request) {
            $email = (string) $request->email;

            return Limit::perMinute(5)->by($email.$request->ip());
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });

        Fortify::loginView(function () {
            return view('admin.auth.login');
        });
        
        Fortify::registerView(function () {
            return view('admin.auth.register');
        });

        Fortify::confirmPasswordView(function () {
            return view('admin.auth.confirm-password');
        });

        Fortify::twoFactorChallengeView(function () {
            return view('admin.auth.two-factor-challenge');
        });
        */
    }
}