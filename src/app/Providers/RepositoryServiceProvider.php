<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // カート
        $this->app->bind(
            \App\Repositories\Cart\CartRepositoryInterface::class,
            \App\Repositories\Cart\CartRepository::class
        );

        // カート詳細
        $this->app->bind(
            \App\Repositories\CartDetail\CartDetailRepositoryInterface::class,
            \App\Repositories\CartDetail\CartDetailRepository::class
        );
        
        // カテゴリ
        $this->app->bind(
            \App\Repositories\Category\CategoryRepositoryInterface::class,
            \App\Repositories\Category\CategoryRepository::class
        );

        // 分類
        $this->app->bind(
            \App\Repositories\ClassCategory\ClassCategoryRepositoryInterface::class,
            \App\Repositories\ClassCategory\ClassCategoryRepository::class
        );

        // 規格
        $this->app->bind(
            \App\Repositories\ClassName\ClassNameRepositoryInterface::class,
            \App\Repositories\ClassName\ClassNameRepository::class
        );

        // コンビニ
        $this->app->bind(
            \App\Repositories\ConveniOrder\ConveniOrderRepositoryInterface::class,
            \App\Repositories\ConveniOrder\ConveniOrderRepository::class
        );

        // 商品割引率
        $this->app->bind(
            \App\Repositories\Discount\DiscountRepositoryInterface::class,
            \App\Repositories\Discount\DiscountRepository::class
        );

        // メール履歴
        $this->app->bind(
            \App\Repositories\MailHistory\MailHistoryRepositoryInterface::class,
            \App\Repositories\MailHistory\MailHistoryRepository::class
        );

        // メールテンプレート
        $this->app->bind(
            \App\Repositories\MailTemplate\MailTemplateRepositoryInterface::class,
            \App\Repositories\MailTemplate\MailTemplateRepository::class
        );

        // 会員
        $this->app->bind(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserRepository::class
        );

        // 注文
        $this->app->bind(
            \App\Repositories\Order\OrderRepositoryInterface::class,
            \App\Repositories\Order\OrderRepository::class
        );

        // 注文詳細
        $this->app->bind(
            \App\Repositories\OrderDetail\OrderDetailRepositoryInterface::class,
            \App\Repositories\OrderDetail\OrderDetailRepository::class
        );

        // 入金処理
        $this->app->bind(
            \App\Repositories\OrderPayment\OrderPaymentRepositoryInterface::class,
            \App\Repositories\OrderPayment\OrderPaymentRepository::class
        );

        // クレジット履歴
        $this->app->bind(
            \App\Repositories\OrderCredit\OrderCreditRepositoryInterface::class,
            \App\Repositories\OrderCredit\OrderCreditRepository::class
        );

        // 商品
        $this->app->bind(
            \App\Repositories\Product\ProductRepositoryInterface::class,
            \App\Repositories\Product\ProductRepository::class
        );

        // 商品のカテゴリ
        $this->app->bind(
            \App\Repositories\ProductCategory\ProductCategoryRepositoryInterface::class,
            \App\Repositories\ProductCategory\ProductCategoryRepository::class
        );

        // 商品規格
        $this->app->bind(
            \App\Repositories\ProductClass\ProductClassRepositoryInterface::class,
            \App\Repositories\ProductClass\ProductClassRepository::class
        );

        // 商品画像
        $this->app->bind(
            \App\Repositories\ProductImage\ProductImageRepositoryInterface::class,
            \App\Repositories\ProductImage\ProductImageRepository::class
        );

        // セット商品
        $this->app->bind(
            \App\Repositories\ProductMixtureMerge\ProductMixtureMergeRepositoryInterface::class,
            \App\Repositories\ProductMixtureMerge\ProductMixtureMergeRepository::class
        );

        // レビュー
        $this->app->bind(
            \App\Repositories\ProductReview\ProductReviewRepositoryInterface::class,
            \App\Repositories\ProductReview\ProductReviewRepository::class
        );

        // 配送
        $this->app->bind(
            \App\Repositories\TrackingNumber\TrackingNumberRepositoryInterface::class,
            \App\Repositories\TrackingNumber\TrackingNumberRepository::class
        );

        // WebMoney
        $this->app->bind(
            \App\Repositories\WebmoneyOrder\WebmoneyOrderRepositoryInterface::class,
            \App\Repositories\WebmoneyOrder\WebmoneyOrderRepository::class
        );

        // 注文ステータス
        $this->app->bind(
            \App\Repositories\OrderStatus\OrderStatusRepositoryInterface::class,
            \App\Repositories\OrderStatus\OrderStatusRepository::class
        );

        // お支払い方法
        $this->app->bind(
            \App\Repositories\Payment\PaymentRepositoryInterface::class,
            \App\Repositories\Payment\PaymentRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}