<?php

namespace App\Actions\Front;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use App\Rules\EntryFormPrefRule;
use App\Rules\EntryFormAgeYearRule;
use App\Rules\EntryFormAgeMonthRule;
use App\Rules\EntryFormAgeDayRule;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        $birth = "";
        if ($input['year']) {
            $birth .= $input['year'];
        }
        if ($input['month']) {
            $birth .= "-".sprintf('%02d', $input['month']);
        }
        if ($input['day']) {
            $birth .= "-".sprintf('%02d', $input['day']);
        }
        Validator::make($input, [
            'name01' => ['required', 'string', 'max:255'],
            'name02' => ['required', 'string', 'max:255'],
            'kana01' => ['required', 'string', 'max:255'],
            'kana02' => ['required', 'string', 'max:255'],
            'sex' => ['required'],
            'year' => [new EntryFormAgeYearRule],
            'month' => [new EntryFormAgeMonthRule],
            'day' => [new EntryFormAgeDayRule],
            'zip01' => ['required'],
            'zip02' => ['required'],
            'pref' => [new EntryFormPrefRule],
            'addr01' => ['required', 'string', 'max:255'],
            'email' => ['required','string','email','max:255',Rule::unique(User::class)],
            'password' => $this->passwordRules(),
        ])->validate();

        return User::create([
            'status' => 1,
            'sex' => $input['sex'],
            'pref' => 1,
            'name01' => $input['name01'],
            'name02' => $input['name02'],
            'kana01' => $input['kana01'],
            'kana02' => $input['kana02'],
            'company_name' => $input['company_name'],
            'zip01' => $input['zip01'],
            'zip02' => $input['zip02'],
            'zipcode' => null,
            'addr01' => $input['addr01'],
            'addr02' => null,
            'tel01' => null,
            'tel02' => null,
            'tel03' => null,
            'tel' => null,
            'birth' => $birth,
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'first_buy_date' => null,
            'last_buy_date' => null,
            'buy_times' => null,
            'buy_total' => null,
            'point' => '300',
            'note' => null,
        ]);
    }
}