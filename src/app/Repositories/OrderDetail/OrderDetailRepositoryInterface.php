<?php

//declare(strict_types=1);

namespace App\Repositories\OrderDetail;

interface OrderDetailRepositoryInterface
{
    public function createOrderDetail($params);
    public function updateOrderDetail($orderDetailId, $params);
    public function seachOrderDetail($orderId, $productClassId);
    public function deleteOrderDetail($orderId, $productClassId);
}