<?php

namespace App\Repositories\OrderDetail;

use Illuminate\Support\Facades\DB;
use App\Models\OrderDetail;

class OrderDetailRepository implements OrderDetailRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * 注文詳細作成
     */
    public function createOrderDetail($params)
    {
        $orderDetail = OrderDetail::create($params);
        if ( $orderDetail->save() ) {
            return $orderDetail;
        }
    }

    /**
     * 注文詳細更新
     */
    public function updateOrderDetail($orderDetailId, $params)
    {
        $orderDetail = OrderDetail::find($orderDetailId)
                    ->fill($params)
                    ->save();
    }

    public function seachOrderDetail($orderId, $productClassId)
    {
        $orderDetail = OrderDetail::where('order_id', $orderId)
            ->where('product_class_id', $productClassId)
            ->first();

        return $orderDetail;
    }

    public function deleteOrderDetail($orderId, $productClassId)
    {
        OrderDetail::where('order_id', $orderId)
            ->where('product_class_id', $productClassId)
            ->delete();
    }
}