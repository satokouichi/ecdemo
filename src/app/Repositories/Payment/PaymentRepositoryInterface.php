<?php

namespace App\Repositories\Payment;

interface PaymentRepositoryInterface
{
    public function get($id);
    public function getAll();
}