<?php

namespace App\Repositories\Payment;

use Illuminate\Support\Facades\DB;
use App\Models\Payment;

class PaymentRepository implements PaymentRepositoryInterface
{
    public function __construct()
    {
        //
    }

    public function get($id)
    {
        $qb = Payment::from('payments AS p')
            ->select('p.*')
            ->where('p.id', $id)
            ->where('p.status', 1);

        return $qb->first();
    }

    public function getAll()
    {
        $qb = Payment::from('payments AS p')
            ->select('p.*')
            ->where('p.status', 1);

        return $qb->get();
    }
}