<?php

namespace App\Repositories\Cart;

use Illuminate\Support\Facades\DB;
use App\Models\Cart;
use App\Repositories\CartDetail\CartDetailRepositoryInterface AS CartDetailRepository;

class CartRepository implements CartRepositoryInterface
{
    private $cartDetailRepository;

    public function __construct (
        CartDetailRepository $cartDetailRepository
    )
    {
        $this->cartDetailRepository = $cartDetailRepository;
    }

    public function deleteCart($cartKey)
    {
        $cart = Cart::where('cart_key', $key)
            ->delete();

        return $cart;
    }

    public function deleteCartByCart($cartKey, $productClassId)
    {
        $cart = $this->getCart($cartKey);
        $cartDetail = $this->cartDetailRepository->deleteCartDetail($cart->id, $productClassId);

        // カート商品が1つもない場合はカート自体削除
        $cartDetails = $this->cartDetailRepository->getCartDetails($cart->id);
        if (!$cartDetails) {
            $this->deleteCart($cartKey);
        }
    }

    public function getCart($cartKey)
    {
        $cart = Cart::where('cart_key', $cartKey)
            ->first();

        return $cart;
    }

    public function arrayCarts($params)
    {
        $cart = [];
        $cart['id']                 = $params->id;
        $cart['delivery_fee_total'] = $params->delivery_fee_total;
        $cart['total']              = $params->total;
        $cart['use_point']          = $params->use_point;
        $cart['add_point']          = $params->add_point;
        foreach ($params->cartDetails as $cartDetail) {
            $id = $cartDetail->id;
            $cart['cartDetails'][$id]['cart_id']          = $id;
            $cart['cartDetails'][$id]['product_id']       = $cartDetail->product_id;
            $cart['cartDetails'][$id]['product_class_id'] = $cartDetail->product_class_id;
            $cart['cartDetails'][$id]['price']            = intval($cartDetail->price);
            $cart['cartDetails'][$id]['quantity']         = intval($cartDetail->quantity);
            $cart['cartDetails'][$id]['subtotal']         = intval($cartDetail->price * $cartDetail->quantity);
            $cart['cartDetails'][$id]['dose']             = intval($cartDetail->productClass->dose);
            $cart['cartDetails'][$id]['name']             = $cartDetail->productClass->product->name;
            $cart['cartDetails'][$id]['shape']            = $cartDetail->productClass->product->shape;
        } unset($cartDetail);

        return $cart;
    }
}