<?php

namespace App\Repositories\Cart;

interface CartRepositoryInterface
{
    public function deleteCart($cartKey);
    public function deleteCartByCart($cartKey, $productClassId);
    public function getCart($cartKey);
    public function arrayCarts($params);
}