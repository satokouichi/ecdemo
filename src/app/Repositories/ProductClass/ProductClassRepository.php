<?php

namespace App\Repositories\ProductClass;

use Illuminate\Support\Facades\DB;
use App\Models\ProductClass;
use App\Repositories\Discount\DiscountRepositoryInterface AS DiscountRepository;
use App\Repositories\Product\ProductRepositoryInterface AS ProductRepository;
use App\Repositories\ClassCategory\ClassCategoryRepositoryInterface AS ClassCategoryRepository;


class ProductClassRepository implements ProductClassRepositoryInterface
{
    private $discountRepository;
    private $productRepository;
    private $classCategoryRepository;

    public function __construct (
        DiscountRepository $discountRepository,
        ProductRepository $productRepository,
        ClassCategoryRepository $classCategoryRepository
    )
    {
        $this->discountRepository = $discountRepository;
        $this->productRepository = $productRepository;
        $this->classCategoryRepository = $classCategoryRepository;
    }

    /**
     * INSERT
     */
    public function createProductClass($params)
    {
        $productClass = ProductClass::create($params);
        if ( $productClass->save() ) {
            return $productClass;
        }
    }

    /**
     * UPDATE
     */
    public function updateProductClass($params, $id)
    {
        $result = ProductClass::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    /**
     * 商品規格を更新
     */
    public function updateProductClassByAdmin($params)
    {
        $discountPattern = $params['discount_pattern'];
        $productClasses = $params['class'];

        $product = $this->productRepository->getProduct([
                'id' => $params['product_id'],
            ])->first();

        if ( $product && $product->discount_pattern != $discountPattern ) {
            $this->productRepository->updateProduct(['discount_pattern' => $discountPattern], $product->id);
        }

        // 価格・数量の基準値を配列にセット
        $baseVals = [];
        foreach ( $productClasses as $productClass ) {
            $set = $this->classCategoryRepository->getClassCategory([
                    'id' => $productClass['class_category_id2'],
                ])->first();
            $set = intval($set->name);
            if ( $set == 1 ) {
                $baseVals[$productClass['class_category_id1']]['price01'] = $productClass['price01'];
                $baseVals[$productClass['class_category_id1']]['dose']    = $productClass['dose'];
            }
        } unset ( $productClass );

        // 登録処理
        $params = [];
        foreach ( $productClasses as $key => $val ) {
            $set = $this->classCategoryRepository->getClassCategory([
                    'id' => $val['class_category_id2'],
                ])->first();
            $set = intval($set->name);
            $dbDiscount = $this->discountRepository->getDiscount([
                'pattern'   => $discountPattern,
                'order_num' => $set
                ])->first();
            if ( !$dbDiscount ) { continue; }
            $price = $baseVals[$val['class_category_id1']]['price01'];
            $params['id']                 = $val['id'];
            $params['class_category_id1'] = $val['class_category_id1'];
            $params['class_category_id2'] = $val['class_category_id2'];
            $params['product_code']       = $val['product_code'];
            $params['zaiko_id']           = $val['zaiko_id'];
            $params['stock']              = array_key_exists('stock', $val) ? $val['stock'] : null;
            $params['status']             = $val['status'];
            $params['price01']            = $price * $set;
            $params['price02']            = ($price - round($price * ($dbDiscount->discount / 100))) * $set;
            $params['dose']               = $baseVals[$val['class_category_id1']]['dose'] * $set;
            $this->updateProductClass($params, $val['id']);
        } unset ( $key, $val );

        return true;
    }

    /**
     * 商品規格を取得
     */
    public function getProductClass($params)
    {
        $qb = ProductClass::from('product_classes AS pc')
            ->select('pc.*');

        // 商品規格ID
        if ( array_key_exists('id', $params) && $params['id'] ) {
            $qb->where('pc.id', $params['id']);
        }

        // 商品ID
        if ( array_key_exists('product_id', $params) && $params['product_id'] ) {
            $qb->where('pc.product_id', $params['product_id']);
        }
        
        // 分類ID1
        if ( array_key_exists('class_category_id1', $params) && $params['class_category_id1'] ) {
            $qb->where('pc.class_category_id1', $params['class_category_id1']);
        }

        // 分類ID2
        if ( array_key_exists('class_category_id2', $params) && $params['class_category_id2'] ) {
            $qb->where('pc.class_category_id2', $params['class_category_id2']);
        }

        return $qb;
    }
    
    /**
     * 商品の規格一覧を取得
     */
    public function getProductClasses($productId)
    {
        $ProductClasses = ProductClass::where('product_id', $productId)
            ->get();

        return $ProductClasses;
    }
}