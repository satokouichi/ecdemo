<?php

namespace App\Repositories\ProductClass;

interface ProductClassRepositoryInterface
{
    public function createProductClass($params);
    public function updateProductClass($params, $id);
    public function updateProductClassByAdmin($request);
    public function getProductClass($params);
    public function getProductClasses($productId);
}