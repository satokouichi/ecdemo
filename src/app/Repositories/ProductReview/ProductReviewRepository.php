<?php

namespace App\Repositories\ProductReview;

use Illuminate\Support\Facades\DB;
use App\Models\ProductReview;
use Carbon\Carbon;

class ProductReviewRepository implements ProductReviewRepositoryInterface
{
    protected $table = 'product_reviews';

    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createProductReview($params)
    {
        $productReview = ProductReview::create($params);
        if ( $productReview->save() ) {
            return $productReview;
        }
    }

    /**
     * UPDATE
     */
    public function updateProductReview($params, $id)
    {
        $result = ProductReview::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    /**
     * レビューを登録
     */
    public function createProductReviewByCommon($request)
    {
        $params = $request->except(['id','_token']);
        return DB::transaction(function() use ($params) {
            $review = $this->createProductReview($params);

            return $review;
        });
    }
    
    /**
     * レビューを更新
     */
    public function updateProductReviewByAdmin($request)
    {
        $params = $request->except(['id','_token']);
        if ( $this->updateProductReview($params, $request->input('id')) ) {
            return true;
        } else {
            return false;
        }
    }

    public function searchReviewAdminQuery($params)
    {
        $qb = ProductReview::from('product_reviews AS r')
            ->select('r.*')
            ->distinct()
            ->where('r.del_flg', 0);

        $joinedProduct = false;
        $joinedProductClass = false;
        $joinedCustomer = false;

        //dd($params);

        // レビューID（複数可）
        if ( array_key_exists('review_ids', $params) && $params['review_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['review_ids']);
            $qb->whereIn('r.id', explode(',', $ids));
        }

        // レビューURL
        if ( array_key_exists('product_review_id_random', $params) && $params['product_review_id_random'] ) {
            // 空白を削除
            $url = str_replace(array(" ", "　"), "", $params['product_review_id_random']);
            $url = strstr($url, 'comments/');
            $url = str_replace('comments/', '', $url);
            $qb->where('r.product_review_id_random', $url);
        }

        // レビューID以上
        if ( array_key_exists('review_over', $params) && $params['review_over'] ) {
            $qb->where('r.id', '>=', $params['review_over']);
        }

        // 商品ID（複数可）
        if ( array_key_exists('product_ids', $params) && $params['product_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['product_ids']);
            $qb->whereIn('r.product_id', explode(',', $ids));
        }

        // 商品名
        if ( array_key_exists('product_name', $params) && $params['product_name'] ) {
            if ( !$joinedProduct ) {
                $qb->join('products as p', 'r.product_id', '=', 'p.id');
                $joinedProduct = true;
            }
            $name = $params['product_name'];
            $qb->where('p.name', 'LIKE', "%$name%");
        }

        // 商品コード
        if ( array_key_exists('product_code', $params) && $params['product_code'] ) {
            if ( !$joinedProduct ) {
                $qb->join('products as p', 'r.product_id', '=', 'p.id');
                $joinedProduct = true;
                if ( !$joinedProductClass ) {
                    $qb->join('product_classes as pc', 'pc.product_id', '=', 'p.id');
                    $joinedProductClass = true;
                }
            }
            $name = $params['product_code'];
            $qb->where('pc.product_code', 'LIKE', "%$name%");
        }

        // 会員ID（複数可）
        if ( array_key_exists('user_ids', $params) && $params['user_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['user_ids']);
            $qb->whereIn('r.user_id', explode(',', $ids));
        }

        // タイトル
        if ( array_key_exists('review_title', $params) && $params['review_title'] ) {
            $qb->where('r.title', $params['review_title']);
        }

        // おすすめレベル
        if ( array_key_exists('review_level', $params) && $params['review_level'] ) {
            $qb->where('r.recommend_level', $params['review_level']);
        }

        // 登録日
        if ( array_key_exists('review_create_start', $params) && $params['review_create_start'] ) {
            $date = $params['review_create_start'];
            $qb->where('r.create_date', '>=' , $date);
        }
        if ( array_key_exists('review_create_end', $params) && $params['review_create_end'] ) {
            $date = Carbon::parse($params['review_create_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('r.create_date', '<' , $date);
        }

        // 更新日
        if ( array_key_exists('review_update_start', $params) && $params['review_update_start'] ) {
            $date = $params['review_update_start'];
            $qb->where('r.update_date', '>=' , $date);
        }
        if ( array_key_exists('review_update_end', $params) && $params['review_update_end'] ) {
            $date = Carbon::parse($params['review_update_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('r.update_date', '<' , $date);
        }

        return $qb;
    }

    public function searchProductAdminOrder($params)
    {
        $qb = Product::from('products AS p')
            ->select('p.*')
            ->distinct()
            ->leftJoin('product_category as pct', 'pct.product_id', '=', 'p.id')
            ->join('product_classes as pc', 'pc.product_id', '=', 'p.id')
            ->where('p.del_flg', 0);

        // カテゴリ
        if ( array_key_exists('category', $params) && $params['category'] ) {
            $category = $params['category'];
            $qb->where('pct.category_id', $category);
        }

        // 商品ID or 商品名 or 商品コード
        if ( array_key_exists('product', $params) && $params['product'] ) {
            $product = preg_match('/^\d{0,10}$/', $params['product']) ? $params['product'] : null;
            $qb
                ->orWhere('p.id', $product)
                ->orWhere('p.name', 'like' , '%'.str_replace(['%', '_'], ['\\%', '\\_'], $product).'%')
                ->orWhere('pc.product_code', $product);
        }

        return $qb->get()->toArray();
    }

    /**
     * レビューを取得
     */
    public function getProductReview($id)
    {
        $productReview = ProductReview::findOrFail($id);

        return $productReview;
    }
}