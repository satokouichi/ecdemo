<?php

namespace App\Repositories\ProductReview;

interface ProductReviewRepositoryInterface
{
    public function createProductReview($params);
    public function updateProductReview($params, $id);
    public function createProductReviewByCommon($request);
    public function updateProductReviewByAdmin($request);
    public function searchReviewAdminQuery($params);
    public function getProductReview($id);
}