<?php

//declare(strict_types=1);

namespace App\Repositories\ConveniOrder;

use Illuminate\Support\Facades\DB;

class ConveniOrderRepository implements ConveniOrderRepositoryInterface
{
    protected $table = 'conveni_orders';

    public function __construct()
    {
        //
    }

    public function getAll()
    {
        return DB::table($this->table)->get();
    }
}