<?php

//declare(strict_types=1);

namespace App\Repositories\ConveniOrder;

interface ConveniOrderRepositoryInterface
{
    public function getAll();
}