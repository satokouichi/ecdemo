<?php

namespace App\Repositories\TrackingNumber;

use Illuminate\Support\Facades\DB;
use App\Models\TrackingNumber;

class TrackingNumberRepository implements TrackingNumberRepositoryInterface
{
    protected $table = 'tracking_numbers';

    public function __construct()
    {
        //
    }

    public function getAll()
    {
        return DB::table($this->table)->get();
    }

    /**
     * INSERT
     */
    public function createTrackingNumber($params)
    {
        $trackingNumber = TrackingNumber::create($params);
        if ( $trackingNumber->save() ) {
            return $trackingNumber;
        }
    }

    /**
     * DELETE
     */
    public function deleteTrackingNumber($params)
    {
        $trackingNumber = TrackingNumber::find($params)->delete();
    }
}