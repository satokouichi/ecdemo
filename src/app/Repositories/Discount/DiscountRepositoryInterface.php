<?php

namespace App\Repositories\Discount;

interface DiscountRepositoryInterface
{
    public function getDiscount($params);
    public function getDiscounts();
}