<?php

namespace App\Repositories\Discount;

use Illuminate\Support\Facades\DB;
use App\Models\Discount;

class DiscountRepository implements DiscountRepositoryInterface
{
    protected $table = 'discounts';

    public function __construct()
    {
        //
    }

    /**
     * 割引率を取得
     */
    public function getDiscount($params)
    {
        $qb = Discount::from('discounts AS d')
            ->select('d.*');

        // 割引パターン
        if ( array_key_exists('pattern', $params) && $params['pattern'] ) {
            $qb->where('d.pattern', $params['pattern']);
        }

        // セット数
        if ( array_key_exists('order_num', $params) && $params['order_num'] ) {
            $qb->where('d.order_num', $params['order_num']);
        }

        return $qb;
    }
    
    public function getDiscounts()
    {
        $qb = Discount::from('discounts AS d')
            ->select('d.*');

        return $qb;
    }
}