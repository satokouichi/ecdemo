<?php

namespace App\Repositories\CartDetail;

use Illuminate\Support\Facades\DB;
use App\Models\CartDetail;

class CartDetailRepository implements CartDetailRepositoryInterface
{
    public function __construct (
        //
    )
    {
        //
    }

    public function deleteCartDetail($cartId, $productClassId)
    {
        CartDetail::where('cart_id', $cartId)
            ->where('product_class_id', $productClassId)
            ->delete();
    }

    public function getCartDetail($cartId, $productClassId)
    {
        $cartDetail = CartDetail::where('cart_id', $cartId)
            ->where('product_class_id', $productClassId)
            ->first();

        return $cartDetail;
    }

    public function getCartDetails($cartId)
    {
        $cartDetails = CartDetail::where('cart_id', $cartId)
            ->get();

        return $cartDetails;
    }
}