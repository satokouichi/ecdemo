<?php

namespace App\Repositories\CartDetail;

interface CartDetailRepositoryInterface
{
    public function deleteCartDetail($cartId, $productClassId);
    public function getCartDetail($cartId, $productClassId);
    public function getCartDetails($cartId);
}