<?php

namespace App\Repositories\Order;

interface OrderRepositoryInterface
{
    public function createOrder($params);
    public function updateOrder($params, $orderId);
    public function createOrderByAdmin($request);
    public function updateOrderByAdmin($request);
    public function createOrderByCart($params, $cart);
    public function updateOrderByCart($params, $cart, $order);
    public function searchOrderAdminQuery($params);
    public function getPreOrder($preOrderId);
    public function getOrder($orderId);
    public function getUserOrders($userId);
    public function arrayOrders($params);
}