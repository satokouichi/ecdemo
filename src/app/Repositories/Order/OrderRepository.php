<?php

namespace App\Repositories\Order;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Repositories\OrderDetail\OrderDetailRepositoryInterface AS OrderDetailRepository;
use App\Repositories\ProductClass\ProductClassRepositoryInterface AS ProductClassRepository;

class OrderRepository implements OrderRepositoryInterface
{
    private $orderDetailRepository;
    
    public function __construct(
        OrderDetailRepository $orderDetailRepository,
        ProductClassRepository $productClassRepository
    )
    {
        $this->orderDetailRepository = $orderDetailRepository;
        $this->productClassRepository = $productClassRepository;
    }

    /**
     * INSERT
     */
    public function createOrder($params)
    {
        $order = Order::create($params);
        if ( $order->save() ) {
            return $order;
        }
    }

    /**
     * UPDATE
     */
    public function updateOrder($params, $orderId)
    {
        $result = Order::find($orderId)
            ->fill($params)
            ->save();

        return $result;
    }

    /**
     * 【管理画面】注文作成
     */
    public function createOrderByAdmin($request)
    {
        $params = $request->except(['id','_token']);
        return DB::transaction(function() use ($params) {
            $order = $this->createOrder($params);

            return $order;
        });        
    }

    /**
     * 【管理画面】注文更新
     */
    public function updateOrderByAdmin($request)
    {
        $params = $request->except(['_token']);

        return DB::transaction(function() use ($params) {

            // 注文更新
            $this->updateOrder($params, $params['id']);

            // 更新前の注文詳細
            $oldOrderDetails = $this->getOrder($params['id'])->orderDetails;
            $oldOrderDetailIds = [];
            foreach ( $oldOrderDetails as $orderDetail ) {
                $oldOrderDetailIds[] = $orderDetail->product_class_id;
            } unset($orderDetail);

            // 更新後の注文詳細
            $newOrderDetails = $params['order_details'];
            foreach ( $newOrderDetails as $orderDetail ) {
                $newOrderDetailIds[] = $orderDetail['product_class_id'];
                $oldOrderDetail = $this->orderDetailRepository->seachOrderDetail($params['id'], $orderDetail['product_class_id']);
                if ( $oldOrderDetail ) {
                    $oldOrderDetail->quantity = $orderDetail['quantity'];
                    $oldOrderDetail->save();
                } else {
                    $productClass = $this->productClassRepository->getProductClass($orderDetail['product_class_id']);
                    if ($productClass) {
                        $orderDetail = OrderDetail::create([
                            'order_id'         => $params['id'],
                            'product_id'       => $productClass->product_id,
                            'product_class_id' => $productClass->id,
                            'product_name'     => $productClass->product->name,
                            'price'            => $productClass->price02,
                            'quantity'         => $orderDetail['quantity'],
                            'product_code'     => $productClass->product_code,
                            'zaiko_id'         => $productClass->zaiko_id,
                        ]);
                    }
                }
            } unset($orderDetail);

            $diff = array_diff($oldOrderDetailIds, $newOrderDetailIds);
            if ( !empty($diff) ) {
                foreach ( $diff as $productClassId ) {
                    //$this->orderDetailRepository->deleteOrderDetail($params['id'], $productClassId);
                }      

            }
        });
    }

    /**
     * 【フロント】注文作成
     */
    public function createOrderByCart($params, $cart)
    {
        return DB::transaction(function() use ($params, $cart) {
            $order = $this->createOrder($params);
            if ( !$order ) {
                throw new \Exception('failed create order');
            }
            if ( $cart->cartDetails ) {
                foreach ( $cart->cartDetails as $cartDetail ) {
                    $orderDetailParams = [
                        'order_id'         => $order->id,
                        'product_id'       => $cartDetail->product_id,
                        'product_class_id' => $cartDetail->product_class_id,
                        'product_name'     => $cartDetail->productClass->product->name,
                        'price'            => $cartDetail->price,
                        'quantity'         => $cartDetail->quantity,
                        'product_code'     => $cartDetail->productClass->product_code,
                    ];
                } unset($cartDetail);
                $this->orderDetailRepository->createOrderDetail($orderDetailParams);
            }
        });
    }

    /**
     * 【フロント】注文更新
     */
    public function updateOrderByCart($params, $cart, $order)
    {
        return DB::transaction(function() use ($params, $cart, $order) {
            $this->updateOrder($params, $order->id);
            $cartDetailIds = [];
            if ( $cart->cartDetails ) {
                foreach ( $cart->cartDetails as $cartDetail ) {
                    $cartDetailIds[] = $cartDetail->product_class_id;
                    $orderDetail = $this->orderDetailRepository->seachOrderDetail($order->id, $cartDetail->product_class_id);
                    if ( $orderDetail ) {
                        $orderDetail->product_id = $cartDetail->product_id;
                        $orderDetail->product_class_id = $cartDetail->product_class_id;
                        $orderDetail->quantity = $cartDetail->quantity;
                        $orderDetail->save();
                    } else {
                        $orderDetail = OrderDetail::create([
                            'order_id'         => $order->id,
                            'product_id'       => $cartDetail->product_id,
                            'product_class_id' => $cartDetail->product_class_id,
                            'product_name'     => $cartDetail->product->name,
                            'price'            => $cartDetail->price,
                            'quantity'         => $cartDetail->quantity,
                            'product_code'     => $cartDetail->productClass->code,
                            'zaiko_id'         => $cartDetail->productClass->code,
                        ]);
                    }
                } unset($cartDetail);
            }
            $orderDetailIds = [];
            foreach ( $order->orderDetails as $orderDetail ) {
                $orderDetailIds[] = $orderDetail->product_class_id;
            }
            $diff = array_diff($orderDetailIds, $cartDetailIds);
            if ( !empty($diff) ) {
                foreach ( $diff as $productClassId ) {
                    $this->orderDetailRepository->deleteOrderDetail($order->id, $productClassId);
                }              
            }
        });
    }

    /**
     * 注文一覧を取得
     */
    public function searchOrderAdminQuery($params)
    {
        $qb = Order::from('orders AS o')
            ->select('o.*')
            ->where('o.del_flg', 0);

        $joinedOrderDetails = false;
        $joinedUsers = false;
        $joinedTrackingNumbers = false;

        // 仮注文ID
        if ( array_key_exists('preOrderId', $params) && $params['preOrderId'] ) {
            $qb->where('o.pre_order_id', $params['preOrderId']);
        }

        // 注文ID
        if ( array_key_exists('order_id', $params) && $params['order_id'] ) {
            $qb->where('o.id', $params['order_id']);
        }

        // 注文ID（複数）
        if ( array_key_exists('order_ids', $params) && $params['order_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['order_ids']);
            $qb->whereIn('o.id', explode(',', $ids));
        }

        // 注文者名
        if ( array_key_exists('order_name', $params) && $params['order_name'] ) {
            $name = $params['order_name'];
            $qb->where(DB::raw('CONCAT(o.order_name01, o.order_name02)'), 'LIKE', "%$name%");
        }

        // メールアドレス
        if ( array_key_exists('order_email', $params) && $params['order_email'] ) {
            $email = $params['order_email'];
            $qb->where('o.order_email', 'LIKE', "%$email%");
        }

        // 注文番号（乱数）
        if ( array_key_exists('order_no', $params) && $params['order_no'] ) {
            $qb->where('o.order_no', $params['order_no']);
        }

        // 注文者フリガナ
        if ( array_key_exists('order_kana', $params) && $params['order_kana'] ) {
            $kana = $params['order_kana'];
            $qb->where(DB::raw('CONCAT(o.order_kana01, o.order_kana02)'), 'LIKE', "%$kana%");
        }

        // 電話番号
        if ( array_key_exists('order_tel', $params) && $params['order_tel'] ) {
            $tel = preg_replace('/[^0-9]/ ', '', $params['order_tel']);
            $qb->where('o.order_tel', 'LIKE', "%$tel%");
        }

        // 注文方法（web注文・電話注文）
        if ( array_key_exists('order_how', $params) && $params['order_how'] ) {
            switch ( $params['order_how'] ) {
                case [1]: // web注文
                    $qb->whereNull('o.order_tel');
                    break;
                case [2]: // 電話注文
                    $qb->whereNotNull('o.order_tel');
                    break;
                default:
                    // 共に選択された場合は全件該当するので検索条件に含めない
            }
        }

        // 注文回数
        if ( array_key_exists('order_count', $params) && $params['order_count'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            switch ( $params['order_count'] ) {
                case [1]: // 新規
                    $qb
                        ->where('u.buy_times', '<=' , 1);
                    break;
                case [2]: // 常連
                    $qb
                        ->where('u.buy_times', '>=' , 2);
                    break;
                default:
            }
        }

        // 性別
        if ( array_key_exists('sex', $params) && $params['sex'] ) {
            $qb->whereIn('o.order_sex', $params['sex']);
        }

        // 注文備考欄に含む
        if ( array_key_exists('order_notein', $params) && $params['order_notein'] ) {
            $note = $params['order_notein'];
            $qb->where('o.note', 'LIKE', "%$note%");
        }

        // 注文備考欄に含まない
        if ( array_key_exists('order_noteout', $params) && $params['order_noteout'] ) {
            $note = $params['order_noteout'];
            $qb->where('o.note', 'NOT LIKE', "%$note%");
        }

        // 注文日
        if ( array_key_exists('order_date_start', $params) && $params['order_date_start'] ) {
            $date = $params['order_date_start'];
            $qb->where('o.order_date', '>=' , $date);
        }
        if ( array_key_exists('order_date_end', $params) && $params['order_date_end'] ) {
            $date = Carbon::parse($params['order_date_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('o.order_date', '<' , $date);
        }

        // 注文商品
        if ( array_key_exists('order_product', $params) && $params['order_product'] ) {
            $orderProduct = $params['order_product'];
            if ( !$joinedOrderDetails ) {
                $qb->leftJoin('order_details as od', 'od.order_id', '=', 'o.id');
                $joinedOrderDetails = true;
            }
            
            $qb->where('od.product_name', 'LIKE', "%$orderProduct%");
        }

        // 入金日
        if ( array_key_exists('pay_date_start', $params) && $params['pay_date_start'] ) {
            $date = $params['pay_date_start'];
            $qb->where('o.payment_date', '>=' , $date);
        }
        if ( array_key_exists('pay_date_end', $params) && $params['pay_date_end'] ) {
            $date = Carbon::parse($params['pay_date_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('o.payment_date', '<' , $date);
        }

        // 請求額
        if ( array_key_exists('pay_total_start', $params) && $params['pay_total_start'] ) {
            $total = $params['pay_total_start'];
            $qb->where('o.payment_total', '>=' , $total);
        }
        if ( array_key_exists('pay_total_end', $params) && $params['pay_total_end'] ) {
            $total = $params['pay_total_end'];
            $qb->where('o.payment_total', '<=' , $total);
        }

        // 発送日
        if ( array_key_exists('send_date_start', $params) && $params['send_date_start'] ) {
            $date = $params['send_date_start'];
            $qb->where('o.commit_date', '>=' , $date);
        }
        if ( array_key_exists('send_date_end', $params) && $params['send_date_end'] ) {
            $date = Carbon::parse($params['send_date_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('o.commit_date', '<' , $date);
        }

        // 追跡番号
        if ( array_key_exists('order_tracks', $params) && $params['order_tracks'] ) {
            if ( !$joinedTrackingNumbers ) {
                $qb->leftJoin('tracking_numbers as t', 't.order_id', '=', 'o.id');
                $joinedTrackingNumbers = true;
            }
            $ids = str_replace(array(" ", "　"), "", $params['order_tracks']);
            $qb->whereIn('t.number', explode(',', $ids));
        }

        // 会員ID
        if ( array_key_exists('user_id', $params) && $params['user_id'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            // スペースを削除
            $params['user_id'] = preg_replace('/[ 　]/u', '', $params['user_id']);
            $qb->where('users.id', $params['user_id']);
        }

        // 初回注文日
        if ( array_key_exists('order_first_start', $params) && $params['order_first_start'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            $date = $params['order_first_start'];
            $qb->where('users.first_buy_date', '>=' , $date);
        }
        if ( array_key_exists('order_first_end', $params) && $params['order_first_end'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            $date = Carbon::parse($params['order_first_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('users.first_buy_date', '<' , $date);
        }

        // 会員備考欄に含む
        if ( array_key_exists('user_notein', $params) && $params['user_notein'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            $note = $params['user_notein'];
            $qb->where('users.note', 'LIKE', "%$note%");
        }

        // 会員備考欄に含まない
        if ( array_key_exists('user_noteout', $params) && $params['user_noteout'] ) {
            if ( !$joinedUsers ) {
                $qb->leftJoin('users as u', 'u.id', '=', 'o.user_id');
                $joinedUsers = true;
            }
            $note = $params['user_noteout'];
            $qb->where('users.note', 'NOT LIKE', "%$note%");
        }
        
        $qb->orderBy('id', 'DESC');

        return $qb;
    }

    /**
     * 仮注文を取得
     */
    public function getPreOrder($preOrderId)
    {
        $order = Order::where('pre_order_id', $preOrderId)
            ->where('user_id', Auth::id())
            ->first();

        return $order;
    }

    /**
     * 注文を取得
     */
    public function getOrder($orderId)
    {
        $order = Order::findOrFail($orderId);

        return $order;
    }

    /**
     * 会員の注文一覧を取得
     */
    public function getUserOrders($userId)
    {
        $qb = Order::where('user_id', $userId);

        return $qb;
    }

    /**
     * 注文を配列に変更
     */
    public function arrayOrders($params)
    {
        $orders = [];
        $orders['id']                   = $params->id;
        $orders['order_no']             = $params->order_no;
        $orders['user_id']              = $params->user_id;
        $orders['order_pref']           = $params->order_pref;
        $orders['order_sex']            = $params->order_sex;
        $orders['payment_id']           = $params->payment_id;
        $orders['order_name01']         = $params->order_name01;
        $orders['order_name02']         = $params->order_name01;
        $orders['order_kana01']         = $params->order_kana01;
        $orders['order_kana02']         = $params->order_kana02;
        $orders['order_company_name']   = $params->order_company_name;
        $orders['order_email']          = $params->order_email;
        $orders['order_tel']            = $params->order_tel;
        $orders['order_post_office_id'] = $params->order_post_office_id;
        $orders['order_zip01']          = $params->order_zip01;
        $orders['order_zip02']          = $params->order_zip02;
        $orders['order_addr01']         = $params->order_addr01;
        $orders['order_addr02']         = $params->order_addr02;
        $orders['order_birth']          = $params->order_birth;
        $orders['subtotal']             = intval($params->subtotal);
        $orders['discount']             = intval($params->discount);
        $orders['delivery_fee_total']   = intval($params->delivery_fee_total);
        $orders['use_point']            = intval($params->use_point);
        $orders['add_point']            = intval($params->add_point);
        $orders['total']                = intval($params->total);
        $orders['payment_total']        = intval($params->payment_total);
        $orders['payment_method']       = $params->payment_method;
        $orders['note']                 = $params->note;
        $orders['create_date']          = $params->create_date ? $params->create_date->format('Y-m-d H:i:s') : '';
        $orders['update_date']          = $params->update_date ? $params->update_date->format('Y-m-d H:i:s') : '';
        $orders['order_date']           = $params->order_date ? $params->order_date->format('Y-m-d H:i:s') : '';
        $orders['commit_date']          = $params->commit_date ? $params->commit_date->format('Y-m-d H:i:s') : '';
        $orders['status']               = $params->status;
        foreach ($params->orderDetails as $orderDetail) {
            $id = $orderDetail->id;
            $orders['orderDetails'][$id]['order_id']         = $orderDetail->order_id;
            $orders['orderDetails'][$id]['product_id']       = $orderDetail->product_id;
            $orders['orderDetails'][$id]['product_class_id'] = $orderDetail->product_class_id;
            $orders['orderDetails'][$id]['product_name']     = $orderDetail->product_name;
            $orders['orderDetails'][$id]['product_code']     = $orderDetail->product_code;
            $orders['orderDetails'][$id]['zaiko_id']         = $orderDetail->zaiko_id;
            $orders['orderDetails'][$id]['class_name1']      = $orderDetail->class_name1;
            $orders['orderDetails'][$id]['class_name2']      = intval($orderDetail->class_name2);
            $orders['orderDetails'][$id]['price']            = intval($orderDetail->price);
            $orders['orderDetails'][$id]['quantity']         = intval($orderDetail->quantity);
            $orders['orderDetails'][$id]['subtotal']         = intval($orderDetail->price * $orderDetail->quantity);
            $orders['orderDetails'][$id]['dose']             = intval($orderDetail->productClass->dose);
            $orders['orderDetails'][$id]['shape']            = $orderDetail->productClass->product->shape;
        } unset($orderDetail);

        return $orders;
    }
}