<?php

//declare(strict_types=1);

namespace App\Repositories\ProductCategory;

use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\ProductCategory;

class ProductCategoryRepository implements ProductCategoryRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createProductCategory($params)
    {
        $productCategory = ProductCategory::create($params);
        if ( $productCategory->save() ) {
            return $productCategory;
        }
    }

    /**
     * DELETE
     */
    public function deleteProductCategoryAll($productId)
    {
        // 一旦全削除
        $productCategories = ProductCategory::where('product_id', $productId)->get();
        if ( $productCategories ) {
            foreach ( $productCategories as $productCategory ) {
                $productCategory->delete();
            } unset ( $productCategory );
        }
    }

    /**
     * 商品のカテゴリを取得
     */
    public function getProductCategories($categoryIds)
    {
        $productCategories = ProductCategory::from('product_category as pc')
                        ->leftJoin('categories as c', 'c.id', '=', 'pc.category_id')
                        ->whereIn('pc.product_id', explode(',', $categoryIds))
                        ->get();

        return $productCategories;
    }
}