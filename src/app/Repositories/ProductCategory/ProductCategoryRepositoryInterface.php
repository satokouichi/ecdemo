<?php

namespace App\Repositories\ProductCategory;

interface ProductCategoryRepositoryInterface
{
    public function deleteProductCategoryAll($params);
    public function getProductCategories($categoryIds);
}