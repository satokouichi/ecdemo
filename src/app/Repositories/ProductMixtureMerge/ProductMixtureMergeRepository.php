<?php

//declare(strict_types=1);

namespace App\Repositories\ProductMixtureMerge;

use Illuminate\Support\Facades\DB;

class ProductMixtureMergeRepository implements ProductMixtureMergeRepositoryInterface
{
    protected $table = 'product_mixture_merges';

    public function __construct()
    {
        //
    }

    public function getAll()
    {
        return DB::table($this->table)->get();
    }
}