<?php

//declare(strict_types=1);

namespace App\Repositories\ProductMixtureMerge;

interface ProductMixtureMergeRepositoryInterface
{
    public function getAll();
}