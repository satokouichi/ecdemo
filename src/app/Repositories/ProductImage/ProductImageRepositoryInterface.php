<?php

//declare(strict_types=1);

namespace App\Repositories\ProductImage;

interface ProductImageRepositoryInterface
{
    public function createProductImage($params);
    public function deleteProductImageAll($productId);
}