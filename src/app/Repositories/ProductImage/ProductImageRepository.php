<?php

namespace App\Repositories\ProductImage;

use Illuminate\Support\Facades\DB;
use App\Models\ProductImage;

class ProductImageRepository implements ProductImageRepositoryInterface
{
    protected $table = 'product_images';

    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createProductImage($params)
    {
        $productImage = ProductImage::create($params);
        if ( $productImage->save() ) {
            return $productImage;
        }
    }

    /**
     * DELETE
     */
    public function deleteProductImageAll($productId)
    {
        // 一旦全削除
        $productImages = ProductImage::where('product_id', $productId)->get();
        if ( $productImages ) {
            foreach ( $productImages as $productImage ) {
                $productImage->delete();
            } unset ( $productImage );
        }
    }
}