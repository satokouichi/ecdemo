<?php

//declare(strict_types=1);

namespace App\Repositories\ClassCategory;

interface ClassCategoryRepositoryInterface
{
    public function createClassCategory($params);
    public function updateClassCategory($params, $id);
    public function getClassCategory($params);
    public function getClassCategories();
}