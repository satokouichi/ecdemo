<?php

namespace App\Repositories\ClassCategory;

use Illuminate\Support\Facades\DB;
use App\Models\ClassCategory;

class ClassCategoryRepository implements ClassCategoryRepositoryInterface
{
    protected $table = 'class_categories';

    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createClassCategory($params)
    {
        $classCategory = ClassCategory::create($params);
        if ( $classCategory->save() ) {
            return $classCategory;
        }
    }

    /**
     * UPDATE
     */
    public function updateClassCategory($params, $id)
    {
        $result = ClassCategory::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    public function getClassCategory($params)
    {
        $qb = ClassCategory::from('class_categories AS cc')
            ->select('cc.*')
            ->join('class_names AS cn', 'cc.class_name_id', '=', 'cn.id')
            ->where('cn.del_flg', 0)
            ->where('cc.del_flg', 0);

        // 分類(ID)
        if ( array_key_exists('id', $params) && $params['id'] ) {
            $qb->where('cc.id', $params['id']);
        }

        // 分類(名称)
        if ( array_key_exists('name', $params) && $params['name'] ) {
            $qb->where('cc.name', $params['name']);
        }

        // 規格(ID)
        if ( array_key_exists('class_names-id', $params) && $params['class_names-id'] ) {
            $qb->where('cn.id', $params['class_names-id']);
        }

        // 規格(名称)
        if ( array_key_exists('class_names-name', $params) && $params['class_names-name'] ) {
            $qb->where('cn.name', $params['class_names-name']);
        }

        return $qb;
    }

    public function getClassCategories()
    {
        $qb = ClassCategory::from('class_categories AS cc')
            ->select('cc.*')
            ->join('class_names AS cn', 'cc.class_name_id', '=', 'cn.id')
            ->where('cn.del_flg', 0)
            ->where('cc.del_flg', 0);

        return $qb;
    }

}