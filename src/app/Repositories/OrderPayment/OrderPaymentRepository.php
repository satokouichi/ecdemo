<?php

namespace App\Repositories\OrderPayment;

use Illuminate\Support\Facades\DB;
use App\Models\OrderPayment;

class OrderPaymentRepository implements OrderPaymentRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createOrderPayment($params)
    {
        $orderPayment = OrderPayment::create($params);
        if ( $orderPayment->save() ) {
            return $orderPayment;
        }
    }

    /**
     * DELETE
     */
    public function deleteOrderPayment($params)
    {
        $orderPayment = OrderPayment::find($params)->delete();
    }
}