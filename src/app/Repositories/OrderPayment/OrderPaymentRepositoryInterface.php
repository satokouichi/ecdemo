<?php

namespace App\Repositories\OrderPayment;

interface OrderPaymentRepositoryInterface
{
    public function createOrderPayment($params);
    public function deleteOrderPayment($params);
}