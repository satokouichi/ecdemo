<?php

namespace App\Repositories\Product;

use Illuminate\Support\Facades\DB;
use App\Models\Product;
use Carbon\Carbon;

class ProductRepository implements ProductRepositoryInterface
{
    public function __construct()
    {
    }

    /**
     * INSERT
     */
    public function createProduct($params)
    {
        $product = Product::create($params);
        if ( $product->save() ) {
            return $product;
        }
    }

    /**
     * UPDATE
     */
    public function updateProduct($params, $id)
    {
        $result = Product::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    /**
     * 商品を更新
     */
    public function updateProductByAdmin($request)
    {
        $params = $request->except(['id','_token']);
        if ( $this->updateProduct($params, $request->input('id')) ) {
            return true;
        } else {
            return false;
        }
    }
        
    public function searchProductFrontQuery($params)
    {
        $qb = Product::from('products AS p')
            ->select('p.*')
            ->distinct()
            ->where('p.del_flg', 0)
            ->where('p.status', 1);

        $joinedProductCategory = false;
        $joinedProductReviews = false;
        $joinedProductClasses = false;

        // 商品ID（複数）
        if ( array_key_exists('product_ids', $params) && $params['product_ids'] ) {
            $qb->whereIn('p.id', $params['product_ids']);
        }

        // 商品名
        if ( array_key_exists('product_name', $params) && $params['product_name'] ) {
            $qb->where('p.name', $params['product_name']);
        }

        return $qb;
    }
    
   
    public function searchProductAdminQuery($params)
    {
        $qb = Product::from('products AS p')
            ->select('p.*')
            ->distinct()
            ->where('p.del_flg', 0);

        $joinedProductCategory = false;
        $joinedProductReviews = false;
        $joinedProductClasses = false;

        // 商品ID（複数）
        if ( array_key_exists('product_ids', $params) && $params['product_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['product_ids']);
            $qb->whereIn('p.id', explode(',', $ids));
        }

        // 商品名
        if ( array_key_exists('product_name', $params) && $params['product_name'] ) {
            $qb->where('p.name', $params['product_name']);
        }

        // カテゴリID（複数）
        if ( array_key_exists('category_ids', $params) && $params['category_ids'] ) {
            $categoryIds = str_replace(array(" ", "　"), "", $params['category_ids']);
            if ( !$joinedProductCategory ) {
                $joinedProductCategory = true;
                $qb->join('product_category AS pct', 'p.id', '=', 'pct.product_id');
            }
            $qb->whereIn('pct.category_id', $categoryIds);
        }

        // 商品コード
        if ( array_key_exists('product_code', $params) && $params['product_code'] ) {
            if ( !$joinedProductClasses ) {
                $qb->join('product_classes as pc', 'pc.product_id', '=', 'p.id');
                $joinedProductClasses = true;
            }
            $code = $params['product_code'];
            $qb->where('pc.product_code', 'LIKE', "%$code%");
        }

        // 在庫ID
        if ( array_key_exists('zaiko_id', $params) && $params['zaiko_id'] ) {
            if ( !$joinedProductClasses ) {
                $qb->join('product_classes as pc', 'pc.product_id', '=', 'p.id');
                $joinedProductClasses = true;
            }
            $zaikoId = $params['zaiko_id'];
            $qb->where('product_classes.zaiko_id', 'LIKE', "%$zaikoId%");
        }

        // 在庫有無
        if ( isset($params['product_stock']) && !empty($params['product_stock']) ) {
            if ( !$joinedProductClasses ) {
                $qb->join('product_classes as pc', 'pc.product_id', '=', 'p.id');
                $joinedProductClasses = true;
            }
            switch ( $params['product_stock'] ) {
                case [1]:
                    $qb->where(function($qb){
                        $qb->where('pc.stock_unlimited', true)
                            ->orWhere('pc.stock', '>' , 0);
                    });
                    break;
                case [2]:
                    $qb->where(function($qb){
                        $qb->where('pc.stock_unlimited', false)
                            ->orWhere('pc.stock', '<=' , 0);
                    });
                    break;
                default:
                    // 共に選択された場合は全権該当するので検索条件に含めない
            }
        }

        // 登録日
        if ( array_key_exists('product_create_start', $params) && $params['product_create_start'] ) {
            $date = $params['product_create_start'];
            $qb->where('p.create_date', '>=' , $date);
        }
        if ( array_key_exists('product_create_end', $params) && $params['product_create_end'] ) {
            $date = Carbon::parse($params['product_create_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('p.create_date', '<' , $date);
        }

        // 更新日
        if ( array_key_exists('product_update_start', $params) && $params['product_update_start'] ) {
            $date = $params['product_update_start'];
            $qb->where('p.update_date', '>=' , $date);
        }
        if ( array_key_exists('product_update_end', $params) && $params['product_update_end'] ) {
            $date = Carbon::parse($params['product_update_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('p.update_date', '<' , $date);
        }

        return $qb;
    }

    public function searchProductAdminOrder($params)
    {
        $qb = Product::from('products AS p')
            ->select('p.*')
            ->distinct()
            ->leftJoin('product_category as pct', 'pct.product_id', '=', 'p.id')
            ->join('product_classes as pc', 'pc.product_id', '=', 'p.id')
            ->where('p.del_flg', 0);

        // カテゴリ
        if ( array_key_exists('category', $params) && $params['category'] ) {
            $category = $params['category'];
            $qb->where('pct.category_id', $category);
        }

        // 商品ID or 商品名 or 商品コード
        if ( array_key_exists('product', $params) && $params['product'] ) {
            $product = preg_match('/^\d{0,10}$/', $params['product']) ? $params['product'] : null;
            $qb
                ->orWhere('p.id', $product)
                ->orWhere('p.name', 'like' , '%'.str_replace(['%', '_'], ['\\%', '\\_'], $product).'%')
                ->orWhere('pc.product_code', $product);
        }

        return $qb->get()->toArray();
    }

    /**
     * 商品を取得
     */
    public function getProduct($params)
    {
        $qb = Product::from('products AS p')
            ->select('p.*')
            ->where('p.del_flg', 0);

        // 商品ID
        if ( array_key_exists('id', $params) && $params['id'] ) {
            $qb->where('p.id', $params['id']);
        }

        // 商品名
        if ( array_key_exists('name', $params) && $params['name'] ) {
            $qb->where('p.name', $params['name']);
        }

        return $qb;
    }

    public function arrayProducts($params)
    {
        $products = [];
        foreach ( $params as $product ) {
            $id = $product->id;
            $products[$id]['id']                 = $product->id;
            $products[$id]['status']             = $product->status;
            $products[$id]['name']               = $product->name;
            $products[$id]['note']               = $product->note;
            $products[$id]['description_list']   = $product->description_list;
            $products[$id]['description_detail'] = $product->description_detail;
            $products[$id]['search_word']        = $product->search_word;
            $products[$id]['recommend_text']     = $product->recommend_text;
            $products[$id]['title']              = $product->title;
            $products[$id]['description']        = $product->description;
            $products[$id]['shape']              = $product->shape;
            $products[$id]['trader_id']          = $product->trader_id;
            // 商品詳細
            if ( $product->productClasses->isNotEmpty() ) {
                foreach ( $product->productClasses as $productClass ) {
                    $products[$id]['productClasses'][$productClass->id]['id']                     = $productClass->id;
                    $products[$id]['productClasses'][$productClass->id]['product_id']             = $productClass->product_id;
                    $products[$id]['productClasses'][$productClass->id]['class_category_id1']     = $productClass->class_category_id1;
                    $products[$id]['productClasses'][$productClass->id]['class_category_id2']     = $productClass->class_category_id2;
                    $products[$id]['productClasses'][$productClass->id]['product_code']           = $productClass->product_code;
                    $products[$id]['productClasses'][$productClass->id]['zaiko_id']               = $productClass->zaiko_id;
                    $products[$id]['productClasses'][$productClass->id]['stock']                  = $productClass->stock;
                    $products[$id]['productClasses'][$productClass->id]['stock_unlimited']        = $productClass->stock_unlimited;
                    $products[$id]['productClasses'][$productClass->id]['price01']                = $productClass->price01;
                    $products[$id]['productClasses'][$productClass->id]['price02']                = $productClass->price02;
                    $products[$id]['productClasses'][$productClass->id]['expire_date']            = $productClass->expire_date;
                    $products[$id]['productClasses'][$productClass->id]['status']                 = $productClass->status;
                    $products[$id]['productClasses'][$productClass->id]['dose']                   = $productClass->dose;
                    $products[$id]['productClasses'][$productClass->id]['handling_flg']           = $productClass->handling_flg;
                    $products[$id]['productClasses'][$productClass->id]['default_flg']            = $productClass->default_flg;
                    $products[$id]['productClasses'][$productClass->id]['class_categories1_name'] = $productClass->classCategories1->name;
                    $products[$id]['productClasses'][$productClass->id]['class_categories2_name'] = $productClass->classCategories2->name;
                } unset($productClass);
            }
            // カテゴリ
            if ( !$product->categories->isEmpty() ) {
                foreach ( $product->categories as $category ) {
                    $products[$id]['categories'][$category->id]['id']                 = $category->id;
                    $products[$id]['categories'][$category->id]['parent_category_id'] = $category->parent_category_id;
                    $products[$id]['categories'][$category->id]['category_name']      = $category->category_name;
                    $products[$id]['categories'][$category->id]['level']              = $category->level;
                    $products[$id]['categories'][$category->id]['rank']               = $category->rank;
                } unset($category);
            }
            // 画像
            if ( !$product->productImages->isEmpty() ) {
                foreach ( $product->productImages as $productImage ) {
                    $products[$id]['productImages'][$productImage->id]['product_id'] = $productImage->product_id;
                    $products[$id]['productImages'][$productImage->id]['file_name']  = $productImage->file_name;
                    $products[$id]['productImages'][$productImage->id]['file_text']  = $productImage->file_text;
                    $products[$id]['productImages'][$productImage->id]['rank']       = $productImage->rank;
                } unset($productImage);
            }
        } unset($item);

        return $products;
    }
}