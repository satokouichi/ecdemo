<?php

namespace App\Repositories\Product;

interface ProductRepositoryInterface
{
    public function createProduct($params);
    public function updateProduct($params, $id);
    public function updateProductByAdmin($request);
    public function searchProductFrontQuery($params);
    public function searchProductAdminQuery($params);
    public function searchProductAdminOrder($params);
    public function getProduct($params);
    public function arrayProducts($params);
}