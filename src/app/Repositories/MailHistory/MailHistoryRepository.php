<?php

namespace App\Repositories\MailHistory;

use Illuminate\Support\Facades\DB;
use App\Models\MailHistory;

class MailHistoryRepository implements MailHistoryRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * 注文のメール送信履歴を取得
     */
    public function getOrderMailHistories($orderId)
    {
        $qb = MailHistory::where('order_id', $orderId);

        return $qb;
    }
}