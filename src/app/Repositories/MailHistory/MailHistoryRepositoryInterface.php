<?php

namespace App\Repositories\MailHistory;

interface MailHistoryRepositoryInterface
{
    public function getOrderMailHistories($orderId);
}