<?php

//declare(strict_types=1);

namespace App\Repositories\Category;

interface CategoryRepositoryInterface
{
    public function createCategory($params);
    public function updateCategory($params, $id);
    public function getCategory($id);
    public function getCategories();
    public function getCategoryArray($items);
}