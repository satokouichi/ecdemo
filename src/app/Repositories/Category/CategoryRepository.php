<?php

namespace App\Repositories\Category;

use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createCategory($params)
    {
        $category = Category::create($params);
        if ( $category->save() ) {
            return $category;
        }
    }

    /**
     * UPDATE
     */
    public function updateCategory($params, $id)
    {
        $result = Category::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    public function getCategory($id)
    {
        $qb = Category::from('categories AS c')
            ->select('c.*')
            ->where('c.id', $id)
            ->where('c.del_flg', 0);

        return $qb;
    }

    public function getCategories()
    {
        $qb = Category::from('categories AS c')
            ->select('c.*')
            ->where('c.del_flg', 0);

        return $qb;
    }

    public function getCategoryArray($items)
    {
        $categories = [];

        // 親カテゴリ
        foreach ($items as $parent) {
            if (is_null($parent->parent_category_id)) {
                $categories[$parent->id]['id'] = $parent->id;
                $categories[$parent->id]['category_name'] = $parent->category_name;
                $categories[$parent->id]['level'] = $parent->level;
                $categories[$parent->id]['rank'] = $parent->rank;
                $categories[$parent->id]['parentCategories'] = [];
            }
        } unset($parent);

        // 子カテゴリ
        foreach ($items as $child) {
            if (array_key_exists($child->parent_category_id, $categories)) {
                $categories[$child->parent_category_id]['childCategories'][$child->id] = $child->toArray();
            }
        } unset($child);

        return $categories;
    }
}