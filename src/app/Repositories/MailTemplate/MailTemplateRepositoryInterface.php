<?php

namespace App\Repositories\MailTemplate;

interface MailTemplateRepositoryInterface
{
    public function getMailTemplate($id);
    public function getMailTemplates();
}