<?php

namespace App\Repositories\MailTemplate;

use Illuminate\Support\Facades\DB;
use App\Models\MailTemplate;

class MailTemplateRepository implements MailTemplateRepositoryInterface
{
    public function __construct()
    {
        //
    }

    /**
     * メールテンプレートを取得
     */
    public function getMailTemplate($id)
    {
        $mailTemplate = MailTemplate::findOrFail($id);

        return $mailTemplate;
    }

    /**
     * メールテンプレートすべてを取得
     */
    public function getMailTemplates()
    {
        $mailTemplate = MailTemplate::all();

        return $mailTemplate;
    }
}