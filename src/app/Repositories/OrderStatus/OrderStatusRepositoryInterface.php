<?php

namespace App\Repositories\OrderStatus;

interface OrderStatusRepositoryInterface
{
    public function get($id);
    public function getAll();
}