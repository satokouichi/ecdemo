<?php

namespace App\Repositories\OrderStatus;

use Illuminate\Support\Facades\DB;
use App\Models\OrderStatus;

class OrderStatusRepository implements OrderStatusRepositoryInterface
{
    public function __construct()
    {
        //
    }

    public function get($id)
    {
        $qb = OrderStatus::from('order_statuses AS os')
            ->select('os.*')
            ->where('os.id', $id)
            ->where('os.status', 1);

        return $qb->first();
    }

    public function getAll()
    {
        $qb = OrderStatus::from('order_statuses AS os')
            ->select('os.*')
            ->where('os.status', '<>', 8);

        return $qb->get();
    }
}