<?php

namespace App\Repositories\User;

interface UserRepositoryInterface
{
    public function createUser($params);
    public function updateUser($params, $userId);
    public function createUserByAdmin($request);
    public function updateUserByAdmin($request);
    public function getUser($id, $status);
    public function getUsers();
    public function searchUserAdminQuery($params);
}