<?php

namespace App\Repositories\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Carbon\Carbon;
use App\Repositories\ProductCategory\ProductCategoryRepositoryInterface AS ProductCategory;

class UserRepository implements UserRepositoryInterface
{
    private $productCategory;
    
    public function __construct(
        ProductCategory $productCategory
    )
    {
        $this->productCategory = $productCategory;
    }

    /**
     * INSERT
     */
    public function createUser($params)
    {
        $user = User::create($params);
        if ( $user->save() ) {
            return $user;
        }
    }

    /**
     * UPDATE
     */
    public function updateUser($params, $userId)
    {
        $result = User::find($userId)
            ->fill($params)
            ->save();

        return $result;
    }

    /**
     * レビューを登録
     */
    public function createUserByAdmin($request)
    {
        $params = $request->except(['id','_token']);
        return DB::transaction(function() use ($params) {
            $user = $this->createUser($params);

            return $user;
        });
    }
    
    /**
     * レビューを更新
     */
    public function updateUserByAdmin($request)
    {
        $params = $request->except(['id','_token']);
        if ($params['password'] == config('params')['password_default']) {
            $params['password'] = User::find($request->input('id'))->password;
        } else {
            $params['password'] = Hash::make($params['password']);
        }
        
        return $this->updateUser($params, $request->input('id'));
    }

    public function getUser($id, $status = null)
    {
        $qb = User::from('users AS u')
            ->select('u.*')
            ->where('u.id', $id);
        
        if ($status) {
            $qb->where('u.status', $status);
        }

        return $qb->first();
    }

    public function getUsers()
    {
        $qb = User::from('users AS p')
            ->select('u.*')
            ->where('u.status', 1);

        return $qb->get();
    }

    /**
     * 会員一覧を取得
     */
    public function searchUserAdminQuery($params)
    {
        $qb = User::from('users AS u')
            ->select('u.*')
            ->distinct()
            ->where('u.del_flg', 0);

        $joinedOrders = false;
        $joinedOrderDetails = false;
        $joinedCategories = false;

        // 会員ID（複数）
        if ( array_key_exists('user_ids', $params) && $params['user_ids'] ) {
            $ids = str_replace(array(" ", "　"), "", $params['user_ids']);
            $qb->whereIn('u.id', explode(',', $ids));
        }

        // 会員者名
        if ( array_key_exists('user_name', $params) && $params['user_name'] ) {
            $name = $params['user_name'];
            $qb->where(DB::raw('CONCAT(u.name01, u.name02)'), 'LIKE', "%$name%");
        }

        // 会員者名フリガナ
        if ( array_key_exists('user_kana', $params) && $params['user_kana'] ) {
            $kana = $params['user_kana'];
            $qb->where(DB::raw('CONCAT(u.kana01, u.kana02)'), 'LIKE', "%$kana%");
        }

        // メールアドレス
        if ( array_key_exists('user_email', $params) && $params['user_email'] ) {
            $email = $params['user_email'];
            $qb->where('u.email', 'LIKE', "%$email%");
        }

        // 所持ポイント
        if ( array_key_exists('user_point_start', $params) && $params['user_point_start'] ) {
            $point = $params['user_point_start'];
            $qb->where('u.point', '>=' , $point);
        }
        if ( array_key_exists('user_point_end', $params) && $params['user_point_end'] ) {
            $point = $params['user_point_end'];
            $qb->where('u.point', '<=' , $point);
        }

        // 会員備考欄に含む
        if ( array_key_exists('user_notein', $params) && $params['user_notein'] ) {
            $note = $params['user_notein'];
            $qb->where('u.note', 'LIKE', "%$note%");
        }

        // 会員備考欄に含まない
        if ( array_key_exists('user_noteout', $params) && $params['user_noteout'] ) {
            $note = $params['user_noteout'];
            $qb->where('u.note', 'NOT LIKE', "%$note%");
        }

        // 登録日
        if ( array_key_exists('user_create_start', $params) && $params['user_create_start'] ) {
            $date = $params['user_create_start'];
            $qb->where('u.create_date', '>=' , $date);
        }
        if ( array_key_exists('user_create_end', $params) && $params['user_create_end'] ) {
            $date = Carbon::parse($params['user_create_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('u.create_date', '<' , $date);
        }

        // 更新日
        if ( array_key_exists('user_update_start', $params) && $params['user_update_start'] ) {
            $date = $params['user_update_start'];
            $qb->where('u.update_date', '>=' , $date);
        }
        if ( array_key_exists('user_update_end', $params) && $params['user_update_end'] ) {
            $date = Carbon::parse($params['user_update_end'])->modify('+1 days')->format('Y-m-d H:i:s');
            $qb->where('u.update_date', '<' , $date);
        }

        // 性別
        if ( array_key_exists('sex', $params) && $params['sex'] ) {
            $qb->whereIn('u.sex', $params['sex']);
        }

        // メルマガ
        if ( array_key_exists('user_mailmaga', $params) && $params['user_mailmaga'] ) {
            $qb->where('u.mailmaga_flg', $params['user_mailmaga']);
        }
        
        // 購入商品名 ※データ量に応じて負荷が増える可能性あり
        if ( array_key_exists('user_product', $params) && $params['user_product'] ) {
            if ( !$joinedOrders ) {
                $qb->leftJoin('orders as o', 'o.user_id', '=', 'u.id');
                $joinedOrders = true;
                if ( !$joinedOrderDetails ) {
                    $qb->leftJoin('order_details as od', 'od.order_id', '=', 'o.id');
                    $joinedOrderDetails = true;
                    $name = $params['user_product'];
                    $qb->where('od.product_name', 'LIKE', "%$name%");
                }
            }
        }

        // 商品カテゴリ ※データ量に応じて負荷が増える可能性あり
        if ( array_key_exists('user_category', $params) && $params['user_category'] ) {
            $qb->leftJoin('orders as o', 'o.user_id', '=', 'u.id');
            $joinedOrders = true;
            if ( !$joinedOrderDetails ) {
                $qb->leftJoin('order_details as od', 'od.order_id', '=', 'o.id');
                $joinedOrderDetails = true;
                if ( !$joinedCategories ) {
                    $qb->leftJoin('product_category as pct', 'pct.product_id', '=', 'od.product_id');
                    $qb->where('pct.product_id', $params['user_category']);
                }
            }
        }

        $qb->orderBy('id', 'DESC');

        return $qb;
    }
}