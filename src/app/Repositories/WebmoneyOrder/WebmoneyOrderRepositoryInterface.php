<?php

//declare(strict_types=1);

namespace App\Repositories\WebmoneyOrder;

interface WebmoneyOrderRepositoryInterface
{
    public function getAll();
}