<?php

//declare(strict_types=1);

namespace App\Repositories\WebmoneyOrder;

use Illuminate\Support\Facades\DB;

class WebmoneyOrderRepository implements WebmoneyOrderRepositoryInterface
{
    protected $table = 'Webmoney_orders';

    public function __construct()
    {
        //
    }

    public function getAll()
    {
        return DB::table($this->table)->get();
    }
}