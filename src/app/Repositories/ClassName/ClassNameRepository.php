<?php

namespace App\Repositories\ClassName;

use Illuminate\Support\Facades\DB;
use App\Models\ClassName;

class ClassNameRepository implements ClassNameRepositoryInterface
{
    protected $table = 'class_names';

    public function __construct()
    {
        //
    }

    /**
     * INSERT
     */
    public function createClassName($params)
    {
        $className = ClassName::create($params);
        if ( $className->save() ) {
            return $className;
        }
    }

    /**
     * UPDATE
     */
    public function updateClassName($params, $id)
    {
        $result = ClassName::find($id)
            ->fill($params)
            ->save();

        return $result;
    }

    public function getClassName($params)
    {
        $qb = ClassName::from('class_names AS cn')
            ->select('cn.*')
            ->where('cn.del_flg', 0);

        // 規格(ID)
        if ( array_key_exists('id', $params) && $params['id'] ) {
            $qb->where('cn.id', $params['id']);
        }

        // 規格(名称)
        if ( array_key_exists('name', $params) && $params['name'] ) {
            $qb->where('cn.name', $params['name']);
        }

        return $qb;
    }

    public function getClassNames()
    {
        $qb = ClassName::from('class_names AS cn')
            ->select('cn.*')
            ->where('cn.del_flg', 0);

        return $qb;
    }
}