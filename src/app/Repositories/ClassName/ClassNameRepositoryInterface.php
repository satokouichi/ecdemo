<?php

namespace App\Repositories\ClassName;

interface ClassNameRepositoryInterface
{
    public function createClassName($params);
    public function updateClassName($params, $id);
    public function getClassName($params);
    public function getClassNames();
}