<?php

return [

  // パスワードデフォルト表記
  'password_default' => '**********',

  // フラッシュメッセージ
  'flash_message' => '登録完了いたしました',
  'error_message' => '登録エラーが発生いたしました',

];