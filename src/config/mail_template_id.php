<?php

return [
    'order'                        => 1,  // 注文受付
    'commit'                       => 6,  // 発送
    'payment'                      => 7,  // 入金
    'not_pay'                      => 8,  // 不足金
    'many_pay'                     => 9,  // 過剰金
    'commit_itibu'                 => 10, // 一部発送
    'cancell'                      => 11, // キャンセル
    'absence'                      => 12, // 不在通知
    'arrival'                      => 13, // 郵便局到着通知
    'hundred_point'                => 14, // 口コミ認証100
    'five_hundreds_point'          => 15, // 口コミ認証500
    'destination'                  => 16, // 口コミ認証500
    'free'                         => 17, // 大切なお知らせ
    'card_order_mail'              => 18, // カード決済完了
    'card_err_order_mail'          => 19, // 入金方法のご案内
    'conveni_retargeting_issue'    => 20, // コンビニ決済-払込ご案内
    'conveni_retargeting_unissued' => 21, // コンビニ決済-払込ご案内
    'card_manual_order_mail'       => 22, // カード決済申請
];