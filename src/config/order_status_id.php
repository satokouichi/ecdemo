<?php

return [
    'new'           => 1,  // 注文中
    'cancel'        => 3,  // キャンセル
    'delivered'     => 5,  // 発送済み
    'paid'          => 6,  // 入金済み
    'pending'       => 7,  // 決済処理中
    'processing'    => 8,  // 購入処理中
    'underpayment'  => 9,  // 不足金
    'partdelivered' => 10, // 一部未発送
    'expired'       => 11, // 期限切れ
    'orderpending'  => 12, // 発注保留
    'cardapplying'  => 13, // カード決済申請中
];